

--------------------------------------------------------------------------------------------------------------
--------------- QA problems: ---------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------
-- 1. missing link from sale transaction to property - possible reason: missing phase info
--------------------------------------------------------------------------------------------

-- update: after dedup sale_transaction
-- projects that have the most transactions unable to match to property 
select b.estate_name, count(*)
from source.hk_midland_realty_sale_transaction b
left join masterdata_hk.sale_transaction a on a.data_uuid = b.data_uuid 
where a.contract_date >= '2020-01-01' and b.mkt_type = 2  and b.area_sqm != 0
and a.property_dwid isnull --and a.activity_dwid notnull
group by 1 order by 2 desc;

-- also can break down to building
select b.estate_name, b.building_name , b.building_id , count(*)
from source.hk_midland_realty_sale_transaction b
left join masterdata_hk.sale_transaction a on a.data_uuid = b.data_uuid 
where a.contract_date >= '2020-01-01' and b.mkt_type = 2  and b.area_sqm != 0
and a.property_dwid isnull and a.activity_dwid notnull
group by 1,2,3 order by 4 desc;


'''
0.sale_transaction table has dups -- same data_uuid but have multi txn records. eg: data_uuid = f1c99d37-d633-4853-8467-90e2b27ca5bb


1.find the estate names with missing phase info
2.fill in gaps in address table for these estate
3.use data_uuid in sale_transaction to join with hk_midland_realty_sale_transaction to get info and join with address table to get address_dwid, 
  --> like old model: tmp_hk_midland_sale_transaction.sql:

            on f_prep_dw_id(b.region_name) = f_prep_dw_id(c.city)
            and f_prep_dw_id(b.subregion_name) = f_prep_dw_id(c.city_area)
            and f_prep_dw_id(b.district_name) = f_prep_dw_id(c.city_subarea)
            and f_prep_dw_id(b.corrected_estate_name)= f_prep_dw_id(c.development)
            and f_prep_dw_id(b.corrected_phase_name)= f_prep_dw_id(c.development_phase)
            and f_prep_dw_id(b.corrected_building_name)= f_prep_dw_id(c.address_building)
            and f_prep_dw_id(b.corrected_street_num)= f_prep_dw_id(c.address_number)
            and f_prep_dw_id(b.corrected_street_name)= f_prep_dw_id(c.street_name)

4.use hk_midland_backfill to fill in gaps for missing floor / stack info, and join with property table using address_dwid and floor and stack to get property_dwid
5.use data_uuid and property_dwid to update sale_transaction table to fill in missing link with property
6.better to update the property_dwid in sale_transaction_w_old_dw_ids as well for convenience to join back with old hk_warehouse for reference

example: 
estate - SOUTH HORIZONS, LAGUNA CITY (follow the steps above)
building - Tai On Building, The Forest Hills (dups in sale_transaction, one has property id the other not) --> seldom have after dedup sale_transaction using data_uuid, 
other - Manhattan Heights, Tin Lai Court (missing correction in reference.hk_midland_hkpost_correction_backfill, fill in correction entities and follow the steps above)
'''

select a.*, b.*
from masterdata_hk.sale_transaction a
left join source.hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where b.estate_name ilike 'SOUTH HORIZONS' 
and a.contract_date >= '2020-01-01' and b.mkt_type = 2 and a.property_dwid isnull and b.area_sqm != 0;
--AND a.data_uuid = '1426dfd8-efdb-48f1-afe4-4f9f67b20'


select a.*, b.*
from masterdata_hk.sale_transaction a
left join source.hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where b.estate_name ilike 'The Forest Hills' 
and a.contract_date >= '2020-01-01' and b.mkt_type = 2  and b.area_sqm != 0
and a.property_dwid isnull;

select a.*, b.*
from masterdata_hk.sale_transaction a
left join source.hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where b.estate_name ilike 'The Forest Hills' 
--and a.contract_date >= '2020-01-01' and b.mkt_type = 2  and b.area_sqm != 0
and a.property_dwid notnull --and a.property_dwid isnull
;


select a.*, b.*
from masterdata_hk.sale_transaction a
left join source.hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where b.estate_name ilike 'No.8 Clear Water Bay Road' 
and a.contract_date >= '2020-01-01' and b.mkt_type = 2  and b.area_sqm != 0
and a.property_dwid isnull;

select a.*, b.*
from masterdata_hk.sale_transaction a
left join source.hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where b.estate_name ilike 'No.8 Clear Water Bay Road' 
and a.property_dwid notnull;


select b.*, a.*
from source.hk_midland_realty_sale_transaction b
left join masterdata_hk.sale_transaction a on a.data_uuid = b.data_uuid 
where a.contract_date >= '2020-01-01' and b.mkt_type = 2  and b.area_sqm != 0
and a.property_dwid isnull and a.activity_dwid notnull
and b.estate_name ilike 'No.8 Clear Water Bay Road';



--- buildings:


'''
No.8 Clear Water Bay Road	No.8 Clear Water Bay Road	B000056512	47 --> done
H Cube	H Cube	B000061721	41							--> done
Lime Stardom	Lime Stardom	B000068904	34 			--> 28 for remianing ones they indeed dont have corresponding property in our model
Tin Lai Court	Tin Lai Court	B000017249	33			--> done
Manhattan Heights	Manhattan Heights	B000005196	32  --> done
Viking Villas	Viking Villas	B000002029	25
Rosedale Gardens	Rosedale Gardens	B000008273	19
Wah Fung Garden	Wah Fung Garden	B000001672	16
Tsuen Fung Centre	Tsuen Fung Centre	B000001611	15
Evelyn Towers	Evelyn Towers	B000001772	15
Dor Boa Building	Dor Boa Building	B000008295	13
Waldorf Centre	Waldorf Centre	B000005096	13
Healey Building	Healey Building	B000005425	12
Ming Fai Building	Ming Fai Building	B000006795	11
Profit Nice Mansion	Profit Nice Mansion	B000022763	10
J Residence	J Residence	B000054217	10
'''

-- begin;

update map_hk.midland_building_to_dwid x
set address_dwid = '724fabed09a211126a4a36b016881ac8', building_dwid = 'cd046d1d660a3b5a3d4b485e581c8fbd'
WHERE building_id = 'B000056512';


create table branch_hk.midland_building_to_dwid_cr_1 as 
SELECT *
from map_hk.midland_building_to_dwid mbtd 
WHERE building_id = 'B000056512'; -- 1


create table branch_hk.midland_unit_to_dwid_cr_1 as 
SELECT 
	a.id,a.region_name,a.region_id,a.district_name,a.district_id,a.subregion_name,a.subregion_id,a.estate_name,a.estate_id,
	a.phase_name,a.phase_id,a.building_name,a.building_id,a.address_number,a.address_street,a.floor,a.stack,a.unit_id,
	p.property_dwid as property_dwid,
	b.building_dwid as building_dwid,
	b.address_dwid as address_dwid,
	b.project_dwid as project_dwid,
	b.lot_group_dwid as lot_group_dwid
FROM map_hk.midland_unit_to_dwid a
left join map_hk.midland_building_to_dwid b on a.building_id = b.building_id 
left join masterdata_hk.property p on b.building_dwid = p.building_dwid and a.floor = p.address_floor_text and a.stack = p.address_stack 
WHERE a.building_id = 'B000056512'; --58


update map_hk.midland_unit_to_dwid a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_unit_to_dwid_cr_1 b
where a.id = b.id; --58


create table branch_hk.midland_sale_txn__map_cr_1 as 
select 
a.data_uuid,c.activity_dwid,b.property_dwid,b.building_dwid,b.address_dwid,b.project_dwid,c.land_parcel_dwid,c.lot_group_dwid,c.status_code
from source.hk_midland_realty_sale_transaction a
left join branch_hk.midland_unit_to_dwid_cr_1 b on a.unit_id = b.unit_id
left join map_hk.midland_sale_txn__map c on a.data_uuid = c.data_uuid  
where b.id notnull;

update map_hk.midland_sale_txn__map a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_1 b
where a.data_uuid = b.data_uuid; --60

update masterdata_hk.sale_transaction a 
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_1 b
where a.data_uuid = b.data_uuid; -- 60
-- a.activity_dwid = b.activity_dwid


create table branch_hk.sale_transaction_temp_cr_1 as 
select a.activity_dwid, c.completion_year , b.property_type_code ,  b.address_unit, d.full_address_text , e.tenure_code ,
	b.bathroom_count , b.bedroom_count , b.gross_floor_area_sqm , b.net_floor_area_sqm
from masterdata_hk.sale_transaction a
left join masterdata_hk.property b on a.property_dwid = b.property_dwid 
left join feature_hk.de__building__completion_year_info c on a.building_dwid = c.building_dwid 
left join masterdata_hk.address d on a.address_dwid = d.address_dwid 
left join masterdata_hk.project e on a.project_dwid = e.project_dwid  
where exists (select 1 from branch_hk.midland_sale_txn__map_cr_1 mp where a.activity_dwid = mp.activity_dwid) 
;

--select f_clone_table('masterdata_hk', 'sale_transaction', 'masterdata_hk', 'sale_transaction1', TRUE, TRUE);

'''
update masterdata_hk.sale_transaction a
set property_completion_year = c.completion_year , 
	property_type_code = b.property_type_code , 
	address_unit = b.address_unit,
	address_local_text = d.full_address_text ,
	tenure_code = e.tenure_code ,
	bathroom_count = b.bathroom_count ,
	bedroom_count = b.bedroom_count ,
	gross_floor_area_sqm = b.gross_floor_area_sqm ,
	net_floor_area_sqm = b.net_floor_area_sqm
from masterdata_hk.property b where a.property_dwid = b.property_dwid 
left join feature_hk.de__building__completion_year_info c on a.building_dwid = c.building_dwid 
left join masterdata_hk.address d on a.address_dwid = d.address_dwid 
left join masterdata_hk.project e on a.project_dwid = e.project_dwid  
where exists (select 1 from branch_hk.midland_sale_txn__map_cr_1 mp where a.activity_dwid = mp.activity_dwid) 
'''

update masterdata_hk.sale_transaction a
set property_completion_year = b.completion_year::int , 
	property_type_code = b.property_type_code , 
	address_unit = b.address_unit,
	address_local_text = b.full_address_text ,
	tenure_code = b.tenure_code ,
	bathroom_count = b.bathroom_count ,
	bedroom_count = b.bedroom_count ,
	gross_floor_area_sqm = b.gross_floor_area_sqm ,
	net_floor_area_sqm = b.net_floor_area_sqm
from branch_hk.sale_transaction_temp_cr_1 b where a.activity_dwid = b.activity_dwid 
and exists (select 1 from branch_hk.sale_transaction_temp_cr_1 mp where a.activity_dwid = mp.activity_dwid); -- 60
	
-- end;


----- estates:

'''
Lohas Park	570			 --> need to integrate missing property
Discovery Bay	499		 --> need to integrate missing property
Coastal Skyline	425		 --> need to integrate missing property
Yoho Town	309 		 --> need to integrate missing property
Festival City	250      --> done
Fanling Centre	204      --> done
Tai Hing Gardens	192  --> done
Fairview Park	133      --> 232, need to integrate missing property
Jade Plaza	117          --> done
Tak Bo Garden	105      --> done
May Shing Court	95       --> 119, need to integrate missing property
Greenland Garden	93   --> done
Celestial Heights	84   --> done
Siu Hei Court	77		 --> need to integrate missing property
Garden Vista	71		 --> need to integrate missing property
Laguna Verde	70		 --> need to integrate missing property
Mount Parker Lodge	63	 --> need to integrate missing property
Wyler Gardens	61		 --> done
Kwong Lam Court	60		 --> done
Ka Tin Court	58		 --> done
Scenery Garden	54		 --> need to integrate missing property
Noble Hill	51			 --> need to integrate missing property
Siu On Court	50		 --> done
Yee Fung Garden	48		 --> need to integrate missing property
Greenery Plaza	44		 --> need to integrate missing property
Green Leaves Garden	42 	 --> need to integrate missing property
Ho Shun Tai Building40	 --> need to integrate missing property
Greenview Garden	40	 --> need to integrate missing property
Po Lam Estate	39		 --> done
Tin Oi Court	39		 --> done
'''

-- begin;


-- update map_hk.midland_building_to_dwid x
-- set --address_dwid = '5184445ab45c82209f947756c4e02e3c', building_dwid = '1600de1c612c30d37db816aa992199a9'
-- WHERE estate_id = 'E000007561';

-- create table branch_hk.midland_building_to_dwid_cr_4 as 
-- SELECT *
-- from map_hk.midland_building_to_dwid mbtd 
-- WHERE estate_id = 'E000007561'; 

create table branch_hk.midland_unit_to_dwid_cr_6 as 
SELECT 
	a.id,a.region_name,a.region_id,a.district_name,a.district_id,a.subregion_name,a.subregion_id,a.estate_name,a.estate_id,
	a.phase_name,a.phase_id,a.building_name,a.building_id,a.address_number,a.address_street,a.floor,a.stack,a.unit_id,
	p.property_dwid as property_dwid,
	b.building_dwid as building_dwid,
	b.address_dwid as address_dwid,
	b.project_dwid as project_dwid,
	b.lot_group_dwid as lot_group_dwid
FROM map_hk.midland_unit_to_dwid a
left join map_hk.midland_building_to_dwid b on a.building_id = b.building_id 
left join masterdata_hk.property p on b.building_dwid = p.building_dwid 
and a.floor = p.address_floor_text 
--and a.floor = trim(replace(split_part(p.address_unit, '-', 1), '/F', ''))
--and a.stack = p.address_stack 
--and (case when a.stack ilike '0%' then split_part(a.stack, '0', 2) else a.stack end) = p.address_stack 
and replace(a.stack, 'N', 'S') = replace(p.address_stack, 'N', 'S')
WHERE a.estate_id = 'E000007561' and a.property_dwid isnull; --373


update map_hk.midland_unit_to_dwid a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_unit_to_dwid_cr_6 b
where a.id = b.id; --373


create table branch_hk.midland_sale_txn__map_cr_6 as 
select 
a.data_uuid,c.activity_dwid,b.property_dwid,b.building_dwid,b.address_dwid,b.project_dwid,c.land_parcel_dwid,c.lot_group_dwid,c.status_code
from source.hk_midland_realty_sale_transaction a
left join branch_hk.midland_unit_to_dwid_cr_6 b on a.unit_id = b.unit_id
left join map_hk.midland_sale_txn__map c on a.data_uuid = c.data_uuid  
where b.id notnull; --386

update map_hk.midland_sale_txn__map a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_6 b
where a.data_uuid = b.data_uuid; --385


update masterdata_hk.sale_transaction a 
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_6 b
where a.data_uuid = b.data_uuid; -- 385
-- a.activity_dwid = b.activity_dwid


create table branch_hk.sale_transaction_temp_cr_6 as 
select a.activity_dwid, c.completion_year , b.property_type_code ,  b.address_unit, d.full_address_text , e.tenure_code ,
	b.bathroom_count , b.bedroom_count , b.gross_floor_area_sqm , b.net_floor_area_sqm
from masterdata_hk.sale_transaction a
left join masterdata_hk.property b on a.property_dwid = b.property_dwid 
left join feature_hk.de__building__completion_year_info c on a.building_dwid = c.building_dwid 
left join masterdata_hk.address d on a.address_dwid = d.address_dwid 
left join masterdata_hk.project e on a.project_dwid = e.project_dwid  
where exists (select 1 from branch_hk.midland_sale_txn__map_cr_6 mp where a.activity_dwid = mp.activity_dwid) 
; -- 385

--select f_clone_table('masterdata_hk', 'sale_transaction', 'masterdata_hk', 'sale_transaction1', TRUE, TRUE);

update masterdata_hk.sale_transaction a
set property_completion_year = b.completion_year::int , 
	property_type_code = b.property_type_code , 
	address_unit = b.address_unit,
	address_local_text = b.full_address_text ,
	tenure_code = b.tenure_code ,
	bathroom_count = b.bathroom_count ,
	bedroom_count = b.bedroom_count ,
	gross_floor_area_sqm = b.gross_floor_area_sqm ,
	net_floor_area_sqm = b.net_floor_area_sqm
from branch_hk.sale_transaction_temp_cr_6 b where a.activity_dwid = b.activity_dwid 
and exists (select 1 from branch_hk.sale_transaction_temp_cr_6 mp where a.activity_dwid = mp.activity_dwid); -- 385

-- end;



------ big projects:

select p2.project_display_name , count(p.property_dwid) as unit_cnt
from masterdata_hk.property p 
left join masterdata_hk.project p2 on p.project_dwid = p2.project_dwid 
group by 1 order by 2 desc



select address_street_text , address_building , count(*)
from masterdata_hk.address a 
where address_type_code = 'point-address'
group by 1,2 having count(*) > 1


select *
from masterdata_hk.address a 
where address_street_text = '100-102 lai chi kok road' and address_building = 'kam fung house'


select *
from masterdata_hk.address a 
where address_street_text = '80 sheung shing street'
and address_building = 'block 37'




--------------------------------------------------------------------------------------------
-- 2. property floor number and floor seq
--------------------------------------------------------------------------------------------
-- use some keywords to fix the commercial property type identifier

select address_unit , count(*)
from masterdata_hk.property p 
where address_unit not ilike '%flat%' and address_unit not ilike '%shop%' and address_unit not ilike '%room%' and address_unit not ilike '%suite%'
group by 1 order by 1;
-- SHOP, STALL


select p2.project_display_name , count(*)
from masterdata_hk.property p 
left join masterdata_hk.project p2 on p.project_dwid = p2.project_dwid 
where p.property_type_code = 'condo'
group by 1 order by 2, 1;

select b.building_display_name  , count(*)
from masterdata_hk.property p 
left join masterdata_hk.building b on p.building_dwid = b.building_dwid 
where p.property_type_code = 'condo'
group by 1 order by 2;


''' -- project_display_name ilike '%Medical%'
-- project_display_name ilike '%University%' and project_display_name! = 'University Heights' --and building_display_name != 'University Court'

(project_name ilike '%indust%'
or project_name ilike '%shop%'
or project_name ilike '%office%'
or project_name ilike '%school%'
or project_name ilike '%kinder%'
or project_name ilike '%commercial%'
or project_name ilike '%prison%'
or project_name ilike '%bank%'
or project_name ilike '%car%park%'
or project_name ilike '%consulate%'
or project_name ilike '%convention%'
or project_name ilike '%hospital%'
or project_name ilike '%factory%'

or building_name ilike '%indust%'
or building_name ilike '%shop%'
or building_name ilike '%office%'
or building_name ilike '%school%'
or building_name ilike '%kinder%'
or building_name ilike '%commercial%'
or building_name ilike '%prison%'
or building_name ilike '%bank%'
or building_name ilike '%car%park%'
or building_name ilike '%consulate%'
-- or building_name ilike '%convention%' -- and building_name != 'Convention Plaza Apartments'
or building_name ilike '%hospital%'
or building_name ilike '%factory%');
'''

-- need to backup and check the difference on property type distribution
select property_type_code , count(*)
from masterdata_hk.property
group by 1;
'''
comm	282190
condo	1595000
hos	510911
null 6
'''

with base as (
select 
	p.property_dwid , 
	case when p.property_type_code = 'condo' then 
		(case when project_display_name ilike '%Medical%'
			or (project_display_name ilike '%University%' and project_display_name != 'University Heights')
			or project_display_name ilike '%indust%'
			or project_display_name ilike '%shop%'
			or project_display_name ilike '%office%'
			or project_display_name ilike '%school%'
			or project_display_name ilike '%kinder%'
			or project_display_name ilike '%commercial%'
			or project_display_name ilike '%prison%'
			or project_display_name ilike '%bank%'
			or project_display_name ilike '%car%park%'
			or project_display_name ilike '%consulate%'
			or project_display_name ilike '%convention%'
			or project_display_name ilike '%hospital%'
			or project_display_name ilike '%factory%'
		
			or building_display_name ilike '%indust%'
			or building_display_name ilike '%shop%'
			or building_display_name ilike '%office%'
			or building_display_name ilike '%school%'
			or building_display_name ilike '%kinder%'
			or building_display_name ilike '%commercial%'
			or building_display_name ilike '%prison%'
			or building_display_name ilike '%bank%'
			or building_display_name ilike '%car%park%'
			or building_display_name ilike '%consulate%'
			or building_display_name ilike '%hospital%'
			or building_display_name ilike '%factory%'
			
			or address_unit ilike '%shop%'
			or address_unit ilike '%stall%'
			
			then 'comm'
			else p.property_type_code 
			end
		) 
		else p.property_type_code 
		end as property_type_code
from masterdata_hk.property p
	left join masterdata_hk.project pj on p.project_dwid = pj.project_dwid 
	left join masterdata_hk.building b on p.building_dwid = b.building_dwid 
)
select property_type_code , count(*)
from base group by 1;


'''
comm	282190 	--> 391018
condo	1595000 --> 1486172
hos		510911 	--> 510911
null 	6
'''

create table playground.property_cr_353_test as
with base as (
select 
	project_display_name, building_display_name,
	p.* , 
	case when p.property_type_code = 'condo' then 
		(case when project_display_name ilike '%Medical%'
			or (project_display_name ilike '%University%' and project_display_name != 'University Heights')
			or project_display_name ilike '%indust%'
			or project_display_name ilike '%shop%'
			or project_display_name ilike '%office%'
			or project_display_name ilike '%school%'
			or project_display_name ilike '%kinder%'
			or project_display_name ilike '%commercial%'
			or project_display_name ilike '%prison%'
			or project_display_name ilike '%bank%'
			or project_display_name ilike '%car%park%'
			or project_display_name ilike '%consulate%'
			or project_display_name ilike '%convention%'
			or project_display_name ilike '%hospital%'
			or project_display_name ilike '%factory%'
			or building_display_name ilike '%indust%'
			or building_display_name ilike '%shop%'
			or building_display_name ilike '%office%'
			or building_display_name ilike '%school%'
			or building_display_name ilike '%kinder%'
			or building_display_name ilike '%commercial%'
			or building_display_name ilike '%prison%'
			or building_display_name ilike '%bank%'
			or building_display_name ilike '%car%park%'
			or building_display_name ilike '%consulate%'
			or building_display_name ilike '%hospital%'
			or building_display_name ilike '%factory%'
			or address_unit ilike '%shop%'
			or address_unit ilike '%stall%'
			then 'comm'
			else p.property_type_code 
			end
		) 
		else p.property_type_code 
		end as new_property_type_code
from masterdata_hk.property p
	left join masterdata_hk.project pj on p.project_dwid = pj.project_dwid 
	left join masterdata_hk.building b on p.building_dwid = b.building_dwid 
)
select 
	project_display_name, building_display_name, address_unit , address_floor_text , address_stack , 
	property_type_code, new_property_type_code, data_source ,
	property_dwid , address_dwid , building_dwid , project_dwid 
from base 
where property_type_code != new_property_type_code --108,828
and ((address_unit not ilike '%shop%' and address_unit not ilike '%stall%') or address_unit isnull)
order by 1,2,3
;



-- change request begin:

select f_clone_table('masterdata_hk', 'property', 'masterdata_hk', 'z_property_backup', TRUE, TRUE);

select metadata.fn_create_change_request(
    'hk-update-property-type-using-keywords-2022-08-04', 'huying','huying'
); -- 353

call metadata.sp_add_change_table(353::int, 'hk', replace('property', '-', '_'));

insert into branch_hk.property_cr_353
with base as (
select 
	p.id , 
	case when p.property_type_code = 'condo' then 
		(case when project_display_name ilike '%Medical%'
			or (project_display_name ilike '%University%' and project_display_name != 'University Heights')
			or project_display_name ilike '%indust%'
			or project_display_name ilike '%shop%'
			or project_display_name ilike '%office%'
			or project_display_name ilike '%school%'
			or project_display_name ilike '%kinder%'
			or project_display_name ilike '%commercial%'
			or project_display_name ilike '%prison%'
			or project_display_name ilike '%bank%'
			or project_display_name ilike '%car%park%'
			or project_display_name ilike '%consulate%'
			or project_display_name ilike '%convention%'
			or project_display_name ilike '%hospital%'
			or project_display_name ilike '%factory%'
			or building_display_name ilike '%indust%'
			or building_display_name ilike '%shop%'
			or building_display_name ilike '%office%'
			or building_display_name ilike '%school%'
			or building_display_name ilike '%kinder%'
			or building_display_name ilike '%commercial%'
			or building_display_name ilike '%prison%'
			or building_display_name ilike '%bank%'
			or building_display_name ilike '%car%park%'
			or building_display_name ilike '%consulate%'
			or building_display_name ilike '%hospital%'
			or building_display_name ilike '%factory%'
			or address_unit ilike '%shop%'
			or address_unit ilike '%stall%'
			then 'comm'
			else p.property_type_code 
			end
		) 
		else p.property_type_code 
		end as new_property_type_code
from masterdata_hk.property p
	left join masterdata_hk.project pj on p.project_dwid = pj.project_dwid 
	left join masterdata_hk.building b on p.building_dwid = b.building_dwid 
)
select 
	p.id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	b.new_property_type_code as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code
from masterdata_hk.property p
left join base b on p.id = b.id
where p.property_type_code != b.new_property_type_code --108,828
and b.id notnull
;


update branch_hk.property_cr_353
set cr_record_action = 'update'
where id notnull; -- 108,828


insert into branch_hk.property_cr_353
select *
from masterdata_hk.property
where project_dwid = '5846e568e495ed4eff70848752ea2c0a'; 


call metadata.sp_submit_change_request(353, 'huying');

call metadata.sp_approve_change_request(353, 'huying');

call metadata.sp_merge_change_request(353);


-- change request end;

-- QA:
select property_type_code , count(*)
from masterdata_hk.property
group by 1;


-- split floor_seq into a feature table

create table feature_hk.de__property__floor_seq as 
select id as property_id, property_dwid , address_floor_num as floor_seq
from masterdata_hk.property
where address_floor_num notnull; -- 1,449,569


-- prepare clean address_floor_num

-- change request begin:
select metadata.fn_create_change_request(
    'hk-update-address-floor-num-deprecate-midland-floor-sequence-number-2022-08-04', 'huying','huying'
); -- 354


call metadata.sp_add_change_table(354::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_354
select 	
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	property_type_code,property_name,address_unit,address_floor_text,
	null as address_floor_num,
	address_stack,address_stack_num,ownership_type_code,bedroom_count,bathroom_count,other_room_count,
	net_floor_area_sqm,gross_floor_area_sqm,slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code
from masterdata_hk.property
where address_floor_num notnull; -- 1,449,569


update branch_hk.property_cr_354
set cr_record_action = 'update'
where id notnull;


call metadata.sp_submit_change_request(354, 'huying');

call metadata.sp_approve_change_request(354, 'huying');

call metadata.sp_merge_change_request(354);

-- change request end;



select p.project_dwid , p.building_dwid , p.address_dwid , p.property_dwid , p.address_unit , p.address_floor_text , seq.floor_seq  
from masterdata_hk.property p
	--left join masterdata_hk.project pj on p.project_dwid = pj.project_dwid 
	--left join masterdata_hk.building b on p.building_dwid = b.building_dwid 
	left join feature_hk.de__property__floor_seq seq on p.property_dwid = seq.property_dwid  
where p.address_unit notnull or p.address_floor_text notnull --2,364,409
order by 1,2,3,6
;


select p.address_floor_text , count(*)
from masterdata_hk.property p
group by 1

select p.project_dwid , p.building_dwid , p.address_dwid , p.property_dwid , p.address_unit , p.address_floor_text , seq.floor_seq  
from masterdata_hk.property p
left join feature_hk.de__property__floor_seq seq on p.property_dwid = seq.property_dwid  
where (p.address_unit notnull or p.address_floor_text notnull)
and address_floor_text = 'G' and  floor_seq notnull and floor_seq != '0' and floor_seq != '1'
order by 1,2,3,6,7;

select p.project_dwid , p.building_dwid , p.address_dwid , p.property_dwid , p.address_unit , p.address_floor_text , seq.floor_seq  
from masterdata_hk.property p
left join feature_hk.de__property__floor_seq seq on p.property_dwid = seq.property_dwid  
where  (p.address_unit notnull or p.address_floor_text notnull)
and project_dwid = 'eba1f610140e81b3cb49c51a11f3c532'
order by 1,2,3,6,7;


'''
M -> 0
G -> 0, 1
B, BASEMENT -> 0
UG -> 0, 1
RT -> max?
P -> 0
PH -> max?
LB -> 0?
LG -> 0?
TR -> max?
RETAIL-> 0?
G-1 -> 0?
PODIUM -> 0?
UB -> 0?
C -> 0? （court）
EZZANINE -> 0? 
UC -> 0? （court）
MTR -> 0?
SF -> 0?

LOWER -> 0?
UPPER -> 1? max?

35-38, 28-29, 7-8, 9-15 -> start from number, use the first number
G-B1, G-1, B-G -> start from character, tag as 0?

B1 -> -1
B2 -> -2
B3 -> -3
G1 -> -1
G2-> -2
G3 -> -3
P1-> -1
P2-> -2
LG1-> -1
LG2-> -2
CP -> 0
CP1-> -1
CP2-> -2
CP3 -> -3
CP4-> -4

'''

select 
	p.project_dwid , p.building_dwid , p.address_dwid ,
	p.address_floor_text , 
	case when address_floor_text in ('M', 'G', 'B', 'BASEMENT', 'UG', 'P', 'LB', 
									'LG', 'RETAIL', 'G-1', 'PODIUM', 'UB', 'C',
									'EZZANINE', 'UC', 'MTR', 'SF', 'CP') then '0'::int
		when address_floor_text in ('B1', 'G1', 'P1', 'LG1', 'CP1') then '-1'::int
		when address_floor_text in ('B2', 'G2', 'P2', 'LG2', 'CP2') then '-2'::int
		when address_floor_text in ('B3', 'G3', 'CP3') then '-3'::int
		when address_floor_text in ('CP4') then '-4'::int
	when address_floor_text not ilike '%-%' and address_floor_text ~ '^[0-9]' and address_floor_text ~ '[0-9]$'
	then address_floor_text::int
	end as new_address_floor_num
from masterdata_hk.property p
where address_floor_text notnull
order by 1,2,3,4;


-- change request begin:
select metadata.fn_create_change_request(
    'hk-update-address-floor-num-2022-08-04', 'huying','huying'
); -- 355


call metadata.sp_add_change_table(355::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_355
select 	
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	property_type_code,property_name,address_unit,address_floor_text,
	case when address_floor_text in ('M', 'G', 'B', 'BASEMENT', 'UG', 'P', 'LB', 
									'LG', 'RETAIL', 'G-1', 'PODIUM', 'UB', 'C',
									'EZZANINE', 'UC', 'MTR', 'SF', 'CP') then '0'::int
		when address_floor_text in ('B1', 'G1', 'P1', 'LG1', 'CP1') then '-1'::int
		when address_floor_text in ('B2', 'G2', 'P2', 'LG2', 'CP2') then '-2'::int
		when address_floor_text in ('B3', 'G3', 'CP3') then '-3'::int
		when address_floor_text in ('CP4') then '-4'::int
	when address_floor_text not ilike '%-%' and address_floor_text ~ '^[0-9]' and address_floor_text ~ '[0-9]$'
	then address_floor_text::int
	end as address_floor_num,
	address_stack,address_stack_num,ownership_type_code,bedroom_count,bathroom_count,other_room_count,
	net_floor_area_sqm,gross_floor_area_sqm,slug,country_code,is_active,property_display_text,
	data_source,data_source_id,status_code, 'update' as cr_record_action
from masterdata_hk.property
where address_floor_text notnull; -- 2,364,409


select *
from branch_hk.property_cr_355
where address_floor_text ilike '%-%'; -- 261


select *
from branch_hk.property_cr_355
where address_floor_num isnull
and address_floor_text ~ '^[0-9]';


with fill1 as (
select building_dwid, sum(case when address_floor_num notnull then 1 else 0 end)*1.0/count(*) as fill_rate
from branch_hk.property_cr_355
group by 1
)
, fill2_base as (
select building_dwid
from fill1 where fill_rate < 1 --758
)
, max_floor_number as (
select 
	a.project_dwid , a.building_dwid , 
	max(a.address_floor_num) as max_floor_num
from branch_hk.property_cr_355 a
left join fill2_base b on a.building_dwid = b.building_dwid
where b.building_dwid notnull 
group by 1,2
)
, fill2_temp as (
select 
	a.project_dwid , a.building_dwid , 
	a.address_floor_text , a.address_floor_num, 
	row_number() over (partition by a.project_dwid , a.building_dwid order by a.address_floor_num) as seq,
	c.max_floor_num
from branch_hk.property_cr_355 a
left join fill2_base b on a.building_dwid = b.building_dwid
left join max_floor_number c on coalesce(a.project_dwid, '') = coalesce(c.project_dwid, '') and a.building_dwid = c.building_dwid
where b.building_dwid notnull -- 39,006 --> 8594
group by 1,2,3,4,6
order by project_dwid , building_dwid , address_floor_num  
)
, fill2 as ( 
select 
	project_dwid, building_dwid, address_floor_text,
	case when address_floor_num notnull then address_floor_num
		when address_floor_num isnull and seq >= max_floor_num then seq
		when address_floor_num isnull and seq < max_floor_num then max_floor_num + 1
		end as address_floor_num,
	seq, max_floor_num
from fill2_temp
order by 1,2,4
)
, fill2_final as (
select a.property_dwid , b.address_floor_num
from branch_hk.property_cr_355 a
left join fill2 b on coalesce(a.project_dwid, '') = coalesce(b.project_dwid, '') and a.building_dwid = b.building_dwid and a.address_floor_text = b.address_floor_text
where a.address_floor_num isnull and b.address_floor_num notnull --505
)
update branch_hk.property_cr_355 a 
set address_floor_num = b.address_floor_num
from fill2_final b where a.property_dwid = b.property_dwid
and a.address_floor_num isnull; -- 403 + 1066


select a.property_dwid , count(b.address_floor_num) as cnt
from branch_hk.property_cr_355 a
left join fill2 b on a.project_dwid = b.project_dwid and a.building_dwid = b.building_dwid and a.address_floor_text = b.address_floor_text
where a.address_floor_num isnull and b.address_floor_num notnull
group by 1 having count(b.address_floor_num) > 1

call metadata.sp_submit_change_request(355, 'huying');

call metadata.sp_approve_change_request(355, 'huying');

-- call metadata.sp_merge_change_request(355);

update masterdata_hk.property a
set property_type_code = b.property_type_code , 
	address_floor_num = b.address_floor_num 
from branch_hk.property_cr_355 b where a.property_dwid = b.property_dwid 
and b.property_dwid notnull 
and a.id >= 581000000 --between 580000000 and 581000000
;

select count(*) from masterdata_hk.property p ; --2388107

select max(id), min(id)
from masterdata_hk.property
-- 582025738	579635642

select sum(case when address_floor_num notnull then 1 else 0 end)*1.0/count(*)
from masterdata_hk.property p; -- 0.99006660924322067646

select address_floor_text ilike '%' || address_floor_num || '%' as floor_match, count(*)
from masterdata_hk.property p 
group by 1;

select property_type_code , count(*) from masterdata_hk.property
group by 1;





--------------------------------------------------------------------------------------------
-- 3. clean address duplicates from phase
--------------------------------------------------------------------------------------------
-- 

select address_street_text , address_building , count(*)
from masterdata_hk.address a  
where address_type_code = 'point-address'
and (is_active = true or is_active isnull)
group by 1,2 having count(*) > 1; -- 4470 --> 1573

select address_street_text , address_building , development , city_subarea , count(*)
from masterdata_hk.address a  
where address_type_code = 'point-address'
and (is_active = true or is_active isnull)
group by 1,2,3,4 having count(*) > 1; -- 2998 -- 102

select address_street_text , address_building , development_phase , development , city_subarea , count(*)
from masterdata_hk.address a  
where address_type_code = 'point-address'
--and (is_active = true or is_active isnull)
group by 1,2,3,4,5 having count(*) > 1; -- 4470 --> 1573 --> 349 --> 182 --> 0



SELECT x.* FROM masterdata_hk.address x WHERE id in (600012315, 600028796);
SELECT x.* FROM masterdata_hk.address x WHERE id in (600013956, 600031056);
SELECT x.* FROM masterdata_hk.address x WHERE id in (600049467, 600054211);


with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'yoho town'
order by a.address_street_text , a.address_building, a.development_phase  
;

select * from masterdata_hk.property p 
where address_dwid in ('128983a79977c50e59c273e19b039672', '00f5765049ca9767698e935edfc21c6e') ;

select * from map_hk.midland_building_to_dwid mbtd 
where address_dwid in ('128983a79977c50e59c273e19b039672', '00f5765049ca9767698e935edfc21c6e') ;


select * from map_hk.midland_unit_to_dwid mutd 
where address_dwid in ('128983a79977c50e59c273e19b039672', '00f5765049ca9767698e935edfc21c6e') ;


select * from map_hk.midland_sale_txn__map mstm 
where address_dwid in ('128983a79977c50e59c273e19b039672', '00f5765049ca9767698e935edfc21c6e');


-----------

select p.*
from masterdata_hk.property p 
left join branch_hk.address_cr_358 m on p.address_dwid = m.address_dwid 
where m.address_dwid notnull;


with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen'
and a.development_phase notnull
)
--, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
select false as correct_addressid, p.*
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
where m.wrong_addressid notnull
;

select true as correct_addressid, p.*
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.correct_addressid
where m.correct_addressid notnull
;

union 
select false as correct_addressid, p.*
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
where m.wrong_addressid notnull
;


'''
to do:
-- , , , , , , , , , , 
-- cheung on estate, deerhill bay, lei tung estate, savanna garden, seaside sonata, 
-- other buildings


waiting the change request to be merged:
projects indeed could have same address number, street name and building name, but different phase name, they do not need to dedup:
-- avignon, crown of st. barths, riva, the vineyard, valais ii, 

delete dups in address, no relationships in other tables:
-- celestial heights, 


delete dups in address, delete/update corresponding records in building, property and mapping table:
-- fairview park, hong lok yuen, lohas park, yoho town


'''

-- delete dups records without phase info
-- remember to update address_dwids and id relationships in building / property / sale_transaction and 3 mapping tables

-- drop FK in centanet map table first
ALTER TABLE map_hk.hk_centanet_clean_unit DROP CONSTRAINT centanet_unit_to_address_fk;
ALTER TABLE map_hk.hk_centanet_clean_unit DROP CONSTRAINT centanet_unit_to_property_fk;
ALTER TABLE map_hk.hk_centanet_clean_unit DROP CONSTRAINT centanet_unit_to_building_fk;
ALTER TABLE map_hk.hk_centanet_hkpost_correction_backfill DROP CONSTRAINT centanet_to_building_fk;


'''DONE'''
-- fix address table and remember to add the FK back at the end 
ALTER TABLE map_hk.hk_centanet_clean_unit ADD CONSTRAINT centanet_unit_to_address_fk FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);
ALTER TABLE map_hk.hk_centanet_clean_unit ADD CONSTRAINT centanet_unit_to_property_fk FOREIGN KEY (property_dwid) REFERENCES masterdata_hk.property(property_dwid);
ALTER TABLE map_hk.hk_centanet_clean_unit ADD CONSTRAINT centanet_unit_to_building_fk FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid)
ALTER TABLE map_hk.hk_centanet_hkpost_correction_backfill ADD CONSTRAINT centanet_to_building_fk FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid)


select metadata.fn_create_change_request(
    'hk-dedup-address-from-phase-2022-08-05', 'huying','huying'
); -- 358

call metadata.sp_add_change_table(358::int, 'hk', replace('address', '-', '_'));


insert into branch_hk.address_cr_358
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
select a.*, 'delete' as cr_record_action
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'yoho town'--'lohas park'--'hong lok yuen'--'fairview park' --'celestial heights' 
and a.development_phase isnull 
;

insert into branch_hk.address_cr_358
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
select 
	id,address_dwid,address_type_code,address_type_attribute,
	lower(trim(trim(coalesce(a.address_street_text,'')||','||coalesce(a.address_building,'')||','||coalesce(development_phase,'')||','|| 
		coalesce(a.development,'')||','||coalesce(city_subarea,'')||','||coalesce(city_area,'')||','||city||','||metro_area, ','))) as full_address_text,
	initcap(trim(trim(coalesce(a.address_street_text,'')||','||coalesce(a.address_building,'')||','||coalesce(development_phase,'')||','|| 
		coalesce(a.development,'')||','||coalesce(city_subarea,'')||','||coalesce(city_area,'')||','||city||','||metro_area, ','))) as address_display_text,
	a.address_building,address_number,address_number_range,address_number_range_start,address_number_range_end,
	a.address_street_text,street_type,street_prefix,street_prefix_type,street_side,street_name,street_name_root,street_suffix,
	postal_code_ext,postal_code,a.development,development_code,development_phase,development_phase_number,
	neighborhood_block,neighborhood_block_code,neighborhood_section,neighborhood,neighborhood_code,neighborhood_group,
	city_subarea,city_subarea_id,city_subarea_code,city_district,city_area,city_area_id,city_area_code,city,city_id,city_code,
	metro_area,metro_area_code,metro_area_district,region_admin_subdistrict,region_admin_subdistrict_code,region_admin_district,
	region_admin_district_id,region_admin_district_code,region_state_province,region_state_province_code,region_state_province_alias,
	territory,territory_code,country,country_code,country_3code,geographic_zone,continent,latitude,longitude,location_marker,slug,
	status_code,language_code,data_source_count,hash_key,dw_address_id,data_source,data_source_id,is_active,
	'update' as cr_record_action
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'crown of st. barths'
and split_part(a.full_address_text, ',', 3) = ''
;



select metadata.fn_create_change_request(
    'hk-update-addressdwid-in-property-to-dedup-address-from-phase-2022-08-05', 'huying','huying'
); -- 359

call metadata.sp_add_change_table(359::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_359
select p.*, 'delete' as cr_record_action
from masterdata_hk.property p 
left join branch_hk.address_cr_358 ac on p.address_dwid = ac.address_dwid 
where ac.development = 'yoho town'--'lohas park'--'fairview park'
;


insert into branch_hk.property_cr_359
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen'
and a.development_phase notnull
)
, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
select 
	p.id,
	p.property_dwid,
	m.correct_addressid as address_dwid,
	b.building_dwid,
	p.unit_group_dwid,
	b.project_dwid,
	property_type_code,property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	p.slug,p.country_code,p.is_active,property_display_text,
	p.data_source,p.data_source_id,p.status_code,
	'update' as cr_record_action
from masterdata_hk.property p 
left join branch_hk.address_cr_358 ac on p.address_dwid = ac.address_dwid 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
left join masterdata_hk.building b on m.correct_addressid = b.address_dwid
where ac.development = 'hong lok yuen'
;





select metadata.fn_create_change_request(
    'hk-update-addressdwid-in-building-to-dedup-address-from-phase-2022-08-08', 'huying','huying'
); -- 365

call metadata.sp_add_change_table(365::int, 'hk', replace('building', '-', '_'));


insert into branch_hk.building_cr_365
select b.*, 'delete' as cr_record_action
from masterdata_hk.building b 
left join branch_hk.address_cr_358 ac on b.address_dwid = ac.address_dwid 
where ac.development = 'yoho town'--'fairview park'--'lohas park' --'hong lok yuen'
;





create table branch_hk.midland_building_to_dwid_cr_10 as 

with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'fairview park'--'yoho town'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'fairview park'--'yoho town'
and a.development_phase notnull
)
, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
select 
	p.id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,
	estate_name,estate_id,phase_name,phase_id,p.building_name,building_id,p.address_number,p.address_street,
	b.building_dwid,
	m.correct_addressid as address_dwid,
	b.project_dwid,
	p.lot_group_dwid
from map_hk.midland_building_to_dwid p
left join branch_hk.address_cr_358 ac on p.address_dwid = ac.address_dwid 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
left join masterdata_hk.building b on m.correct_addressid = b.address_dwid
where ac.development = 'fairview park'--'yoho town';


update map_hk.midland_building_to_dwid a
set building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_building_to_dwid_cr_10 b
where a.id = b.id; 



create table branch_hk.midland_unit_to_dwid_cr_10 as 

with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'fairview park'--'yoho town'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'fairview park'--'yoho town'
and a.development_phase notnull
)
, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
, wrong_propertyid as (
select distinct m.wrong_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
where m.wrong_addressid notnull
)
, correct_propertyid as (
select distinct m.correct_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.correct_addressid
where m.correct_addressid notnull
)
, propertyid_update_map as (
select a.property_dwid as wrong_property_dwid, c.property_dwid as correct_property_dwid
from wrong_propertyid a 
left join addressid_update_map b on a.wrong_addressid = b.wrong_addressid
left join correct_propertyid c on b.correct_addressid = c.correct_addressid and a.address_unit = c.address_unit --2167
)
select 
	p.id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,
	estate_name,estate_id,phase_name,phase_id,building_name,building_id,address_number,address_street,floor,stack,unit_id,
	pm.correct_property_dwid as property_dwid,
	pp.building_dwid,
	pp.address_dwid,
	pp.project_dwid,
	p.lot_group_dwid
from map_hk.midland_unit_to_dwid p
left join propertyid_update_map pm on p.property_dwid = pm.wrong_property_dwid
left join masterdata_hk.property pp on pm.correct_property_dwid = pp.property_dwid 
where p.property_dwid notnull and pm.wrong_property_dwid notnull
union
select 
	p.id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,
	estate_name,estate_id,phase_name,phase_id,p.building_name,building_id,p.address_number,address_street,floor,stack,unit_id,
	p.property_dwid,
	b.building_dwid,
	am.correct_addressid as address_dwid,
	b.project_dwid,
	p.lot_group_dwid
from map_hk.midland_unit_to_dwid p
left join addressid_update_map am on p.address_dwid = am.wrong_addressid
left join masterdata_hk.building b on am.correct_addressid = b.address_dwid
where p.property_dwid isnull and p.address_dwid notnull and am.wrong_addressid notnull
;


update map_hk.midland_unit_to_dwid a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_unit_to_dwid_cr_10 b
where a.id = b.id; 




create table branch_hk.midland_sale_txn__map_cr_14 as 
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'fairview park'--'yoho town'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'fairview park'--'yoho town'
and a.development_phase notnull
)
, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
, wrong_propertyid as (
select distinct m.wrong_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
where m.wrong_addressid notnull
)
, correct_propertyid as (
select distinct m.correct_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.correct_addressid
where m.correct_addressid notnull
)
, propertyid_update_map as (
select a.property_dwid as wrong_property_dwid, c.property_dwid as correct_property_dwid
from wrong_propertyid a 
left join addressid_update_map b on a.wrong_addressid = b.wrong_addressid
left join correct_propertyid c on b.correct_addressid = c.correct_addressid and coalesce(a.address_unit, '') = coalesce(c.address_unit, '') --2167
)
select 
	p.data_uuid,activity_dwid,
	pm.correct_property_dwid as property_dwid,
	pp.building_dwid,
	pp.address_dwid,
	pp.project_dwid,
	p.land_parcel_dwid,
	p.lot_group_dwid,
	p.status_code
from map_hk.midland_sale_txn__map p
left join propertyid_update_map pm on p.property_dwid = pm.wrong_property_dwid
left join masterdata_hk.property pp on pm.correct_property_dwid = pp.property_dwid 
where p.property_dwid notnull and pm.wrong_property_dwid notnull
;

update map_hk.midland_sale_txn__map a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_14 b
where a.data_uuid = b.data_uuid; 




----

create table branch_hk.midland_sale_txn__map_cr_15 as 
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen' --'fairview park'--'yoho town'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen' --'fairview park'--'yoho town'
and a.development_phase notnull
)
, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
, wrong_propertyid as (
select distinct m.wrong_addressid, p.address_unit, p.property_dwid  
from branch_hk.property_cr_359 p 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
where m.wrong_addressid notnull
)
, correct_propertyid as (
select distinct m.correct_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.correct_addressid
where m.correct_addressid notnull
)
, propertyid_update_map as (
select a.property_dwid as wrong_property_dwid, c.property_dwid as correct_property_dwid
from wrong_propertyid a 
left join addressid_update_map b on a.wrong_addressid = b.wrong_addressid
left join correct_propertyid c on b.correct_addressid = c.correct_addressid and coalesce(a.address_unit, '') = coalesce(c.address_unit, '') --2167
)
select 
	p.data_uuid,activity_dwid,
	pm.correct_property_dwid as property_dwid,
	pp.building_dwid,
	pp.address_dwid,
	pp.project_dwid,
	p.land_parcel_dwid,
	p.lot_group_dwid,
	p.status_code
from map_hk.midland_sale_txn__map p
left join propertyid_update_map pm on p.property_dwid = pm.wrong_property_dwid
left join masterdata_hk.property pp on pm.correct_property_dwid = pp.property_dwid 
where p.property_dwid notnull and pm.wrong_property_dwid notnull
;

update map_hk.midland_sale_txn__map a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_15 b
where a.data_uuid = b.data_uuid; 


----


select metadata.fn_create_change_request(
    'hk-update-addressdwid-in-sale-transaction-to-dedup-address-from-phase-2022-08-08', 'huying','huying'
); -- 366

call metadata.sp_add_change_table(366::int, 'hk', replace('sale_transaction', '-', '_'));


insert into branch_hk.sale_transaction_cr_366
select
	st.id,st.activity_dwid,st.property_dwid,pc.address_dwid,pc.building_dwid,pc.project_dwid,st.lot_group_dwid,
	current_lot_group_dwid,activity_name,units_sold,sale_type,sale_subtype,property_completion_year,
	st.property_type_code,st.address_unit,
	a.full_address_text as address_local_text,pj.tenure_code,st.bathroom_count,st.bedroom_count,
	st.gross_floor_area_sqm,st.net_floor_area_sqm,st.land_area_sqm,contract_date,settlement_date,purchase_amount,
	st.country_code,st.data_uuid,st.data_source_uuid,st.data_source,'update' as cr_record_action
from masterdata_hk.sale_transaction st 
left join branch_hk.property_cr_359 pc on st.property_dwid = pc.property_dwid 
left join masterdata_hk.address a on pc.address_dwid = a.address_dwid 
left join masterdata_hk.project pj on pc.project_dwid = pj.project_dwid 
where pc.cr_record_action = 'update';


insert into branch_hk.sale_transaction_cr_366
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'yoho town' --'fairview park'
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'yoho town' --'fairview park'
and a.development_phase notnull
)
, addressid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
)
, wrong_propertyid as (
select distinct m.wrong_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.wrong_addressid
where m.wrong_addressid notnull
)
, correct_propertyid as (
select distinct m.correct_addressid, p.address_unit, p.property_dwid  
from masterdata_hk.property p 
left join addressid_update_map m on p.address_dwid = m.correct_addressid
where m.correct_addressid notnull
)
, propertyid_update_map as (
select a.property_dwid as wrong_property_dwid, c.property_dwid as correct_property_dwid
from wrong_propertyid a 
left join addressid_update_map b on a.wrong_addressid = b.wrong_addressid
left join correct_propertyid c on b.correct_addressid = c.correct_addressid and coalesce(a.address_unit, '') = coalesce(c.address_unit, '') --2167
)
select 
	st.id,st.activity_dwid,
	pm.correct_property_dwid as property_dwid,
	pp.address_dwid,
	pp.building_dwid,
	pp.project_dwid,
	st.lot_group_dwid,
	current_lot_group_dwid,activity_name,units_sold,sale_type,sale_subtype,
	c.completion_year::int as property_completion_year,
	st.property_type_code,st.address_unit,
	a.full_address_text as address_local_text,
	pj.tenure_code,
	st.bathroom_count,st.bedroom_count,
	st.gross_floor_area_sqm,st.net_floor_area_sqm,st.land_area_sqm,contract_date,settlement_date,purchase_amount,
	st.country_code,st.data_uuid,st.data_source_uuid,st.data_source,
	'update' as cr_record_action
from masterdata_hk.sale_transaction st
left join propertyid_update_map pm on st.property_dwid = pm.wrong_property_dwid
left join masterdata_hk.property pp on pm.correct_property_dwid = pp.property_dwid 
left join masterdata_hk.address a on pp.address_dwid = a.address_dwid 
left join masterdata_hk.project pj on pp.project_dwid = pj.project_dwid 
left join feature_hk.de__building__completion_year_info c on pp.building_dwid = c.building_dwid 
where st.property_dwid notnull and pm.wrong_property_dwid notnull



select st.*
from masterdata_hk.sale_transaction st
left join branch_hk.property_cr_359 pc on st.property_dwid = pc.property_dwid 
where pc.cr_record_action = 'delete' and pc.project_dwid != '93d0845e66dd772fe7a16a4333fd0915';



CREATE TABLE change_log_hk.sale_transaction_change_log (
	id int8 NULL,
	activity_dwid text NULL,
	property_dwid text NULL,
	address_dwid text NULL,
	building_dwid text NULL,
	project_dwid text NULL,
	lot_group_dwid text NULL,
	current_lot_group_dwid text NULL,
	activity_name text NULL,
	units_sold int4 NULL,
	sale_type text NULL,
	sale_subtype text NULL,
	property_completion_year int2 NULL,
	property_type_code text NULL,
	address_unit text NULL,
	address_local_text text NULL,
	tenure_code text NULL,
	bathroom_count int4 NULL,
	bedroom_count int4 NULL,
	gross_floor_area_sqm numeric NULL,
	net_floor_area_sqm numeric NULL,
	unit_price_psm numeric NULL,
	contract_date date NULL,
	settlement_date date NULL,
	purchase_amount numeric NULL,
	country_code text NULL,
	data_uuid text NULL,
	data_source_uuid text NULL,
	data_source text NULL,
	cr_record_action varchar NOT NULL,
	cr_action_date date NULL,
	cr_id int4 NULL,
	cr_change_id int4 NOT NULL,
	CONSTRAINT sale_transaction_change_log_pk PRIMARY KEY (cr_change_id)
);



select a.*
from feature_hk.de__building__completion_year_info a 
left join branch_hk.building_cr_365 b on a.building_dwid = b.building_dwid 
where b.building_dwid notnull;

--create table branch_hk.de__building__completion_year_info_cr_1 as 
insert into branch_hk.de__building__completion_year_info_cr_1
with base as (
select address_street_text , address_building , development , count(*)
from masterdata_hk.address a  where address_type_code = 'point-address'
group by 1,2,3 having count(*) > 1
)
, wrong_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen'--'fairview park'--'yoho town' 
and a.development_phase isnull 
)
, correct_addressid as (
select a.*
from masterdata_hk.address a
left join base b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
where b.development = 'hong lok yuen'--'fairview park'--'yoho town' 
and a.development_phase notnull
)
, buildingid_update_map as (
select a.address_dwid as wrong_addressid, b.address_dwid as correct_addressid,
bd1.building_dwid as wrong_building_dwid, bd2.building_dwid as correct_building_dwid
from wrong_addressid a
left join correct_addressid b on a.address_street_text = b.address_street_text and a.address_building = b.address_building
left join masterdata_hk.building bd1 on a.address_dwid = bd1.address_dwid 
left join masterdata_hk.building bd2 on b.address_dwid = bd2.address_dwid 
)
select b.wrong_building_dwid as building_dwid, b.correct_building_dwid,'update' as cr_record_action
from feature_hk.de__building__completion_year_info a
left join buildingid_update_map b on a.building_dwid = b.wrong_building_dwid
where b.wrong_building_dwid notnull
;


with base as  (
select a.building_dwid 
from branch_hk.de__building__completion_year_info_cr_1 a 
left join feature_hk.de__building__completion_year_info b on a.correct_building_dwid = b.building_dwid 
where b.building_dwid notnull
)
update branch_hk.de__building__completion_year_info_cr_1 a
set cr_record_action = 'delete'
from base b where a.building_dwid = b.building_dwid 


update feature_hk.de__building__completion_year_info a
set building_dwid = b.correct_building_dwid
from branch_hk.de__building__completion_year_info_cr_1 b
where a.building_dwid = b.building_dwid and b.cr_record_action = 'update'
;


delete from feature_hk.de__building__completion_year_info
where building_dwid in (select building_dwid from branch_hk.de__building__completion_year_info_cr_1);


select a.*
from feature_hk.de__building__phase_info a 
left join branch_hk.building_cr_365 b on a.building_dwid = b.building_dwid 
where b.building_dwid notnull; -- 0




create table branch_hk.midland_sale_txn__map_cr_16 as 
select
	a.data_uuid,a.activity_dwid,a.property_dwid,
	b.correct_building_dwid as building_dwid,
	c.address_dwid,
	c.project_dwid,
	a.land_parcel_dwid,a.lot_group_dwid,a.status_code
from map_hk.midland_sale_txn__map a
left join branch_hk.de__building__completion_year_info_cr_1 b on a.building_dwid = b.building_dwid  
left join masterdata_hk.building c on b.correct_building_dwid  = c.building_dwid 
where b.building_dwid notnull; -- 24

update map_hk.midland_sale_txn__map a
set --property_dwid = b.property_dwid, 
building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_16 b
where a.data_uuid = b.data_uuid; -- 24

select a.*
from map_hk.midland_building_to_dwid a 
left join branch_hk.de__building__completion_year_info_cr_1 b on a.building_dwid = b.building_dwid  
where b.building_dwid notnull; -- 0

select a.*
from map_hk.midland_unit_to_dwid a 
left join branch_hk.de__building__completion_year_info_cr_1 b on a.building_dwid = b.building_dwid  
where b.building_dwid notnull; -- 0



-- sale transaction change request [DONE]
call metadata.sp_submit_change_request(366, 'huying');

call metadata.sp_approve_change_request(366, 'huying');

call metadata.sp_merge_change_request(366);


-- property change request [DONE]
call metadata.sp_submit_change_request(359, 'huying');

call metadata.sp_approve_change_request(359, 'huying');

call metadata.sp_merge_change_request(359);


-- building change request [DONE]
call metadata.sp_submit_change_request(365, 'huying');

call metadata.sp_approve_change_request(365, 'huying');

call metadata.sp_merge_change_request(365); 


select f_clone_table('masterdata_hk', 'building', 'masterdata_hk', 'z_building_backup', TRUE, TRUE);

--CREATE UNIQUE INDEX building_un_dwid ON masterdata_hk.building USING btree (building_dwid)

ALTER TABLE masterdata_hk.building DROP CONSTRAINT building_to_address_fk;
ALTER TABLE masterdata_hk.building DROP CONSTRAINT building_to_project_fk;
--ALTER TABLE masterdata_hk.building DROP CONSTRAINT building_un_slug;
--ALTER TABLE masterdata_hk.building DROP CONSTRAINT building_un_dwid;
ALTER TABLE masterdata_hk.building DROP CONSTRAINT building_pk_id;

--ALTER TABLE masterdata_hk.building ADD CONSTRAINT building_un_slug UNIQUE (slug);
--ALTER TABLE masterdata_hk.building ADD CONSTRAINT bvuilding_un_dwid UNIQUE (building_dwid);
ALTER TABLE masterdata_hk.building ADD CONSTRAINT building_pk_id PRIMARY KEY (id);
ALTER TABLE masterdata_hk.building ADD CONSTRAINT building_to_address_fk FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);
ALTER TABLE masterdata_hk.building ADD CONSTRAINT building_to_project_fk FOREIGN KEY (project_dwid) REFERENCES masterdata_hk.project(project_dwid);

--delete from masterdata_hk.building where id in (select distinct id from branch_hk.building_cr_365 where id < 66152565);

--delete from masterdata_hk.building a where exists (select 1 from branch_hk.building_cr_365 b where a.id = b.id);  

--delete from masterdata_hk.building where building_dwid = '0e81eb334d30966cfc2b4252cb3ec3b7';

update masterdata_hk.building a
set is_active = false 
from branch_hk.building_cr_365 b where a.id = b.id; -- 2823

-- then delete 'is_active = false' records mannually

select count(*) from masterdata_hk.building; --63334 --> 
select count(*) from masterdata_hk.z_building_backup; --63536

-- delete from masterdata_hk.building where is_active = false

-- 7b7ac54a11c14f71f5b45c62e1716a37

begin;
explain (analyze,buffers,timing)
delete from masterdata_hk.building where is_active = false;
rollback;

-- address change request [in progress]
call metadata.sp_submit_change_request(358, 'huying');

call metadata.sp_approve_change_request(358, 'huying');

call metadata.sp_merge_change_request(358); -- to do


update masterdata_hk.address a
set full_address_text = b.full_address_text , address_display_text = b.address_display_text 
from branch_hk.address_cr_358 b where a.id = b.id
and b.cr_record_action = 'update'; -- 19

update masterdata_hk.address a
set is_active = false 
from branch_hk.address_cr_358 b where a.id = b.id
and b.cr_record_action = 'delete'; -- 2899

delete from masterdata_hk.address where is_active = false;



-- add the FK back
ALTER TABLE map_hk.hk_centanet_clean_unit ADD CONSTRAINT centanet_unit_to_address_fk FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);

with base as (
select a.building_id, c.correct_building_dwid, b2.address_dwid , b2.project_dwid  
from map_hk.hk_centanet_hkpost_correction_backfill a
left join masterdata_hk.building b on a.building_dwid = b.building_dwid 
left join branch_hk.de__building__completion_year_info_cr_1 c on a.building_dwid  = c.building_dwid 
left join masterdata_hk.building b2 on c.correct_building_dwid = b2.building_dwid 
where b.building_dwid isnull and a.building_dwid notnull -- 107
)
update map_hk.hk_centanet_hkpost_correction_backfill a
set address_dwid = b.address_dwid, building_dwid = b.correct_building_dwid, project_dwid = b.project_dwid
from base b where a.building_id = b.building_id; --107

ALTER TABLE map_hk.hk_centanet_hkpost_correction_backfill ADD CONSTRAINT centanet_to_building_fk FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid);


with base as (
select distinct building_id , address_dwid , building_dwid , project_dwid 
from map_hk.hk_centanet_hkpost_correction_backfill
where address_dwid notnull or building_dwid notnull or project_dwid notnull
)
update map_hk.hk_centanet_clean_unit a
set address_dwid = b.address_dwid, building_dwid = b.building_dwid, project_dwid = b.project_dwid
from base b where a.building_id = b.building_id; -- 


ALTER TABLE map_hk.hk_centanet_clean_unit ADD CONSTRAINT centanet_unit_to_building_fk FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid);


-- update property_dwids not existing now

select a.*
from map_hk.hk_centanet_clean_unit a
left join masterdata_hk.property p on a.property_dwid = p.property_dwid 
where p.property_dwid isnull and a.property_dwid notnull;

update map_hk.hk_centanet_clean_unit
set property_dwid = null 
where unit_id = 'BWPPWGDXEKBP';

ALTER TABLE map_hk.hk_centanet_clean_unit ADD CONSTRAINT centanet_unit_to_property_fk FOREIGN KEY (property_dwid) REFERENCES masterdata_hk.property(property_dwid);





--------------------------------------------------------------------------------------------
-- 3. commercial property type v.s. bedroom count
--------------------------------------------------------------------------------------------

-- If bedroom count not null, the unit should not be commercial type

select bedroom_count notnull as has_bedroom, property_type_code , count(*)
from masterdata_hk.property p 
group by 1,2;
-- true		comm	3230


select *
from masterdata_hk.property
where property_type_code = 'comm' and bedroom_count notnull and bedroom_count != 0 --1769
order by project_dwid , building_dwid 


select metadata.fn_create_change_request(
    'hk-update-property-type-with-notnull-bedroom-count-2022-08-11', 'huying','huying'
);-- 410

call metadata.sp_add_change_table(410::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_410 
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	'condo' as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,
	'update' as cr_record_action
from masterdata_hk.property
where project_dwid = '02db6b90397afd0b345ca23199329a24' and property_type_code = 'comm';


insert into branch_hk.property_cr_410 
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	'condo' as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,
	'update' as cr_record_action
from masterdata_hk.property
where project_dwid = '33b54bc00760ab98c8a1ca3020559cc2' and building_dwid in ('325d46f658d42cd3adf80994a11c445d')
and property_type_code = 'comm';


insert into branch_hk.property_cr_410 
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	'condo' as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,
	'update' as cr_record_action
from masterdata_hk.property
where project_dwid = '7855db0a41134ae4dc162c86994dcdb7' 
and building_dwid not in ('57ef5d886ceab5057c387964d1e78e82', '67db8ad8a79beae34bf5ef14fa7e782a', 'b65162047f021f6304fd1c9bc567d17e')
and property_type_code = 'comm';



insert into branch_hk.property_cr_410 
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	'condo' as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,
	'update' as cr_record_action
from masterdata_hk.property
where project_dwid in ('8257d3dad08837ffe904834fb4f0cea4', 'e18b85dc6f05a94f5e059341338a7fec') 
and property_type_code = 'comm';


insert into branch_hk.property_cr_410 
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	'condo' as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,
	'update' as cr_record_action
from masterdata_hk.property
where project_dwid = 'bb6b4042ec07a29d07aa257d14450988' 
and property_type_code = 'comm';


insert into branch_hk.property_cr_410 
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	'condo' as property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,
	'update' as cr_record_action
from masterdata_hk.property
where building_dwid = '1f55c22c80bee5e74f97cc56a0b8b7e1'
and property_type_code = 'comm';


call metadata.sp_submit_change_request(410, 'huying');

call metadata.sp_approve_change_request(410, 'huying');

call metadata.sp_merge_change_request(410);

'''
SQL Error [42703]: ERROR: column "address_lot_number" of relation "property_change_log" does not exist
  Where: PL/pgSQL function metadata.sp_append_new_change_logs(character varying,character varying) line 26 at EXECUTE
SQL statement "CALL metadata.sp_append_new_change_logs(v_change_table, v_change_log_table)"
PL/pgSQL function metadata.sp_merge_change_request(integer) line 64 at CALL
'''

-- maunally do this process

select f_clone_table('branch_hk', 'address_cr_358', 'backup_hk', 'address_cr_358', TRUE, TRUE);
select f_clone_table('branch_hk', 'building_cr_365', 'backup_hk', 'building_cr_365', TRUE, TRUE);
select f_clone_table('branch_hk', 'de__building__completion_year_info_cr_1', 'backup_hk', 'de__building__completion_year_info_cr_1', TRUE, TRUE);
select f_clone_table('branch_hk', 'property_cr_355', 'backup_hk', 'property_cr_355', TRUE, TRUE);
select f_clone_table('branch_hk', 'sale_transaction_cr_366', 'backup_hk', 'sale_transaction_cr_366', TRUE, TRUE);
select f_clone_table('branch_hk', 'property_cr_410', 'backup_hk', 'property_cr_410', TRUE, TRUE);


insert into change_log_hk.property_change_log
(id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	property_type_code,property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,slug,
	country_code,is_active,property_display_text,data_source,data_source_id,
	cr_record_action,cr_action_date,cr_id,cr_change_id)
select 
	id,property_dwid,address_dwid,building_dwid,unit_group_dwid,project_dwid,
	property_type_code,property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,slug,
	country_code,is_active,property_display_text,data_source,data_source_id,
	cr_record_action,cr_action_date,cr_id,cr_change_id
from branch_hk.property_cr_410;


update masterdata_hk.property a
set property_type_code = b.property_type_code 
from branch_hk.property_cr_410 b 
where a.id = b.id
and b.cr_record_action = 'update'; -- 1107






-- consistency among txn / listing and premap, map tables
'''
1.rent_transaction v.s. midland_rent_txn_map
2.rent_transaction v.s. midland_unit_to_dwid
3.rent_transaction v.s. midland_building_to_dwid️
4.midland_rent_txn_map v.s. midland_unit_to_dwid
5.midland_unit_to_dwid v.s. midland_building_to_dwid
'''
--1. rent_transaction v.s. midland_rent_txn_map
-- check

select a.*
from masterdata_hk.rent_transaction a
left join map_hk.midland_rent_txn__map b on a.data_uuid = b.data_uuid 
where md5(f_prep_dw_id(a.property_dwid)||'__'||f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(b.property_dwid)||'__'||f_prep_dw_id(b.address_dwid)||'__'||f_prep_dw_id(b.building_dwid)||'__'||f_prep_dw_id(b.project_dwid)) 
;
-- change request
create table branch_hk.midland_rent_txn__map_cr_1 as 
select 
	a.data_uuid , a.activity_dwid , a.property_dwid , a.building_dwid , a.address_dwid , a.project_dwid , 
	null as land_parcel_dwid, null as lot_group_dwid, null as status_code
from masterdata_hk.rent_transaction a
left join map_hk.midland_rent_txn__map b on a.data_uuid = b.data_uuid 
where md5(f_prep_dw_id(a.property_dwid)||'__'||f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(b.property_dwid)||'__'||f_prep_dw_id(b.address_dwid)||'__'||f_prep_dw_id(b.building_dwid)||'__'||f_prep_dw_id(b.project_dwid)) 
; -- 595
-- merge change request
update map_hk.midland_rent_txn__map a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_rent_txn__map_cr_1 b
where a.activity_dwid = b.activity_dwid; -- 595

--2. rent_transaction v.s. midland_unit_to_dwid
-- check
select a.*
from masterdata_hk.rent_transaction a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id
where md5(f_prep_dw_id(a.property_dwid)||'__'||f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(c.property_dwid)||'__'||f_prep_dw_id(c.address_dwid)||'__'||f_prep_dw_id(c.building_dwid)||'__'||f_prep_dw_id(c.project_dwid)) 
;

select a.property_dwid notnull, c.property_dwid notnull, a.property_dwid = c.property_dwid , count(*)
from masterdata_hk.rent_transaction a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
group by 1,2,3; -- true	true	false	3 is correct

select a.address_dwid notnull, c.address_dwid notnull, a.address_dwid = c.address_dwid , count(*)
from masterdata_hk.rent_transaction a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
where a.property_dwid isnull and c.property_dwid isnull
group by 1,2,3;

select a.project_dwid notnull, c.project_dwid notnull, a.project_dwid = c.project_dwid , count(*)
from masterdata_hk.rent_transaction a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
where a.property_dwid isnull and c.property_dwid isnull and a.address_dwid isnull and c.address_dwid isnull
group by 1,2,3;

-- change request
	-- update dwid relationships in rent_transaction
	select metadata.fn_create_change_request(
	    'hk-update-propertydwid-in-rent-transaction-2022-08-16', 'huying','huying'
	); --506
	call metadata.sp_add_change_table(506::int, 'hk', replace('rent_transaction', '-', '_'));

	insert into branch_hk.rent_transaction_cr_506
	select 
		a.id,a.activity_dwid,
		c.property_dwid,c.address_dwid,c.building_dwid,c.project_dwid,a.lot_group_dwid,a.current_lot_group_dwid,
		activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,
		gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
		rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,a.country_code,
		a.data_uuid,a.data_source_uuid,a.data_source,'update' as cr_record_action
	from masterdata_hk.rent_transaction a
	left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
	left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
	where a.id notnull and a.property_dwid isnull and c.property_dwid notnull
	; -- 228
	insert into branch_hk.rent_transaction_cr_506
	select 
		a.id,a.activity_dwid,
		c.property_dwid,c.address_dwid,c.building_dwid,c.project_dwid,a.lot_group_dwid,a.current_lot_group_dwid,
		activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,
		gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
		rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,a.country_code,
		a.data_uuid,a.data_source_uuid,a.data_source,'update' as cr_record_action
	from masterdata_hk.rent_transaction a
	left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
	left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
	where a.id notnull and a.property_dwid != c.property_dwid
	; -- 53
	insert into branch_hk.rent_transaction_cr_506
	select distinct 
		a.id,a.activity_dwid,
		c.property_dwid,c.address_dwid,c.building_dwid,c.project_dwid,a.lot_group_dwid,a.current_lot_group_dwid,
		activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,
		gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
		rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,a.country_code,
		a.data_uuid,a.data_source_uuid,a.data_source,'update' as cr_record_action
	from masterdata_hk.rent_transaction a
	left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
	left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
	where a.id notnull and a.property_dwid isnull and c.property_dwid isnull and a.address_dwid isnull and c.address_dwid isnull and c.project_dwid notnull
	and a.id not in (select id from branch_hk.rent_transaction_cr_506)
	; --219
	insert into branch_hk.rent_transaction_cr_506
	select distinct 
		a.id,a.activity_dwid,
		c.property_dwid,c.address_dwid,c.building_dwid,c.project_dwid,a.lot_group_dwid,a.current_lot_group_dwid,
		activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,
		gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
		rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,a.country_code,
		a.data_uuid,a.data_source_uuid,a.data_source,'update' as cr_record_action
	from masterdata_hk.rent_transaction a
	left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
	left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
	where a.id notnull and a.property_dwid isnull and c.property_dwid isnull and a.address_dwid isnull and c.address_dwid notnull
	and a.id not in (select id from branch_hk.rent_transaction_cr_506)
	; -- 18
	insert into branch_hk.rent_transaction_cr_506
	select distinct 
		a.id,a.activity_dwid,
		c.property_dwid,c.address_dwid,c.building_dwid,c.project_dwid,a.lot_group_dwid,a.current_lot_group_dwid,
		activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,
		gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
		rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,a.country_code,
		a.data_uuid,a.data_source_uuid,a.data_source,'update' as cr_record_action
	from masterdata_hk.rent_transaction a
	left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
	left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
	where a.id notnull and a.property_dwid isnull and c.property_dwid isnull and a.address_dwid != c.address_dwid
	and a.id not in (select id from branch_hk.rent_transaction_cr_506)
	; -- 2

	-- insert missing dwid relationships into midland_unit_to_dwid
	create table branch_hk.midland_unit_to_dwid_cr_14 as 
	with midland_units as (
	    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
	    from reference.hk_midland_backfill
	    where unit_id notnull and floor notnull
	)
	select
		distinct
		null as id,b.region_name,b.region_id,b.district_name,b.district_id,b.subregion_name,b.subregion_id,
		b.estate_name,b.estate_id,b.phase_name,b.phase_id,b.building_name,b.building_id,
		d.corrected_street_num as address_number, d.corrected_street_name as address_street, 
		mu.floor,b.flat as stack, b.unit_id,
		a.property_dwid,a.building_dwid,a.address_dwid,a.project_dwid,a.lot_group_dwid, 
		'insert' as cr_record_action
	from masterdata_hk.rent_transaction a
	left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
	left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
	left join reference.hk_midland_hkpost_correction_backfill d on b.building_id = d.building_id 
	left join midland_units mu on b.unit_id = mu.unit_id and mu.rn = 1
	where a.id notnull and a.id notnull and c.property_dwid isnull
	--and a.property_dwid notnull and c.property_dwid isnull
	; --24331

-- merge change request
call metadata.sp_submit_change_request(506, 'huying');

call metadata.sp_approve_change_request(506, 'huying');

call metadata.sp_merge_change_request(506);

--
update premap_hk.midland_unit_to_dwid a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_unit_to_dwid_cr_14 b
where a.id = b.id::int and b.cr_record_action = 'update'; --0

delete from premap_hk.midland_unit_to_dwid a
where exists (select 1 from branch_hk.midland_unit_to_dwid_cr_14 b where a.id = b.id::int and b.cr_record_action = 'delete'); --567

insert into premap_hk.midland_unit_to_dwid
(
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,phase_name,phase_id,
	building_name,building_id,address_number,address_street,floor,stack,unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid
)
with base1 as(
select 
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,phase_name,phase_id,
	building_name,building_id,address_number,address_street,floor,stack,unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid,
	row_number() over (partition by region_name, district_name, subregion_name, estate_name, phase_name, building_name, floor, stack order by project_dwid) as seq
from branch_hk.midland_unit_to_dwid_cr_14 a
where cr_record_action = 'insert'
and not exists (select 1 from premap_hk.midland_unit_to_dwid b where b.unit_id = a.unit_id)
)
select 
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,phase_name,phase_id,
	building_name,building_id,address_number,address_street,floor,stack,unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid
from base1 where seq = 1; -- 26132



--3.rent_transaction v.s. midland_building_to_dwid️
select a.*
from masterdata_hk.rent_transaction a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_building_to_dwid c on b.building_id = c.building_id
where md5(
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)
		) 
	!= md5(
		f_prep_dw_id(c.address_dwid)||'__'||
		f_prep_dw_id(c.building_dwid)||'__'||
		f_prep_dw_id(c.project_dwid)
		) 
; -- 0

--4.midland_rent_txn_map v.s. midland_unit_to_dwid
select a.*, c.*
from map_hk.midland_rent_txn__map a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
where md5(
		f_prep_dw_id(a.property_dwid)||'__'||
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)
		) 
	!= md5(
		f_prep_dw_id(c.property_dwid)||'__'||
		f_prep_dw_id(c.address_dwid)||'__'||
		f_prep_dw_id(c.building_dwid)||'__'||
		f_prep_dw_id(c.project_dwid)
		) 
; --  <= 3 < 10 (some special edgecases)


--5.midland_unit_to_dwid v.s. midland_building_to_dwid
-- check
with idbase as (
select building_id , building_dwid, address_dwid, project_dwid, 
	ROW_NUMBER() over (PARTITION BY building_id order by building_dwid, address_dwid, project_dwid) AS seq
from premap_hk.midland_unit_to_dwid
group by 1,2,3,4
)
, base as (
select building_id , building_dwid, address_dwid, project_dwid
from idbase where seq = 1
)
select a.*, b.*
from premap_hk.midland_building_to_dwid a
left join base b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(b.address_dwid)||'__'||f_prep_dw_id(b.building_dwid)||'__'||f_prep_dw_id(b.project_dwid)) 
--and a.building_id notnull
; -- 0
with idbase as (
select building_id , building_dwid, address_dwid, project_dwid, 
	ROW_NUMBER() over (PARTITION BY building_id order by building_dwid, address_dwid, project_dwid) AS seq
from premap_hk.midland_unit_to_dwid
group by 1,2,3,4
)
, base as (
select building_id , building_dwid, address_dwid, project_dwid
from idbase where seq = 1
)
select a.*, b.*
from premap_hk.midland_building_to_dwid a
right join base b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(b.address_dwid)||'__'||f_prep_dw_id(b.building_dwid)||'__'||f_prep_dw_id(b.project_dwid)) 
; -- 1

--change request
create table branch_hk.midland_unit_to_dwid_cr_16 as 
with idbase as (
select building_id , building_dwid, address_dwid, project_dwid, 
	ROW_NUMBER() over (PARTITION BY building_id order by building_dwid, address_dwid, project_dwid) AS seq
from premap_hk.midland_unit_to_dwid
group by 1,2,3,4
)
, base as (
select building_id , building_dwid, address_dwid, project_dwid
from idbase where seq = 1
)
select 
	c.id,c.region_name,c.region_id,c.district_name,c.district_id,c.subregion_name,c.subregion_id,
	c.estate_name,c.estate_id,c.phase_name,c.phase_id,
	c.building_name,c.building_id,c.address_number,c.address_street,c.floor,c.stack,c.unit_id,
	c.property_dwid,a.building_dwid,a.address_dwid,a.project_dwid,c.lot_group_dwid,
	'update' as cr_record_action
--a.*, c.*
from premap_hk.midland_building_to_dwid a
left join base b on a.building_id = b.building_id
left join premap_hk.midland_unit_to_dwid c on b.building_id = c.building_id
where md5(f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(b.address_dwid)||'__'||f_prep_dw_id(b.building_dwid)||'__'||f_prep_dw_id(b.project_dwid)) 
and b.building_dwid isnull and b.address_dwid isnull and b.project_dwid isnull
; 

create table branch_hk.midland_building_to_dwid_cr_10 as 
with idbase as (
select building_id , building_dwid, address_dwid, project_dwid, 
	ROW_NUMBER() over (PARTITION BY building_id order by building_dwid, address_dwid, project_dwid) AS seq
from premap_hk.midland_unit_to_dwid
group by 1,2,3,4
)
, base as (
select building_id , building_dwid, address_dwid, project_dwid
from idbase where seq = 1
)
select 
	id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,
	estate_name,estate_id,phase_name,phase_id,building_name,a.building_id,address_number,address_street,
	b.building_dwid,b.address_dwid,b.project_dwid,lot_group_dwid,
	'update' as cr_record_action
from premap_hk.midland_building_to_dwid a
left join base b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.address_dwid)||'__'||f_prep_dw_id(a.building_dwid)||'__'||f_prep_dw_id(a.project_dwid)) 
	!= md5(f_prep_dw_id(b.address_dwid)||'__'||f_prep_dw_id(b.building_dwid)||'__'||f_prep_dw_id(b.project_dwid)) 
and a.building_id notnull;

insert into branch_hk.midland_building_to_dwid_cr_10
with idbase as (
select building_id , building_dwid, address_dwid, project_dwid, 
	ROW_NUMBER() over (PARTITION BY building_id order by building_dwid, address_dwid, project_dwid) AS seq
from premap_hk.midland_unit_to_dwid a
where not exists (select 1 from premap_hk.midland_building_to_dwid b where a.building_id = b.building_id) -- 70
group by 1,2,3,4
)
, cr_base as (
select 
	null as id,c.region_name,c.region_id,c.district_name,c.district_id,c.subregion_name,c.subregion_id,
	c.estate_name,c.estate_id,c.phase_name,c.phase_id,c.building_name,b.building_id,c.address_number,c.address_street,
	b.building_dwid,b.address_dwid,b.project_dwid,c.lot_group_dwid,'insert' as cr_record_action,
	row_number() over (partition by c.region_name, c.district_name, c.subregion_name, c.estate_name, c.phase_name, c.building_name, b.building_id) as seq
from idbase b
left join premap_hk.midland_building_to_dwid a on a.building_id = b.building_id
left join premap_hk.midland_unit_to_dwid c on b.building_id = c.building_id 
where b.seq = 1
--where a.building_id isnull --and b.address_dwid notnull
)
select 
	id::int,region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,
	building_dwid,address_dwid,project_dwid,lot_group_dwid,cr_record_action
from cr_base where seq = 1; --71

--merge change request
update premap_hk.midland_unit_to_dwid a
set property_dwid = b.property_dwid, building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_unit_to_dwid_cr_16 b
where a.id = b.id and b.cr_record_action = 'update'; --414

update premap_hk.midland_building_to_dwid a
set building_dwid = b.building_dwid, address_dwid = b.address_dwid, project_dwid = b.project_dwid
from branch_hk.midland_building_to_dwid_cr_10 b
where a.id = b.id and b.cr_record_action = 'update'; -- 324

insert into premap_hk.midland_building_to_dwid
(
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,
	building_dwid,address_dwid,project_dwid,lot_group_dwid
)
select distinct 
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,
	building_dwid,address_dwid,project_dwid,lot_group_dwid
from branch_hk.midland_building_to_dwid_cr_10
where cr_record_action = 'insert'; -- 71









--------------------------------------------------------------------------------------------
-- 5. fill in dwid and status_code gaps in map table 
--------------------------------------------------------------------------------------------
-- 

select estate_id , project_dwid notnull as has_project, count(*) as cnt, row_number() over(partition by estate_id order by project_dwid notnull desc) as seq
from premap_hk.midland_unit_to_dwid 
group by 1,2 order by 1;


select building_id , project_dwid notnull as has_project, building_dwid notnull as has_building, address_dwid notnull as has_address, count(*) as cnt, row_number() over(partition by building_id order by project_dwid notnull desc, building_dwid notnull desc, address_dwid notnull desc) as seq
from premap_hk.midland_unit_to_dwid mutd 
group by 1,2,3,4 order by 1;



select estate_id , count(distinct project_dwid) as cnt
from premap_hk.midland_unit_to_dwid 
where project_dwid notnull
group by 1 having count(distinct project_dwid) > 1
;


insert into branch_hk.midland_unit_to_dwid_cr_30
SELECT 
	id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,floor,stack,
	unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid,
	'update' as cr_record_action
FROM premap_hk.midland_unit_to_dwid a
where phase_id = 'P000000490';



insert into branch_hk.midland_unit_to_dwid_cr_30
SELECT 
	id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,floor,stack,
	unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid,
	'update' as cr_record_action
FROM premap_hk.midland_unit_to_dwid a
where estate_id in ('E000010411', 'E000013989', 'E000015290', 'E000016124', 'E00023', 'E00029')
and project_dwid isnull order by estate_id, phase_id;

with base as (
select distinct phase_id, project_dwid
from premap_hk.midland_unit_to_dwid a
where estate_id in ('E000010411', 'E000013989', 'E000015290', 'E000016124', 'E00023', 'E00029')
and project_dwid notnull
)
update branch_hk.midland_unit_to_dwid_cr_30 a
set project_dwid = b.project_dwid
from base b where a.phase_id = b.phase_id



with base as (
select estate_id , project_dwid notnull as has_project, count(*) as cnt, row_number() over(partition by estate_id order by project_dwid notnull desc) as seq
from premap_hk.midland_unit_to_dwid 
group by 1,2 order by 1
)
select sum(cnt)
from base where seq > 1; -- 11549

'''
insert into branch_hk.midland_unit_to_dwid_cr_30
with estate_base as (
select estate_id , project_dwid, count(*) as cnt, row_number() over(partition by estate_id order by project_dwid notnull desc) as seq
from premap_hk.midland_unit_to_dwid 
group by 1,2 order by 1
)
, estate_need_fix as (
select estate_id from estate_base where seq > 1
)
select
a.id,a.region_name,a.region_id,a.district_name,a.district_id,a.subregion_name,a.subregion_id,a.estate_name,a.estate_id,
a.phase_name,a.phase_id,a.building_name,a.building_id,a.address_number,a.address_street,a.floor,a.stack,a.unit_id,
a.property_dwid,a.building_dwid,a.address_dwid,c.project_dwid,a.lot_group_dwid,'update' as cr_record_action
from premap_hk.midland_unit_to_dwid a
left join estate_need_fix b on a.estate_id = b.estate_id
left join estate_base c on b.estate_id = c.estate_id and seq = 1
where b.estate_id notnull and b.estate_id not in ('E000006506', 'E000010411', 'E000013989', 'E000015290', 'E000016124', 'E00023', 'E00029') -- 1682-7 
and a.project_dwid isnull; -- 11413
'''

create table branch_hk.midland_unit_to_dwid_cr_20220921 as
with estate_base as (
select estate_id , project_dwid, count(*) as cnt, row_number() over(partition by estate_id order by project_dwid notnull desc) as seq
from premap_hk.midland_unit_to_dwid 
group by 1,2 order by 1
)
, estate_need_fix as (
select estate_id from estate_base where seq > 1
)
, estate_need_exclude as (
select estate_id --, count(distinct project_dwid) as cnt
from premap_hk.midland_unit_to_dwid 
where project_dwid notnull
group by 1 having count(distinct project_dwid) > 1
)
select
a.id,a.region_name,a.region_id,a.district_name,a.district_id,a.subregion_name,a.subregion_id,a.estate_name,a.estate_id,
a.phase_name,a.phase_id,a.building_name,a.building_id,a.address_number,a.address_street,a.floor,a.stack,a.unit_id,
a.property_dwid,a.building_dwid,a.address_dwid,c.project_dwid,a.lot_group_dwid,'update' as cr_record_action
from premap_hk.midland_unit_to_dwid a
left join estate_need_fix b on a.estate_id = b.estate_id
left join estate_base c on b.estate_id = c.estate_id and seq = 1
where b.estate_id notnull 
and not exists (select 1 from estate_need_exclude d where a.estate_id = d.estate_id)
and a.project_dwid isnull;


-- merge change request
'''
update premap_hk.midland_unit_to_dwid a
set project_dwid = b.project_dwid 
from branch_hk.midland_unit_to_dwid_cr_30 b
where a.id = b.id; -- 11523
'''

update premap_hk.midland_unit_to_dwid a
set project_dwid = b.project_dwid 
from branch_hk.midland_unit_to_dwid_cr_20220921 b
where a.id = b.id;



select distinct estate_id, project_dwid
from branch_hk.midland_unit_to_dwid_cr_30

select building_id
from branch_hk.midland_unit_to_dwid_cr_30
group by 1; -- 2583


with idbase as (
select distinct building_id, project_dwid
from branch_hk.midland_unit_to_dwid_cr_30
)
select a.*, b.*
from premap_hk.midland_building_to_dwid a
left join idbase b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.project_dwid)) != md5(f_prep_dw_id(b.project_dwid)) -- 8631
and b.building_id notnull -- 1496
; 

'''
create table branch_hk.midland_building_to_dwid_cr_15 as 
with idbase as (
select distinct building_id, project_dwid
from branch_hk.midland_unit_to_dwid_cr_30
)
select
	a.id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,a.building_id,address_number,address_street,
	a.building_dwid,a.address_dwid,b.project_dwid,a.lot_group_dwid,'update' as cr_record_action
from premap_hk.midland_building_to_dwid a
left join idbase b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.project_dwid)) != md5(f_prep_dw_id(b.project_dwid)) -- 8631
and b.building_id notnull -- 1496
'''
	

create table branch_hk.midland_building_to_dwid_cr_20220921 as 
with idbase as (
select distinct building_id, project_dwid
from branch_hk.midland_unit_to_dwid_cr_20220921
)
select
	a.id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,a.building_id,address_number,address_street,
	a.building_dwid,a.address_dwid,b.project_dwid,a.lot_group_dwid,'update' as cr_record_action
from premap_hk.midland_building_to_dwid a
left join idbase b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.project_dwid)) != md5(f_prep_dw_id(b.project_dwid)) -- 8631
and b.building_id notnull -- 1496



-- merge change request
update premap_hk.midland_building_to_dwid a
set project_dwid = b.project_dwid 
from branch_hk.midland_building_to_dwid_cr_20220921 b
where a.id = b.id; -- 1496





select mapp.*, cr.*
from map_hk.midland_sale_txn__map mapp 
left join "source".hk_midland_realty_sale_transaction st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
and cr.estate_name = 'South Horizons' -- 49

select mapp.*, cr.*
from map_hk.midland_sale_listing__map mapp 
left join "source".hk_midland_realty_sale_listing st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
and cr.estate_name = 'South Horizons' -- 168


select mapp.*, cr.*
from map_hk.midland_rent_txn__map mapp 
left join "source".hk_midland_realty_rental_transaction st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
and cr.estate_name = 'South Horizons' -- 263


select mapp.*, cr.*
from map_hk.midland_rent_listing__map mapp 
left join "source".hk_midland_realty_rental_listing st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
and cr.estate_name = 'South Horizons' -- 180



--create table branch_hk.midland_sale_txn__map_cr_17 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_sale_txn__map mapp 
left join "source".hk_midland_realty_sale_transaction st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
; -- 8477

--create table branch_hk.midland_sale_txn__map_cr_18 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_sale_txn__map mapp 
left join "source".hk_midland_realty_sale_transaction st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
; -- 2860


create table branch_hk.midland_sale_txn__map_cr_20220921 as 


select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_sale_txn__map mapp 
left join "source".hk_midland_realty_sale_transaction st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull 
and md5(f_prep_dw_id(mapp.project_dwid)) != md5(f_prep_dw_id(cr.project_dwid)) and cr.project_dwid notnull
--and mapp.project_dwid isnull and cr.project_dwid notnull
;



--create table branch_hk.midland_sale_listing__map_cr_2 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_sale_listing__map mapp 
left join "source".hk_midland_realty_sale_listing st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
; -- 1350


--create table branch_hk.midland_sale_listing__map_cr_3 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_sale_listing__map mapp 
left join "source".hk_midland_realty_sale_listing st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
; -- 0

create table branch_hk.midland_sale_listing__map_cr_20220921 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_sale_listing__map mapp 
left join "source".hk_midland_realty_sale_listing st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
;


--create table branch_hk.midland_rent_txn__map_cr_6 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_rent_txn__map mapp 
left join "source".hk_midland_realty_rental_transaction st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
; -- 1307

--create table branch_hk.midland_rent_txn__map_cr_7 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_rent_txn__map mapp 
left join "source".hk_midland_realty_rental_transaction st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
; -- 0


create table branch_hk.midland_rent_txn__map_cr_20220921 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_rent_txn__map mapp 
left join "source".hk_midland_realty_rental_transaction st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
;

--create table branch_hk.midland_rent_listing__map_cr_2 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_rent_listing__map mapp 
left join "source".hk_midland_realty_rental_listing st on mapp.data_uuid = st.data_uuid 
left join branch_hk.midland_unit_to_dwid_cr_30 cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull
; -- 970

--create table branch_hk.midland_rent_listing__map_cr_3 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_rent_listing__map mapp 
left join "source".hk_midland_realty_rental_listing st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
; -- 0

create table branch_hk.midland_rent_listing__map_cr_20220921 as 
select 
	mapp.data_uuid , mapp.activity_dwid , mapp.property_dwid , mapp.building_dwid , mapp.address_dwid , 
	cr.project_dwid , 
	mapp.land_parcel_dwid, mapp.lot_group_dwid, mapp.status_code
from map_hk.midland_rent_listing__map mapp 
left join "source".hk_midland_realty_rental_listing st on mapp.data_uuid = st.data_uuid 
left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id 
where cr.id notnull and mapp.project_dwid isnull and cr.project_dwid notnull
;

-- merge change request
update map_hk.midland_sale_txn__map a 
set project_dwid = b.project_dwid
from branch_hk.midland_sale_txn__map_cr_20220921 b
where a.activity_dwid = b.activity_dwid;

update map_hk.midland_sale_listing__map a 
set project_dwid = b.project_dwid
from branch_hk.midland_sale_listing__map_cr_20220921 b
where a.activity_dwid = b.activity_dwid;

update map_hk.midland_rent_txn__map a 
set project_dwid = b.project_dwid
from branch_hk.midland_rent_txn__map_cr_20220921 b
where a.activity_dwid = b.activity_dwid;

update map_hk.midland_rent_listing__map a 
set project_dwid = b.project_dwid
from branch_hk.midland_rent_listing__map_cr_20220921 b
where a.activity_dwid = b.activity_dwid;




select st.*, mapp.*
from masterdata_hk.sale_transaction st 
left join map_hk.midland_sale_txn__map mapp on st.activity_dwid = mapp.activity_dwid
where mapp.activity_dwid notnull and st.project_dwid isnull and mapp.project_dwid notnull
and st.data_uuid = 'e838e07f-2558-4f11-ab94-996e95f4eea1'



select metadata.fn_create_change_request(
    'hk-update-project_dwid-in-sale_transaction-2022-09-20', 'huying','huying'
); --1063

call metadata.sp_add_change_table(1063::int, 'hk', replace('sale_transaction', '-', '_'));



insert into branch_hk.sale_transaction_cr_1063
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_name,units_sold,sale_type,sale_subtype,property_completion_year,property_type_code,address_unit,address_local_text,
	tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,contract_date,settlement_date,
	purchase_amount,country_code,data_uuid,data_source_uuid,data_source,address_lot_number,cr_record_action
)
select
	st.id,st.activity_dwid,st.property_dwid,st.address_dwid,st.building_dwid,
	mapp.project_dwid,
	st.lot_group_dwid,st.current_lot_group_dwid,activity_name,
	units_sold,sale_type,sale_subtype,property_completion_year,property_type_code,address_unit,address_local_text,tenure_code,
	bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,contract_date,settlement_date,purchase_amount,
	country_code,st.data_uuid,data_source_uuid,data_source,address_lot_number,'update' as cr_record_action
from masterdata_hk.sale_transaction st 
left join map_hk.midland_sale_txn__map mapp on st.activity_dwid = mapp.activity_dwid
where mapp.activity_dwid notnull and st.project_dwid isnull and mapp.project_dwid notnull
;

call metadata.sp_submit_change_request(1063, 'huying');

call metadata.sp_approve_change_request(1063, 'huying');

call metadata.sp_merge_change_request(1063);


-- check:
select rt.*  
from masterdata_hk.sale_transaction rt
left join masterdata_hk.project pj on rt.project_dwid = pj.project_dwid
where project_name ilike '%Fortune%Gardens%'



select metadata.fn_create_change_request(
    'hk-update-project_dwid-in-sale_listing-2022-09-20', 'huying','huying'
); --1064

call metadata.sp_add_change_table(1064::int, 'hk', replace('sale_listing', '-', '_'));


insert into branch_hk.sale_listing_cr_1064
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_display_text,listing_status,unit_count,sale_type,sale_subtype,property_completion_year,property_type_code,
	address_unit,address_local_text,tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,
	first_listing_date,last_listing_date,transaction_date,listing_amount,country_code,data_uuid,data_source_uuid,data_source,status_code,listing_agent_dwid,address_lot_number,cr_record_action
)
select
	sl.id,sl.activity_dwid,sl.property_dwid,sl.address_dwid,sl.building_dwid,
	mapp.project_dwid,
	sl.lot_group_dwid,current_lot_group_dwid,
	activity_display_text,listing_status,unit_count,sale_type,sale_subtype,property_completion_year,property_type_code,
	address_unit,address_local_text,tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,
	first_listing_date,last_listing_date,transaction_date,listing_amount,country_code,
	sl.data_uuid,data_source_uuid,data_source,sl.status_code,listing_agent_dwid,address_lot_number,'update' as cr_record_action
from masterdata_hk.sale_listing sl
left join map_hk.midland_sale_listing__map mapp on sl.activity_dwid = mapp.activity_dwid 
where mapp.activity_dwid notnull and sl.project_dwid isnull and mapp.project_dwid notnull
;

call metadata.sp_submit_change_request(1064, 'huying');

call metadata.sp_approve_change_request(1064, 'huying');

call metadata.sp_merge_change_request(1064);



select metadata.fn_create_change_request(
    'hk-update-project_dwid-in-rent_transaction-2022-09-20', 'huying','huying'
); --1065

call metadata.sp_add_change_table(1065::int, 'hk', replace('rent_transaction', '-', '_'));


insert into branch_hk.rent_transaction_cr_1065
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
	rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,country_code,data_uuid,data_source_uuid,data_source,cr_record_action
)
select
	rt.id,rt.activity_dwid,rt.property_dwid,rt.address_dwid,rt.building_dwid,
	mapp.project_dwid,
	rt.lot_group_dwid,rt.current_lot_group_dwid,
	activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
	rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,country_code,rt.data_uuid,data_source_uuid,data_source,'update' as cr_record_action
from masterdata_hk.rent_transaction rt
left join map_hk.midland_rent_txn__map mapp on rt.activity_dwid = mapp.activity_dwid 
where mapp.activity_dwid notnull and rt.project_dwid isnull and mapp.project_dwid notnull
;

call metadata.sp_submit_change_request(1065, 'huying');

call metadata.sp_approve_change_request(1065, 'huying');

call metadata.sp_merge_change_request(1065);



select metadata.fn_create_change_request(
    'hk-update-project_dwid-in-rent_listing-2022-09-20', 'huying','huying'
); --1066

call metadata.sp_add_change_table(1066::int, 'hk', replace('rent_listing', '-', '_'));


insert into branch_hk.rent_listing_cr_1066
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_display_text,listing_status,rent_type,property_type_code,property_subtype,address_unit,address_local_text,bathroom_count,bedroom_count,
	gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,first_listing_date,last_listing_date,transaction_date,
	rent_amount_weekly,rent_amount_monthly,country_code,data_uuid,data_source_uuid,data_source,status_code,listing_agent_dwid,address_lot_number,cr_record_action
)
select
	rl.id,rl.activity_dwid,rl.property_dwid,rl.address_dwid,rl.building_dwid,
	mapp.project_dwid,
	rl.lot_group_dwid,rl.current_lot_group_dwid,
	activity_display_text,listing_status,rent_type,property_type_code,property_subtype,address_unit,address_local_text,bathroom_count,bedroom_count,
	gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,first_listing_date,last_listing_date,transaction_date,
	rent_amount_weekly,rent_amount_monthly,country_code,rl.data_uuid,data_source_uuid,data_source,rl.status_code,listing_agent_dwid,address_lot_number,'update' as cr_record_action
from masterdata_hk.rent_listing rl
left join map_hk.midland_rent_listing__map mapp on rl.activity_dwid = mapp.activity_dwid 
where mapp.activity_dwid notnull and rl.project_dwid isnull and mapp.project_dwid notnull
;

call metadata.sp_submit_change_request(1066, 'huying');

call metadata.sp_approve_change_request(1066, 'huying');

call metadata.sp_merge_change_request(1066);

  	price  area  	psf
1 	100		10		10
2	50		4		12.5
3	60		3		20
4	40		2		20
avg 62.5	4.75	

62.5/4.75 = 13.15
avg(10,12.5,20,20) = 15.625






create table branch_hk.midland_unit_to_dwid_cr_20220921_2 as
with building_base as (
select building_id , building_dwid , address_dwid , count(*) as cnt, row_number() over(partition by building_id order by building_dwid notnull desc, address_dwid notnull desc) as seq
from premap_hk.midland_unit_to_dwid 
group by 1, 2, 3 order by 1
)
, building_need_fix as (
select building_id from building_base where seq > 1 and building_dwid isnull
)
, building_need_exclude as (
select building_id from building_base where seq > 2
)
select
a.id,a.region_name,a.region_id,a.district_name,a.district_id,a.subregion_name,a.subregion_id,a.estate_name,a.estate_id,
a.phase_name,a.phase_id,a.building_name,a.building_id,a.address_number,a.address_street,a.floor,a.stack,a.unit_id,
a.property_dwid,c.building_dwid,c.address_dwid,a.project_dwid,a.lot_group_dwid,'update' as cr_record_action
from premap_hk.midland_unit_to_dwid a
left join building_need_fix b on a.building_id = b.building_id
left join building_base c on b.building_id = c.building_id and c.seq = 1
where b.building_id notnull 
and not exists (select 1 from building_need_exclude d where a.building_id = d.building_id)
and (a.building_dwid isnull or a.address_dwid isnull)
; -- 11829


update premap_hk.midland_unit_to_dwid a
set building_dwid  = b.building_dwid, address_dwid = b.address_dwid 
from branch_hk.midland_unit_to_dwid_cr_20220921_2 b
where a.id = b.id
; -- 11829



create table branch_hk.midland_building_to_dwid_cr_20220921_2 as 
with idbase as (
select distinct building_id, building_dwid , address_dwid 
from branch_hk.midland_unit_to_dwid_cr_20220921_2
)
select
	a.id,region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,a.building_id,address_number,address_street,
	b.building_dwid,b.address_dwid,a.project_dwid,a.lot_group_dwid,'update' as cr_record_action
from premap_hk.midland_building_to_dwid a
left join idbase b on a.building_id = b.building_id
where md5(f_prep_dw_id(a.building_dwid)||f_prep_dw_id(a.address_dwid)) != md5(f_prep_dw_id(b.building_dwid)||f_prep_dw_id(b.address_dwid)) -- 10835
and b.building_id notnull -- 0


update premap_hk.midland_building_to_dwid a
set building_dwid  = b.building_dwid, address_dwid = b.address_dwid 
from branch_hk.midland_building_to_dwid_cr_20220921_2 b
where a.id = b.id
; -- 0


--create table branch_hk.midland_sale_txn__map_cr_20220921_2 as
select
	mapp.data_uuid,
    mapp.activity_dwid,
    mapp.property_dwid,
    cr.building_dwid,
    cr.address_dwid,
	mapp.project_dwid,
	mapp.land_parcel_dwid,
    mapp.lot_group_dwid,
    mapp.status_code
from map_hk.midland_sale_txn__map mapp
    left join "source".hk_midland_realty_sale_transaction st on mapp.data_uuid = st.data_uuid
    left join premap_hk.midland_unit_to_dwid cr on st.unit_id = cr.unit_id
where cr.id notnull
and md5(f_prep_dw_id(mapp.building_dwid)||f_prep_dw_id(mapp.address_dwid)) != md5(f_prep_dw_id(cr.building_dwid)||f_prep_dw_id(cr.address_dwid)) and (cr.building_dwid notnull and cr.address_dwid notnull) -- 23344
--and (mapp.building_dwid isnull or mapp.address_dwid isnull) and (cr.building_dwid notnull and cr.address_dwid notnull) -- 7707 this conditin not enough
; 

--drop table branch_hk.midland_sale_txn__map_cr_20220921_2;

create table branch_hk.midland_sale_txn__map_cr_20220921_2 as
select
	mapp.data_uuid,
    mapp.activity_dwid,
    mapp.property_dwid,
    cr.building_dwid,
    cr.address_dwid,
	mapp.project_dwid,
	mapp.land_parcel_dwid,
    mapp.lot_group_dwid,
    mapp.status_code
from map_hk.midland_sale_txn__map mapp
    left join "source".hk_midland_realty_sale_transaction st on mapp.data_uuid = st.data_uuid
    left join branch_hk.midland_unit_to_dwid_cr_20220921_2 cr on st.unit_id = cr.unit_id -- 12008
where cr.id notnull
and md5(f_prep_dw_id(mapp.building_dwid)||f_prep_dw_id(mapp.address_dwid)) != md5(f_prep_dw_id(cr.building_dwid)||f_prep_dw_id(cr.address_dwid)) and (cr.building_dwid notnull and cr.address_dwid notnull) -- 23344
;

create table branch_hk.midland_sale_listing__map_cr_20220921_2 as
select
	mapp.data_uuid,
    mapp.activity_dwid,
    mapp.property_dwid,
    cr.building_dwid,
    cr.address_dwid,
	mapp.project_dwid,
	mapp.land_parcel_dwid,
    mapp.lot_group_dwid,
    mapp.status_code
from map_hk.midland_sale_listing__map mapp
    left join "source".hk_midland_realty_sale_listing st on mapp.data_uuid = st.data_uuid
    left join branch_hk.midland_unit_to_dwid_cr_20220921_2 cr on st.unit_id = cr.unit_id
where cr.id notnull
and md5(f_prep_dw_id(mapp.building_dwid)||f_prep_dw_id(mapp.address_dwid)) != md5(f_prep_dw_id(cr.building_dwid)||f_prep_dw_id(cr.address_dwid)) 
and (cr.building_dwid notnull and cr.address_dwid notnull) -- 0
; 


create table branch_hk.midland_rent_txn__map_cr_20220921_2 as
select
	mapp.data_uuid,
    mapp.activity_dwid,
    mapp.property_dwid,
    cr.building_dwid,
    cr.address_dwid,
	mapp.project_dwid,
	mapp.land_parcel_dwid,
    mapp.lot_group_dwid,
    mapp.status_code
from map_hk.midland_rent_txn__map mapp
    left join "source".hk_midland_realty_rental_transaction st on mapp.data_uuid = st.data_uuid
    left join branch_hk.midland_unit_to_dwid_cr_20220921_2 cr on st.unit_id = cr.unit_id
where cr.id notnull
and md5(f_prep_dw_id(mapp.building_dwid)||f_prep_dw_id(mapp.address_dwid)) != md5(f_prep_dw_id(cr.building_dwid)||f_prep_dw_id(cr.address_dwid)) 
and (cr.building_dwid notnull and cr.address_dwid notnull) -- 0
; -- 0

create table branch_hk.midland_rent_listing__map_cr_20220921_2 as
select
	mapp.data_uuid,
    mapp.activity_dwid,
    mapp.property_dwid,
    cr.building_dwid,
    cr.address_dwid,
	mapp.project_dwid,
	mapp.land_parcel_dwid,
    mapp.lot_group_dwid,
    mapp.status_code
from map_hk.midland_rent_listing__map mapp
    left join "source".hk_midland_realty_rental_listing st on mapp.data_uuid = st.data_uuid
    left join branch_hk.midland_unit_to_dwid_cr_20220921_2 cr on st.unit_id = cr.unit_id
where cr.id notnull
and md5(f_prep_dw_id(mapp.building_dwid)||f_prep_dw_id(mapp.address_dwid)) != md5(f_prep_dw_id(cr.building_dwid)||f_prep_dw_id(cr.address_dwid)) 
and (cr.building_dwid notnull and cr.address_dwid notnull) -- 0
;

update map_hk.midland_sale_txn__map a
set building_dwid  = b.building_dwid, address_dwid = b.address_dwid 
from branch_hk.midland_sale_txn__map_cr_20220921_2 b
where a.activity_dwid = b.activity_dwid
; -- 11968





select metadata.fn_create_change_request(
    'hk-update-building_dwid-in-sale_transaction-2022-09-22', 'huying','huying'
); --1106

call metadata.sp_add_change_table(1106::int, 'hk', replace('sale_transaction', '-', '_'));

-- drop table branch_hk.sale_transaction_cr_1105;

insert into branch_hk.sale_transaction_cr_1106
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_name,units_sold,sale_type,sale_subtype,property_completion_year,property_type_code,address_unit,address_local_text,
	tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,contract_date,settlement_date,
	purchase_amount,country_code,data_uuid,data_source_uuid,data_source,address_lot_number,cr_record_action
)
select
	st.id,st.activity_dwid,st.property_dwid,
	mapp.address_dwid,mapp.building_dwid,st.project_dwid,
	st.lot_group_dwid,st.current_lot_group_dwid,activity_name,
	units_sold,sale_type,sale_subtype,property_completion_year,property_type_code,address_unit,address_local_text,tenure_code,
	bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,contract_date,settlement_date,purchase_amount,
	country_code,st.data_uuid,data_source_uuid,data_source,address_lot_number,'update' as cr_record_action
from masterdata_hk.sale_transaction st 
left join map_hk.midland_sale_txn__map mapp on st.activity_dwid = mapp.activity_dwid
where mapp.activity_dwid notnull 
and md5(f_prep_dw_id(mapp.building_dwid) || f_prep_dw_id(mapp.address_dwid))
    != md5(f_prep_dw_id(st.building_dwid) || f_prep_dw_id(st.address_dwid))
and mapp.building_dwid notnull and mapp.address_dwid notnull; -- 11969


select
	st.id,st.activity_dwid,st.property_dwid,
	mapp.address_dwid,mapp.building_dwid,st.project_dwid,
	'update' as cr_record_action
from masterdata_hk.sale_transaction st 
left join branch_hk.midland_sale_txn__map_cr_20220921_2 mapp on st.activity_dwid = mapp.activity_dwid
where mapp.activity_dwid notnull 
and md5(f_prep_dw_id(mapp.building_dwid) || f_prep_dw_id(mapp.address_dwid))
    != md5(f_prep_dw_id(st.building_dwid) || f_prep_dw_id(st.address_dwid))
and mapp.building_dwid notnull and mapp.address_dwid notnull; -- 12008


call metadata.sp_submit_change_request(1106, 'huying');

call metadata.sp_approve_change_request(1106, 'huying');

call metadata.sp_merge_change_request(1106);


select data_uuid , count(*)
from map_hk.midland_sale_txn__map mstm 
group by 1 having count(*) > 1




select metadata.fn_create_change_request(
    'hk-update-building_dwid-in-sale_listing-2022-09-20', 'huying','huying'
); --1064

call metadata.sp_add_change_table(1064::int, 'hk', replace('sale_listing', '-', '_'));


insert into branch_hk.sale_listing_cr_1064
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_display_text,listing_status,unit_count,sale_type,sale_subtype,property_completion_year,property_type_code,
	address_unit,address_local_text,tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,
	first_listing_date,last_listing_date,transaction_date,listing_amount,country_code,data_uuid,data_source_uuid,data_source,status_code,listing_agent_dwid,address_lot_number,cr_record_action
)
select
	sl.id,sl.activity_dwid,sl.property_dwid,
	mapp.address_dwid,mapp.building_dwid,sl.project_dwid,
	sl.lot_group_dwid,current_lot_group_dwid,
	activity_display_text,listing_status,unit_count,sale_type,sale_subtype,property_completion_year,property_type_code,
	address_unit,address_local_text,tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,
	first_listing_date,last_listing_date,transaction_date,listing_amount,country_code,
	sl.data_uuid,data_source_uuid,data_source,sl.status_code,listing_agent_dwid,address_lot_number,'update' as cr_record_action
from masterdata_hk.sale_listing sl
left join map_hk.midland_sale_listing__map mapp on sl.activity_dwid = mapp.activity_dwid 
where mapp.activity_dwid notnull
and md5(f_prep_dw_id(mapp.building_dwid) || f_prep_dw_id(mapp.address_dwid))
    != md5(f_prep_dw_id(sl.building_dwid) || f_prep_dw_id(sl.address_dwid))
and mapp.building_dwid notnull and mapp.address_dwid notnull
;

call metadata.sp_submit_change_request(1064, 'huying');

call metadata.sp_approve_change_request(1064, 'huying');

call metadata.sp_merge_change_request(1064);



select metadata.fn_create_change_request(
    'hk-update-building_dwid-in-rent_transaction-2022-09-20', 'huying','huying'
); --1065

call metadata.sp_add_change_table(1065::int, 'hk', replace('rent_transaction', '-', '_'));


insert into branch_hk.rent_transaction_cr_1065
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
	rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,country_code,data_uuid,data_source_uuid,data_source,cr_record_action
)
select
	rt.id,rt.activity_dwid,rt.property_dwid,
	mapp.address_dwid,mapp.building_dwid,rt.project_dwid,
	rt.lot_group_dwid,rt.current_lot_group_dwid,
	activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,
	rent_start_date,rent_end_date,rent_amount_weekly,rent_amount_monthly,country_code,rt.data_uuid,data_source_uuid,data_source,'update' as cr_record_action
from masterdata_hk.rent_transaction rt
left join map_hk.midland_rent_txn__map mapp on rt.activity_dwid = mapp.activity_dwid 
where mapp.activity_dwid notnull
and md5(f_prep_dw_id(mapp.building_dwid) || f_prep_dw_id(mapp.address_dwid))
    != md5(f_prep_dw_id(rt.building_dwid) || f_prep_dw_id(rt.address_dwid))
and mapp.building_dwid notnull and mapp.address_dwid notnull
;

call metadata.sp_submit_change_request(1065, 'huying');

call metadata.sp_approve_change_request(1065, 'huying');

call metadata.sp_merge_change_request(1065);



select metadata.fn_create_change_request(
    'hk-update-building_dwid-in-rent_listing-2022-09-20', 'huying','huying'
); --1066

call metadata.sp_add_change_table(1066::int, 'hk', replace('rent_listing', '-', '_'));


insert into branch_hk.rent_listing_cr_1066
(
	id,activity_dwid,property_dwid,address_dwid,building_dwid,project_dwid,lot_group_dwid,current_lot_group_dwid,
	activity_display_text,listing_status,rent_type,property_type_code,property_subtype,address_unit,address_local_text,bathroom_count,bedroom_count,
	gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,first_listing_date,last_listing_date,transaction_date,
	rent_amount_weekly,rent_amount_monthly,country_code,data_uuid,data_source_uuid,data_source,status_code,listing_agent_dwid,address_lot_number,cr_record_action
)
select
	rl.id,rl.activity_dwid,rl.property_dwid,
	mapp.address_dwid,mapp.building_dwid,rl.project_dwid,
	rl.lot_group_dwid,rl.current_lot_group_dwid,
	activity_display_text,listing_status,rent_type,property_type_code,property_subtype,address_unit,address_local_text,bathroom_count,bedroom_count,
	gross_floor_area_sqm,net_floor_area_sqm,land_area_sqm,gross_floor_area_sqm_min,gross_floor_area_sqm_max,first_listing_date,last_listing_date,transaction_date,
	rent_amount_weekly,rent_amount_monthly,country_code,rl.data_uuid,data_source_uuid,data_source,rl.status_code,listing_agent_dwid,address_lot_number,'update' as cr_record_action
from masterdata_hk.rent_listing rl
left join map_hk.midland_rent_listing__map mapp on rl.activity_dwid = mapp.activity_dwid 
where mapp.activity_dwid notnull
and md5(f_prep_dw_id(mapp.building_dwid) || f_prep_dw_id(mapp.address_dwid))
    != md5(f_prep_dw_id(rl.building_dwid) || f_prep_dw_id(rl.address_dwid))
and mapp.building_dwid notnull and mapp.address_dwid notnull
;

call metadata.sp_submit_change_request(1066, 'huying');

call metadata.sp_approve_change_request(1066, 'huying');

call metadata.sp_merge_change_request(1066);









--------------------------------------------------------------------------------------------
-- 6. hk sale transaction source difference: midland / centanet / hk data gov / email transaction
--------------------------------------------------------------------------------------------


select a.*
from "source".hk_centanet_sale_transaction a 
left join "source".hk_daily_transaction b on a.id::text = b.mem_no::text 
where b.mem_no notnull -- 49469

select a.*
from "source".hk_daily_transaction b
left join "source".hk_centanet_sale_transaction a on a.id::text = b.mem_no::text 
where a.id notnull -- 49469


select id::text from "source".hk_centanet_sale_transaction where ins_date::date > '2021-01-01'::date
except 
select mem_no::text from "source".hk_daily_transaction
-- 114442 --> 18867 after 2021

select mem_no::text from "source".hk_daily_transaction
except 
select id::text from "source".hk_centanet_sale_transaction -- 251818
where ins_date::date > '2021-01-01'::date -- 254720


select a.*
from "source".hk_midland_realty_sale_transaction a 
left join "source".hk_daily_transaction b on split_part(a.id, left(a.id, 10), 2)::text = b.mem_no::text 
where b.mem_no notnull and a.id ilike 'NO%'
-- 71224

select count(*) from "source".hk_midland_realty_sale_transaction where update_date::date > '2021-01-01'::date -- 114460


select a.*
from "source".hk_midland_realty_sale_transaction a 
left join "source".hk_centanet_sale_transaction b on split_part(a.id, left(a.id, 10), 2)::text = b.id::text 
where b.id notnull and a.id ilike 'NO%' -- 112689
and a.update_date::date > '2021-01-01'::date and b.ins_date::date > '2021-01-01'::date -- 47417

select a.*
from "source".hk_centanet_sale_transaction b
left join "source".hk_midland_realty_sale_transaction a on split_part(a.id, left(a.id, 10), 2)::text = b.id::text 
where a.id notnull and a.id ilike 'NO%' -- 112689



(
with m as ( 
    select building_dwid
    from masterdata_hk.sale_transaction st 
    where building_dwid notnull 
    group by building_dwid 
    having max(address_dwid) <> min(address_dwid)
)
select 'building and address' as relationship_name, count(*) as outstanding_row_cnt
from masterdata_hk.sale_transaction st2 
join m on st2.building_dwid = m.building_dwid
)
union
(
with m as ( 
    select building_dwid
    from masterdata_hk.sale_transaction st 
    where building_dwid notnull 
    group by building_dwid 
    having max(project_dwid) <> min(project_dwid)
)
select 'building and parent project' as relationship_name, count(*) as outstanding_row_cnt
from masterdata_hk.sale_transaction st2 
join m on st2.building_dwid = m.building_dwid
)
union
(
select 'property and parent project' as relationship_name, count(*) as outstanding_row_cnt
from masterdata_hk.sale_transaction st 
left join masterdata_hk.property p on p.property_dwid = st.property_dwid 
where p.project_dwid <> st.project_dwid
)
;

















