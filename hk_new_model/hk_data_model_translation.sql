-- translation 1st round:

create table translation_hk.project_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	project_dwid text NULL,
	project_name_text text NULL,
	project_display_name text NULL,
	project_display_name_cn text NULL,
	project_display_name_hk text NULL,
	project_display_name_my text NULL,
	project_display_name_id text NULL,
	CONSTRAINT translateion_hk_project_translation_pkey PRIMARY KEY (id),
	CONSTRAINT translateion_hk_project_translation_fk_project_dwid FOREIGN KEY (project_dwid) REFERENCES masterdata_hk.project(project_dwid)
);

create table translation_hk.address_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	address_dwid text NULL,
	full_address_text text NULL,
	address_display_text text NULL,
	address_display_text_cn text NULL,
	address_display_text_hk text NULL,
	address_display_text_my text NULL,
	address_display_text_id text NULL,
	CONSTRAINT translateion_hk_address_translation_pkey PRIMARY KEY (id),
	CONSTRAINT translateion_hk_address_translation_fk_address_dwid FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid)
);

create table translation_hk.company_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	company_dwid text NULL,
	company_name text NULL,
	company_display_name text NULL,
	company_display_name_cn text NULL,
	company_display_name_hk text NULL,
	company_display_name_my text NULL,
	company_display_name_id text NULL,
	CONSTRAINT translateion_hk_company_translation_pkey PRIMARY KEY (id),
	CONSTRAINT translateion_hk_company_translation_fk_company_dwid FOREIGN KEY (company_dwid) REFERENCES masterdata_common.company(company_dwid)
);

create table translation_hk.building_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	building_dwid text NULL,
	building_name text NULL,
	block_num text NULL,
	block_num_cn text NULL,
	block_num_hk text NULL,
	block_num_my text NULL,
	block_num_id text NULL,
	CONSTRAINT translateion_hk_building_translation_pkey PRIMARY KEY (id),
	CONSTRAINT translateion_hk_building_translation_fk_building_dwid FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid)
);-- make use of data_warehouse_hk


create table translation_hk.location_poi_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	location_poi_dwid text NULL,
	location_poi_name text NULL,
	location_poi_display_text text NULL,
	location_poi_display_text_cn text NULL,
	location_poi_display_text_hk text NULL,
	location_poi_display_text_my text NULL,
	location_poi_display_text_id text NULL,
	CONSTRAINT translateion_hk_location_poi_translation_pkey PRIMARY KEY (id),
	CONSTRAINT translateion_hk_location_poi_translation_fk_location_poi_dwid FOREIGN KEY (location_poi_dwid) REFERENCES masterdata_hk.poi(location_poi_dwid)
);


select location_poi_dwid, count(*)
from masterdata_hk.poi
group by 1 having count(*) > 1;


ALTER TABLE masterdata_hk.poi ADD CONSTRAINT location_poi_un_dwid UNIQUE (location_poi_dwid);


with new_launch_project_translation as (
select mapp.project_dwid, project_zh as project_display_name_hk, row_number() over(partition by mapp.project_dwid) as rn
from reference.hk_new_launch_meta a 
left join premap_hk.new_launch_schematic_to_dwid mapp on lower(a.project_name) = lower(mapp.original_project_name)
where project_zh notnull and project_zh ~ '[^A-Za-z0-9 .-]+'
group by 1,2
)
insert into translation_hk.project_translation
(project_dwid, project_display_name_hk, project_display_name, project_name_text)
select a.project_dwid, a.project_display_name_hk, b.project_display_name, b.project_name_text
from new_launch_project_translation a
left join masterdata_hk.project b on a.project_dwid = b.project_dwid
where a.rn = 1; -- 33

'''
update translation_hk.project_translation a
set project_name_text = b.project_name_text , project_display_name = b.project_display_name 
from masterdata_hk.project b where a.project_dwid = b.project_dwid
and a.project_name_text isnull
'''


with new_launch_project_translation as (
select mapp.project_dwid, project_zh as project_display_name_hk, row_number() over(partition by mapp.project_dwid order by project_zh) as rn
from reference.hk_new_launch_meta a 
left join premap_hk.new_launch_schematic_to_dwid mapp on lower(a.project_name) = lower(mapp.original_project_name)
left join translation_hk.project_translation c on mapp.project_dwid = c.project_dwid 
where project_zh notnull and project_zh ~ '[^A-Za-z0-9 .-]+' and c.project_dwid isnull
group by 1,2
)
insert into translation_hk.project_translation
(project_dwid, project_display_name_hk, project_display_name, project_name_text)
select a.project_dwid, a.project_display_name_hk, b.project_display_name, b.project_name_text
from new_launch_project_translation a
left join masterdata_hk.project b on a.project_dwid = b.project_dwid
where a.rn = 1;

ALTER TABLE translation_hk.project_translation ADD CONSTRAINT project_translation_un_dwid UNIQUE (project_dwid);

ALTER TABLE translation_hk.company_translation ADD CONSTRAINT company_translation_un_dwid UNIQUE (company_dwid);


with new_launch_company_translation as (
select c.company_dwid , developer_zh as company_display_name_hk,-- developer,
row_number() over(partition by developer order by developer_zh) as rn
from reference.hk_new_launch_meta a 
left join masterdata_common.company c on lower(c.company_display_name) = lower(a.developer)
left join translation_hk.company_translation ct on lower(ct.company_display_name) = lower(a.developer)
where ct.company_dwid isnull and developer_zh notnull and developer_zh ~ '[^A-Za-z0-9 .-]+'
group by 1,2,developer--,3 order by 1
)
insert into translation_hk.company_translation
(
	company_dwid, company_display_name_hk, company_display_name, company_name
)
select a.company_dwid, a.company_display_name_hk, b.company_display_name , b.company_name 
from new_launch_company_translation a
left join masterdata_common.company b on a.company_dwid = b.company_dwid
where a.rn = 1; --31


-- test:
select
	c.company_dwid,
	c.company_display_name,
	c.gov_tax_id as company_code,
	coalesce(ct.company_display_name_cn, c.company_display_name) as company_display_name_cn,
	coalesce(ct.company_display_name_hk, c.company_display_name) as company_display_name_hk,
	coalesce(ct.company_display_name_my, c.company_display_name) as company_display_name_my,
	'HK' as country_code,
	c.slug,
	c.company_desc as company_description,
	c.incorporation_year,
	c.company_url,
	null as location_display_text,
	null as location_display_text_cn,
	null as location_display_text_hk,
	null as location_display_text_my,
	null as parent_company_dwids,
	'HKD' as currency_code,
	null::int as completed_gdv,
	null::int as company_size
from masterdata_common.company c
	left join translation_hk.company_translation ct
		on c.company_dwid = ct.company_dwid
where country_code = 'cn';



ALTER TABLE translation_hk.address_translation ADD CONSTRAINT address_translation_un_dwid UNIQUE (address_dwid);


with new_launch_address_translation as (
select 
	mapp.address_dwid, 
	coalesce(uid.addressdisplaytexthk, address_zh) as address_display_text_hk, 
	uid.addressdisplaytextcn as address_display_text_cn,
	row_number() over(partition by mapp.address_dwid order by address_zh) as rn
from reference.hk_new_launch_meta a 
left join "source".user_input_dmproject_uniqueness uid on lower(uid.projectdisplayname) = lower(a.project_name) and uid.flag in ('create', 'update')
left join premap_hk.new_launch_schematic_to_dwid mapp on lower(a.project_name) = lower(mapp.original_project_name)
left join translation_hk.address_translation c on mapp.address_dwid = c.address_dwid
where address_zh notnull and address_zh ~ '[^A-Za-z0-9 .-]+' and c.address_dwid isnull
and (uid.countrycode = 'hk' or uid.countrycode isnull)
group by 1,2,3,address_zh
)
insert into translation_hk.address_translation
(
	address_dwid, address_display_text_hk, address_display_text_cn, address_display_text, full_address_text
)
select a.address_dwid, a.address_display_text_hk, a.address_display_text_cn, b.address_display_text , b.full_address_text
from new_launch_address_translation a
left join masterdata_hk.address b on a.address_dwid = b.address_dwid
where a.rn = 1; -- 198





ALTER TABLE translation_hk.location_poi_translation ADD CONSTRAINT location_poi_translation_un_dwid UNIQUE (location_poi_dwid);

select a.location_poi_dwid , a.poi_name , a.poi_address , a.poi_type , a.poi_subtype , b.poi_ch
from masterdata_hk.poi a
left join reference.hk_new_launch_poi b
on lower(a.poi_type) = lower(b.poi_type) and lower(a.poi_name) = lower(b.poi) and lower(a.poi_address) = lower(b.poi_address)
where b.poi_ch notnull or (a.poi_type = 'mtr station' and a.poi_name ilike '%station%')


with new_launch_poi_translation as (
select 
	a.location_poi_dwid , 
	b.poi_ch as location_poi_display_text_hk,
	row_number() over(partition by a.location_poi_dwid order by poi_ch) as rn
from masterdata_hk.poi a
left join reference.hk_new_launch_poi b on lower(a.poi_type) = lower(b.poi_type) and lower(a.poi_name) = lower(b.poi) and lower(a.poi_address) = lower(b.poi_address)
left join translation_hk.location_poi_translation c on a.location_poi_dwid = c.location_poi_dwid
where ((b.poi_ch notnull and poi_ch ~ '[^A-Za-z0-9 .-]+') or (a.poi_type = 'mtr station' and a.poi_name ilike '%station%')) 
and c.location_poi_dwid isnull
group by 1,2
)
insert into translation_hk.location_poi_translation
(
	location_poi_dwid, location_poi_display_text_hk, location_poi_display_text,location_poi_name
)
select a.location_poi_dwid, a.location_poi_display_text_hk, initcap(b.poi_name) as location_poi_display_text , b.poi_name as location_poi_name
from new_launch_poi_translation a
left join masterdata_hk.poi b on a.location_poi_dwid = b.location_poi_dwid
where a.rn = 1;


select location_poi_dwid
from translation_hk.location_poi_translation
except
select location_poi_dwid
from masterdata_hk.poi;

select location_poi_dwid
from masterdata_hk.poi
except 
select location_poi_dwid
from translation_hk.location_poi_translation; -- 1938


with poi_translation_base as (
select location_poi_dwid
from masterdata_hk.poi
except 
select location_poi_dwid
from translation_hk.location_poi_translation
)
insert into translation_hk.location_poi_translation
(
	location_poi_dwid, location_poi_display_text_hk, location_poi_display_text,location_poi_name
)
select base.location_poi_dwid, b.poi_ch as location_poi_display_text_hk, initcap(a.poi_name) as location_poi_display_text , a.poi_name as location_poi_name
from poi_translation_base base
left join masterdata_hk.poi a on base.location_poi_dwid = a.location_poi_dwid
left join reference.hk_new_launch_poi b on lower(a.poi_subtype) = lower(b.poi_type) and lower(a.poi_name) = lower(b.poi) and lower(a.poi_address) = lower(b.poi_address)
where b.poi_ch notnull
group by 1,2,3,4; -- 660









-- translation 2nd round:
with project_translation_base as (
select
	p.project_dwid ,
 	p.project_name_text ,
 	p.project_display_name,
 	hpa.estate_ch 
from masterdata_hk.project p 
left join translation_hk.project_translation pt on p.project_dwid = pt.project_dwid 
left join masterdata_hk.address a on p.address_dwid = a.address_dwid 
left join reference.hk_post_address hpa on lower(p.project_name_text) = lower(hpa.estate) and lower(a.city_subarea) = lower(hpa.district_name) 
where pt.project_dwid isnull and hpa.estate notnull
group by 1,2,3,4
)
insert into translation_hk.project_translation 
(project_dwid, project_name_text, project_display_name, project_display_name_hk)
select project_dwid, project_name_text, project_display_name, estate_ch as project_display_name_hk
from project_translation_base
;

with base as (
select project_dwid, count(*)
from translation_hk.project_translation 
group by 1 having count(*) > 1
)
select pt.*, p.*
from base 
left join translation_hk.project_translation pt on base.project_dwid = pt.project_dwid
left join masterdata_hk.project p on pt.project_name_text = p.project_name_text 
where base.project_dwid notnull;


with base as (
select project_dwid, count(*)
from translation_hk.project_translation 
group by 1 having count(*) > 1
)
delete from translation_hk.project_translation
where project_dwid in (select project_dwid from base);


CREATE UNIQUE INDEX project_translation_un_dwid ON translation_hk.project_translation  USING btree (project_dwid);
CREATE INDEX project_translation_idx ON translation_hk.project_translation  USING btree (project_dwid);
CREATE INDEX project_translation_project_name_text_idx ON translation_hk.project_translation  USING btree (project_name_text);


insert into translation_hk.project_translation
(project_dwid, project_name_text, project_display_name)
select project_dwid, project_name_text, project_display_name
from masterdata_hk.project
where project_dwid not in (select project_dwid from translation_hk.project_translation)


with project_translation_base as (
select
	p.project_dwid ,
 	p.project_name_text ,
 	p.project_display_name,
 	hpa.estate_ch ,
 	row_number() over (partition by p.project_dwid) as seq
from masterdata_hk.project p 
left join translation_hk.project_translation pt on p.project_dwid = pt.project_dwid 
left join masterdata_hk.address a on p.address_dwid = a.address_dwid 
left join reference.hk_post_address hpa on lower(p.project_name_text) = lower(hpa.estate) and lower(a.city_subarea) = lower(hpa.district_name) 
where pt.project_display_name_hk isnull and hpa.estate notnull
group by 1,2,3,4
)
, project_translation as (
select project_dwid, project_name_text, project_display_name, estate_ch as project_display_name_hk
from project_translation_base
where project_dwid not in (select project_dwid from project_translation_base where seq >1)
)
update translation_hk.project_translation a
set project_display_name_hk = b.project_display_name_hk
from project_translation b where a.project_dwid = b.project_dwid
;

CREATE UNIQUE INDEX building_translation_un_dwid ON translation_hk.building_translation USING btree (building_dwid);
CREATE INDEX building_translation_idx ON translation_hk.building_translation  USING btree (building_dwid);
CREATE INDEX building_translation_building_name_text_idx ON translation_hk.building_translation USING btree (building_name);


insert into translation_hk.building_translation
(building_dwid, building_name, building_display_name)
select building_dwid, lower(coalesce(building_name_text, building_display_name)) as building_name, building_display_name
from masterdata_hk.building
where building_dwid not in (select building_dwid from translation_hk.building_translation)


with building_translation_base as (
select
	b.building_dwid,
 	b.building_name_text,
 	b.building_display_name,
 	hpa.building_ch,
 	row_number() over (partition by b.building_dwid) as seq
from masterdata_hk.building b
left join translation_hk.building_translation bt on b.building_dwid = bt.building_dwid 
left join masterdata_hk.address a on b.address_dwid = a.address_dwid 
left join reference.hk_post_address hpa on lower(b.building_name_text) = lower(hpa.building) and lower(a.city_subarea) = lower(hpa.district_name) 
where bt.building_display_name_hk isnull and hpa.building notnull
group by 1,2,3,4
)
, building_translation as (
select building_dwid, building_name_text, building_display_name, building_ch as building_display_name_hk
from building_translation_base
where building_dwid not in (select building_dwid from building_translation_base where seq >1)
)
update translation_hk.building_translation a
set building_display_name_hk = b.building_display_name_hk
from building_translation b where a.building_dwid = b.building_dwid
; -- 



with building_translation_base as (
select
	b.building_dwid,
 	b.building_name_text,
 	b.building_display_name,
 	hpa.building_ch,
 	row_number() over (partition by b.building_dwid) as seq
from masterdata_hk.building b
left join translation_hk.building_translation bt on b.building_dwid = bt.building_dwid 
left join masterdata_hk.address a on b.address_dwid = a.address_dwid 
left join reference.hk_post_address hpa on lower(b.building_name_text) = lower(hpa.building) and lower(a.city_subarea) = lower(hpa.district_name) 
where bt.building_display_name_hk isnull and hpa.building notnull
group by 1,2,3,4
)
--, building_translation as (
select * --building_dwid, building_name_text, building_display_name, building_ch as building_display_name_hk
from building_translation_base
where building_dwid in (select building_dwid from building_translation_base where seq >1)


delete from translation_hk.building_translation
where building_name isnull; -- 17880


select building_name, count(*)
from translation_hk.building_translation
where building_display_name_hk isnull
group by 1;


select 
	building_name,
	case when building_name ilike 'House%' then '房 ' || replace(initcap(building_name), 'House ', '')
		when building_name like '%House%' then replace(building_name, 'House ', '') || ' 房'
		when building_name like 'Maisonette Building%' then '复式 ' || replace(building_name, 'Maisonette Building ', '')
		when building_name like 'Mansion%' then '豪宅 ' || replace(building_name, 'Mansion ', '')
		when building_name like 'Villa%' then '別墅 ' || replace(building_name, 'Villa ', '')
		when building_name like 'Tower%' then replace(building_name, 'Tower ', '') || ' 座'
		when building_name like '%Tower%' then replace(building_name, 'Tower ', '') || ' 座'
		when building_name = '-' then null
		end as building_display_name_hk
from translation_hk.building_translation
where building_display_name_hk isnull
group by 1;

select building_name, replace(building_name, 'house ', '') || '號' as building_display_name_hk
from translation_hk.building_translation
where building_display_name_hk isnull and building_name ilike 'House%'
group by 1;

update translation_hk.building_translation
set building_display_name_hk = replace(building_name, 'house ', '') || '號'
where building_display_name_hk isnull and building_name ilike 'House%'; 
-- 第一街26號, 第五街47號

update translation_hk.building_translation
set building_display_name_hk = upper(building_display_name_hk) 
where building_name ilike 'House%' and building_display_name_hk notnull

update translation_hk.building_translation
set building_display_name_hk = upper(replace(building_name, 'house ', '') || '號')
where building_name ilike 'House%' and building_display_name_hk ~ '^[A-Z]' and building_display_name_hk ilike '% 號'

update translation_hk.building_translation
set building_display_name_hk = replace(building_display_name_hk, ' 號', '號') 
where building_display_name_hk ilike '% 號%'
;

select 
	building_name, building_display_name_hk, count(*)
from translation_hk.building_translation
where building_name ~ '[0-9]' and building_name !~ '[a-z]'
group by 1,2;

update translation_hk.building_translation
set building_display_name_hk = building_name || '號'
where building_name ~ '[0-9]' and building_name !~ '[a-z]' and building_display_name_hk isnull

select 
	building_name, building_display_name_hk, count(*)
from translation_hk.building_translation
where building_name ilike 'block%' and building_name ~ '[0-9]$'
group by 1,2;

update translation_hk.building_translation
set building_display_name_hk = upper(replace(building_name, 'block ', '') || '座')
where building_name ilike 'block%' and building_name ~ '[0-9]$' and building_display_name_hk isnull

select 
	building_name, building_display_name_hk, count(*)
from translation_hk.building_translation
where building_name not ilike '% %' and length(building_name) <=3
group by 1,2;



CREATE TABLE translation_hk.phase_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	building_dwid text NULL,
	phase_name text NULL,
	phase_name_cn text NULL,
	phase_name_hk text NULL,
	phase_name_my text NULL,
	phase_name_id text NULL,
	CONSTRAINT translateion_hk_phase_translation_pkey PRIMARY KEY (id),
	CONSTRAINT translateion_hk_phase_translation_fk_building_dwid FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid)
);

CREATE INDEX phase_translation_phase_name_idx ON translation_hk.phase_translation USING btree (phase_name);
CREATE INDEX phase_translation_idx ON translation_hk.phase_translation USING btree (building_dwid);
CREATE UNIQUE INDEX phase_translation_un_dwid ON translation_hk.phase_translation USING btree (building_dwid);


insert into translation_hk.phase_translation
(building_dwid, phase_name)
select building_dwid, a.development_phase as phase_name
from masterdata_hk.building b
left join masterdata_hk.address a on b.address_dwid = a.address_dwid 
where a.development_phase notnull
group by 1, 2;



with phase_translation_base as (
select
	b.building_dwid,
 	a.development_phase as phase_name,
 	hpa.phase_ch ,
 	row_number() over (partition by b.building_dwid) as seq
from masterdata_hk.building b
left join masterdata_hk.address a on b.address_dwid = a.address_dwid 
left join reference.hk_post_address hpa on lower(b.building_name_text) = lower(hpa.building) and lower(a.city_subarea) = lower(hpa.district_name) and lower(a.development_phase) = lower(hpa.phase)
where hpa.phase notnull
group by 1,2,3
)
, phase_translation as (
select building_dwid, phase_name, phase_ch as phase_name_hk
from phase_translation_base
where building_dwid not in (select building_dwid from phase_translation_base where seq >1)
)
update translation_hk.phase_translation a
set phase_name_hk = b.phase_name_hk
from phase_translation b where a.building_dwid = b.building_dwid and a.phase_name = b.phase_name
; -- 4024

with phase_translation as (
select phase_name , phase_name_hk 
from translation_hk.phase_translation
where phase_name_hk notnull group by 1,2
)
update translation_hk.phase_translation a
set phase_name_hk = b.phase_name_hk
from phase_translation b where a.phase_name = b.phase_name; --8094




CREATE UNIQUE INDEX translateion_hk_address_translation_pkey ON translation_hk.address_translation USING btree (id);
ALTER TABLE translation_hk.address_translation ADD CONSTRAINT translateion_hk_address_translation_fk_address_dwid FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);
CREATE INDEX address_translation_full_address_text_idx ON translation_hk.address_translation USING btree (full_address_text);
CREATE INDEX address_translation_address_display_text_idx ON translation_hk.address_translation USING btree (address_display_text);
CREATE INDEX address_translation_idx ON translation_hk.address_translation USING btree (address_dwid);
CREATE UNIQUE INDEX address_translation_un_dwid ON translation_hk.address_translation USING btree (address_dwid);

insert into translation_hk.address_translation 
(id, address_dwid, full_address_text , address_display_text)
select id, address_dwid, full_address_text , address_display_text
from masterdata_hk.address a 
where address_dwid not in (select address_dwid from translation_hk.address_translation);



CREATE TABLE translation_hk.geographic_area_translation (
	id text NULL,
	location_level text NULL,
	"label" text NULL,
	label_cn text NULL,
	label_hk text NULL,
	label_my text NULL,
	label_id text NULL
);

CREATE UNIQUE INDEX translateion_hk_geographic_area_translation_pkey ON translation_hk.geographic_area_translation USING btree (id);
CREATE INDEX geographic_area_translation_label_idx ON translation_hk.geographic_area_translation USING btree (label);
CREATE INDEX geographic_area_translation_idx ON translation_hk.geographic_area_translation USING btree (id);

insert into translation_hk.geographic_area_translation 
(id, location_level , "label" )
select id, location_level , "label" 
from masterdata_hk.geographic_area a;

with base as (
select city_area  , city_area_chinese  
from reference.ref_hk_address_hierarchy_subarea rhahs 
group by 1,2
)
update translation_hk.geographic_area_translation a 
set label_hk = b.city_area_chinese
from base b where lower(a."label") = lower(b.city_area) and a.location_level = 'region'
;


with base as (
select corrected_city_subarea  , corrected_city_subarea_chinese  
from reference.ref_hk_address_hierarchy_subarea rhahs 
group by 1,2
)
update translation_hk.geographic_area_translation a 
set label_hk = b.corrected_city_subarea_chinese
from base b where lower(a."label") = lower(b.corrected_city_subarea) and a.location_level = 'zone'
;


CREATE TABLE translation_hk.street_translation (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	city text NULL,
	city_area text NULL,
	city_subarea text NULL,
	street_name text NULL,
	street_name_cn text NULL,
	street_name_hk text NULL,
	street_name_my text NULL,
	street_name_id text NULL,
	CONSTRAINT translateion_hk_street_translation_pkey PRIMARY KEY (id)
);
CREATE INDEX street_translation_street_name_idx ON translation_hk.street_translation USING btree (street_name);
CREATE INDEX street_translation_idx ON translation_hk.street_translation USING btree (id);

insert into translation_hk.street_translation 
(city , city_area, city_subarea , street_name )
select city , city_area, city_subarea , street_name 
from masterdata_hk.address
group by 1,2,3,4;


with street_translation_base as (
select a.id, a.city , a.city_area, a.city_subarea , a.street_name, hpa.street_name_ch , row_number() over (partition by a.id) as seq
from translation_hk.street_translation a
left join reference.hk_post_address hpa on lower(a.city) = replace(lower(hpa.zone), 'hong kong', 'hong kong island') and lower(a.city_subarea) = lower(hpa.district_name) and lower(a.street_name) = lower(hpa.street_name)
where hpa.street_name_ch notnull and a.street_name_hk isnull
group by 1,2,3,4,5,6
)
, street_translation as (
select id, street_name, street_name_ch as street_name_hk
from street_translation_base
where id not in (select id from street_translation_base where seq > 1)
)
update translation_hk.street_translation a
set street_name_hk = b.street_name_hk
from street_translation b where a.id = b.id
; -- 2677 + 871


with address_project_map as (
select a.address_dwid , p.project_dwid 
from masterdata_hk.address a
left join masterdata_hk.project p on lower(a.development)=lower(p.project_name_text) 
and trim(initcap(reverse(split_part(reverse(a.full_address_text), ',', 4)||' ,'||split_part(reverse(a.full_address_text), ',', 3)||' ,'||split_part(reverse(a.full_address_text), ',', 2)))) = p.location_display_text 
where p.project_dwid notnull
group by 1,2
)
, address_building_map as (
select a.address_dwid, b.building_dwid , row_number() over (partition by a.address_dwid) as seq
from masterdata_hk.address a
left join masterdata_hk.building b on a.address_dwid = b.address_dwid 
where b.building_dwid notnull
group by 1, 2
)
, address_street_map as (
select a.address_dwid , b.id --, row_number() over (partition by a.address_dwid) as seq
from masterdata_hk.address a
left join translation_hk.street_translation b on a.city = b.city and a.city_area = b.city_area and a.city_subarea = b.city_subarea and a.street_name = b.street_name 
)
, address_translation as (
select 
	a.address_dwid , a.address_display_text ,
	case when address_type_code = 'point-address' or address_type_code = 'building-address' or address_type_code = 'street-address' then 
		replace(initcap(replace(trim(replace(replace(replace(replace(trim(trim(coalesce(pt.project_display_name_hk, development, '') || ',' || coalesce(pht.phase_name_hk, development_phase, '') || ',' || coalesce(bt.building_display_name_hk, address_building, '') || ',' || 
		coalesce(st.street_name_hk, a.street_name, '') || ' ' || coalesce(a.address_number, '') || '号' || ',' || coalesce(ga2.label_hk, '') || ',' || coalesce(ga1.label_hk, ''), ',')), ',,,,', ','), ',,,', ','), ',,', ','), ', ,', ','), ','), ',', ', ')), '''S', '''s')
		when address_type_code = 'project-address' then 
		replace(initcap(replace(trim(trim(replace(replace(coalesce(pt.project_display_name_hk, development, '') || ',' ||coalesce(st.street_name_hk, a.street_name, '') || ',' 
		|| coalesce(ga2.label_hk, '') || ',' || coalesce(ga1.label_hk, ''), ',,,', ','), ',,', ','), ',')), ',', ', ')), '''S', '''s')
		end as address_display_text_hk,
	case when address_type_code = 'point-address' or address_type_code = 'building-address' or address_type_code = 'street-address' then 
		replace(initcap(replace(trim(replace(replace(replace(replace(trim(trim(coalesce(pt.project_display_name_cn, development , '') || ',' || coalesce(pht.phase_name_cn, development_phase, '') || ',' || coalesce(bt.building_display_name_cn, address_building, '') || ',' || 
		coalesce(st.street_name_hk, a.street_name, '') || ' ' || coalesce(a.address_number, '') || '號' || ',' || coalesce(ga2.label_cn, '') || ',' || coalesce(ga1.label_cn, ''), ',')), ',,,,', ','), ',,,', ','), ',,', ','), ', ,', ','), ','), ',', ', ')), '''S', '''s')
		when address_type_code = 'project-address' then 
		replace(initcap(replace(trim(trim(replace(replace(coalesce(pt.project_display_name_cn, development, '') || ',' ||coalesce(st.street_name_cn, a.street_name, '') || ',' 
		|| coalesce(ga2.label_cn, '') || ',' || coalesce(ga1.label_cn, ''), ',,,', ','), ',,', ','), ',')), ',', ', ')), '''S', '''s')
		end as address_display_text_cn,
	row_number() over (partition by a.address_dwid) as seq
from translation_hk.address_translation att
left join masterdata_hk.address a on att.address_dwid = a.address_dwid 
left join address_project_map ap on a.address_dwid = ap.address_dwid 
left join translation_hk.project_translation_wiz_cn pt on ap.project_dwid = pt.project_dwid 
left join address_building_map ab on a.address_dwid = ab.address_dwid and ab.seq = 1
left join translation_hk.building_translation_wiz_cn bt on ab.building_dwid = bt.building_dwid 
left join translation_hk.phase_translation_wiz_cn pht on ab.building_dwid = pht.building_dwid
left join address_street_map asm on a.address_dwid = asm.address_dwid
left join translation_hk.street_translation_wiz_cn st on asm.id = st.id
left join translation_hk.geographic_area_translation_wiz_cn ga1 on lower(a.city) = lower(ga1."label") and ga1.location_level = 'district'
left join translation_hk.geographic_area_translation_wiz_cn ga2 on lower(a.city_subarea) = lower(ga2."label") and ga2.location_level = 'zone'
--where att.address_display_text_hk isnull
group by 1,2,3,4 -- 68828
)
update translation_hk.address_translation a 
set address_display_text_hk = b.address_display_text_hk, address_display_text_cn = b.address_display_text_cn
from address_translation b
where a.address_dwid = b.address_dwid --and a.address_display_text_hk isnull; -- 68828
and a.data_source isnull;


SELECT f_clone_table('translation_hk', 'location_poi_translation_wiz_cn', 'translation_hk', 'location_poi_translation', TRUE, TRUE);


select building_block_number, count(*)
from masterdata_hk.building b 
group by 1
;






