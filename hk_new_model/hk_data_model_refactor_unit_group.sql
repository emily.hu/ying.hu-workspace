-- document process of refactoring HK unit_group

-- Pipeline:
--step1.deactivate hk unit_group replication task temporarily before the refactoring is done

-- metadata table:
--step2.update metadata.md_attribute
-- old: '2629286	project_dwid	ID	text	null	unit_group	null 	null	null	true	10	project		null 	null	null'
-- new: '2629286	unit_group_parent_dwid	ID	text	the parent object of the unit group, if it has project name then use project_dwid, if it's single building without project name then use building_dwid	unit_group		null 	null	true	true	10	null 	null 	null	null'	

-- change log table 
-- step3.rename 'unit_group_change_log' to be 'unit_group_change_log_old' and create a new 'unit_group_change_log' table with latest table structure

CREATE TABLE change_log_hk.unit_group_change_log (
	id int8 NULL,
	unit_group_dwid text NULL,
	unit_group_parent_dwid text NULL,
	unit_group text NULL,
	unit_type text NULL,
	unit_subtype_code text NULL,
	gross_floor_area_sqm numeric NULL,
	gross_floor_area_sqft numeric NULL,
	net_floor_area_sqm numeric NULL,
	net_floor_area_sqft numeric NULL,
	unit_count int4 NULL,
	bedroom_count int4 NULL,
	bathroom_count int4 NULL,
	other_room_count int4 NULL,
	unit_group_order int4 NULL,
	is_penthouse bool NULL,
	country_code text NULL,
	property_type_code varchar NULL,
	slug text NULL,
	cr_record_action varchar NULL,
	cr_action_date date NULL,
	cr_id int4 NULL,
	cr_change_id int4 NULL
);


-- core table:
--step4.refactor masterdata_hk.unit_group table

--4.1 delete `project_dwid` foreign key, rename column `project_dwid` to `unit_group_parent_dwid`, rename index name
ALTER TABLE masterdata_hk.unit_group DROP CONSTRAINT unit_group_fk_project_dwid;
ALTER TABLE masterdata_hk.unit_group RENAME COLUMN project_dwid TO unit_group_parent_dwid;
ALTER INDEX masterdata_hk.unit_group_project_dwid_idx RENAME TO unit_group_parent_dwid_idx;

--4.2 add single building level unit groups using building_dwid as unit_group_parent_dwid, slug start with 'building/...'

select metadata.fn_create_change_request(
    'hk-new-unit_group-records-2022-09-28', 'huying','huying'
); --1184

call metadata.sp_add_change_table(1184::int, 'hk', replace('unit_group', '-', '_'));

insert into branch_hk.unit_group_cr_1184
(
	unit_group_parent_dwid,unit_group,unit_type,unit_subtype_code,gross_floor_area_sqm,gross_floor_area_sqft,net_floor_area_sqm,net_floor_area_sqft,
	unit_count,bedroom_count,bathroom_count,other_room_count,country_code,property_type_code, cr_record_action
)
with unit_group_test as (
select p.building_dwid , bedroom_count::int , bathroom_count::int , other_room_count::int , gross_floor_area_sqm::int , net_floor_area_sqm::int , property_type_code , count(*) as unit_count
from masterdata_hk.property p 
where p.unit_group_dwid isnull and p.project_dwid isnull and p.building_dwid notnull and bedroom_count notnull and bedroom_count != 0
group by 1,2,3,4,5,6,7 -- 24014
)
select 
	--null as unit_group_dwid,
	a.building_dwid as unit_group_parent_dwid,
	lower(coalesce(b.building_display_name  || ' - ', '') || 'C' || bedroom_count) as unit_group,
	lower(a.bedroom_count || ' bedroom') as unit_type,
	lower('C' || a.bedroom_count) as unit_subtype_code,
	a.gross_floor_area_sqm,
	f_sqm2sqft(a.gross_floor_area_sqm)::int as gross_floor_area_sqft,
	a.net_floor_area_sqm,
	f_sqm2sqft(a.net_floor_area_sqm)::int as net_floor_area_sqft,
	a.unit_count,
	a.bedroom_count,
	a.bathroom_count,
	a.other_room_count,
	--null::int as unit_group_order,
 	--null::boolean as is_penthouse,
 	'hk' as country_code,
 	a.property_type_code,
 	'insert' as cr_record_action
from unit_group_test a
left join masterdata_hk.building b on a.building_dwid = b.building_dwid
; -- 24014


update branch_hk.unit_group_cr_1184
set unit_group_dwid = api.get_dwid(country_code, 'unit_group', id)
where unit_group_dwid isnull; -- 24014


with base as (
select 
	ug.id,
	trim(trim(replace(replace('building/hk/'||ug.property_type_code||'/'||replace(trim(replace(split_part(ug.unit_group, reverse(split_part(reverse(ug.unit_group), ' - ', 1)), 1), ' - ', ' ')), ' ', '-')
		||'-'||b.id||'/'||ug.bedroom_count||'/'||replace(replace(ug.unit_subtype_code, ' ', '-'), '/', '-')||'-'||coalesce(ug.gross_floor_area_sqm::text, ''),'(',''),')',''), '-')) as slug
from branch_hk.unit_group_cr_1184 ug
left join masterdata_hk.building b on ug.unit_group_parent_dwid = b.building_dwid
)
update branch_hk.unit_group_cr_1184 a
set slug = b.slug
from base b where a.id = b.id 
and a.slug isnull
; -- 24014


call metadata.sp_submit_change_request(1184, 'huying');

call metadata.sp_approve_change_request(1184, 'huying');

call metadata.sp_merge_change_request(1184);


-- temp fix --
'''
update change_log_hk.unit_group_change_log a
set property_type_code = b.property_type_code 
from branch_hk.unit_group_cr_1184 b
where a.id = b.id;

update masterdata_hk.unit_group a
set property_type_code = b.property_type_code 
from branch_hk.unit_group_cr_1184 b
where a.id = b.id
and a.slug ilike 'building%';

select unit_group_dwid , split_part(slug, '/', 3) as property_type_code, slug 
from masterdata_hk.unit_group
where property_type_code isnull;

update masterdata_hk.unit_group
set property_type_code = split_part(slug, '/', 3)
where property_type_code isnull; -- 44097

select a.property_dwid, b.unit_group_dwid
from masterdata_hk.property a 
left join masterdata_hk.unit_group b
on a.project_dwid = b.unit_group_parent_dwid 
and f_prep_dw_id(a.bedroom_count::int::text) = f_prep_dw_id(b.bedroom_count::text) 
and f_prep_dw_id(a.gross_floor_area_sqm::int::text) = f_prep_dw_id(b.gross_floor_area_sqm::text) 
and f_prep_dw_id(a.net_floor_area_sqm::int::text) = f_prep_dw_id(b.net_floor_area_sqm::text) 
and f_prep_dw_id(a.other_room_count::int::text) = f_prep_dw_id(b.other_room_count::text) 
and a.property_type_code = b.property_type_code 
where a.project_dwid notnull and a.unit_group_dwid isnull and b.unit_group_dwid notnull; -- 0
'''

--4.3 update unit_group_dwid in property table for units in single building

select metadata.fn_create_change_request(
    'hk-update-unit_group_dwid-in-property-2022-09-29', 'huying','huying'
); --1197

call metadata.sp_add_change_table(1197::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_1197
with unit_group_dwidbase as (
select a.property_dwid, b.unit_group_dwid
from masterdata_hk.property a 
left join masterdata_hk.unit_group b
on a.building_dwid = b.unit_group_parent_dwid 
and f_prep_dw_id(a.bedroom_count::int::text) = f_prep_dw_id(b.bedroom_count::text) 
and f_prep_dw_id(a.gross_floor_area_sqm::int::text) = f_prep_dw_id(b.gross_floor_area_sqm::text) 
and f_prep_dw_id(a.net_floor_area_sqm::int::text) = f_prep_dw_id(b.net_floor_area_sqm::text) 
and f_prep_dw_id(a.other_room_count::int::text) = f_prep_dw_id(b.other_room_count::text) 
and a.property_type_code = b.property_type_code 
where --a.project_dwid isnull and a.building_dwid notnull and 
a.unit_group_dwid isnull and b.unit_group_dwid notnull -- 874 --> 81275
)
select 
	p.id,p.property_dwid,p.address_dwid,p.building_dwid,b.unit_group_dwid,p.project_dwid,property_type_code,
	property_name,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	ownership_type_code,bedroom_count,bathroom_count,other_room_count,net_floor_area_sqm,gross_floor_area_sqm,
	slug,country_code,is_active,property_display_text,data_source,data_source_id,status_code,'update' as cr_record_action
from masterdata_hk.property p
left join unit_group_dwidbase b on p.property_dwid = b.property_dwid
where b.property_dwid notnull; -- 81275


call metadata.sp_submit_change_request(1197, 'huying');

call metadata.sp_approve_change_request(1197, 'huying');

call metadata.sp_merge_change_request(1197);


-- integration part:
-- step5.new launch data ingestion code (daily transaction, listing do not have change concerning unit_group)
-- for new launch we always find or create project for units so just need to join with project table and use project_dwid as unit_group_parent_dwid

-- redshift part:
-- step6.activate hk unit_group replication task 
-- step7.update ui app tables: unit_group_summary / search_unit_group / (project_summary)


-- pay attention to:
--1/unit_group will have slug start with 'building/...', need to check with frontend whether they will accept them, 
-- and how to add these building level unit groups into unit_group_summary and search_unit_group table (add 'building' columns or coalesce them into 'project' columns);
--2/valuation side need to change the column name when using unit_group, and need to know HK may have building level data, so they may also need to join with building table besides project table;



-- refactore for sg and au as well to keep consistent? or give willy/alice list of steps to do and let them update by themselves

-- SG:

CREATE TABLE change_log_sg.unit_group_change_log (
	id int8 NULL,
	unit_group_dwid text NULL,
	unit_group_parent_dwid text NULL,
	unit_group text NULL,
	unit_type text NULL,
	unit_subtype_code text NULL,
	gross_floor_area_sqm numeric NULL,
	gross_floor_area_sqft numeric NULL,
	net_floor_area_sqm numeric NULL,
	net_floor_area_sqft numeric NULL,
	unit_count int4 NULL,
	bedroom_count int4 NULL,
	bathroom_count int4 NULL,
	other_room_count int4 NULL,
	unit_group_order int4 NULL,
	is_penthouse bool NULL,
	country_code text NULL,
	property_type_code varchar NULL,
	slug text NULL,
	cr_record_action varchar NULL,
	cr_action_date date NULL,
	cr_id int4 NULL,
	cr_change_id int4 NULL
);


CREATE TABLE change_log_au.unit_group_change_log (
	id int8 NULL,
	unit_group_dwid text NULL,
	unit_group_parent_dwid text NULL,
	unit_group text NULL,
	unit_type text NULL,
	unit_subtype_code text NULL,
	gross_floor_area_sqm numeric NULL,
	gross_floor_area_sqft numeric NULL,
	net_floor_area_sqm numeric NULL,
	net_floor_area_sqft numeric NULL,
	unit_count int4 NULL,
	bedroom_count int4 NULL,
	bathroom_count int4 NULL,
	other_room_count int4 NULL,
	unit_group_order int4 NULL,
	is_penthouse bool NULL,
	country_code text NULL,
	property_type_code varchar NULL,
	slug text NULL,
	cr_record_action varchar NULL,
	cr_action_date date NULL,
	cr_id int4 NULL,
	cr_change_id int4 NULL
);



ALTER TABLE masterdata_sg.unit_group RENAME COLUMN project_dwid TO unit_group_parent_dwid;

