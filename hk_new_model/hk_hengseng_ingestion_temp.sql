'''
-- ingest directly use raw_hongkong.hk_hangseng_floor_flat -- reference: hk centanet ingestion temp
[done]1.'Regal Court', Wan chai, single building -- hkpost does not have this project / building in this district
[done]2.'1 Lion Rock Road'
[done]3.'Yuet Wah Court'
[done]4.'Noble Park'
[done]5.'Yee Yuen'
'''

select *
from raw_hongkong.hk_hangseng_details hhd 
limit 500;

with base as (
select 
	area,district,estate,block,floor,flat,property_address,
	case when trim(replace(split_part(property_address, chr(10),3),',','')) ~ '^[0-9]' then trim(replace(split_part(property_address, chr(10),3),',','')) end as address_street,
	gross_floor_area_sqft,saleable_floor_area_sqft,date_of_completion,valuation_hkd,date_valued
from raw_hongkong.hk_hangseng_details hhd 
)
select address_street, count(*)
from base
group by 1;


with hangseng_info_base as (
select 
	area,district,estate,block,floor,flat,property_address,
	case when trim(replace(split_part(property_address, chr(10),3),',','')) ~ '^[0-9]' then trim(replace(split_part(property_address, chr(10),3),',','')) end as address_street,
	gross_floor_area_sqft,saleable_floor_area_sqft,date_of_completion,valuation_hkd,date_valued
from raw_hongkong.hk_hangseng_details hhd 
)
select 
	area,district,estate,block,floor,flat,
	trim(split_part(address_street, ' ', 1)) as address_num,
	trim(split_part(address_street, trim(split_part(address_street, ' ', 1)), 2)) as street_name,
	gross_floor_area_sqft,
	saleable_floor_area_sqft as net_floor_area_sqft,
	trim(split_part(date_of_completion, '/',2)) as completion_year,
	valuation_hkd,
	to_date(date_valued, 'dd/mm/yyyy') as date_valued
from hangseng_info_base
; -- none of those 4 estate have necessary info from this table



select a.*, b.*
from premap_hk.hangseng_block_to_dwid a 
left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
where a.project_dwid isnull and a.building_dwid isnull; -- 215

-- use midland to fill in additional info: address, area size, bedroom, lat/long, completion year


select distinct 
	a.district_name , a.estate_name , a.block_name , b."floorCode" , b."flatCode" , c.address_number , c.address_street , d.location_lat , d.location_lon , d.completion_date , d.hos , e.area , e.net_area , e.bedroom
from premap_hk.hangseng_block_to_dwid a 
left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
left join premap_hk.midland_building_to_dwid c on lower(a.estate_name) = lower(c.estate_name) and trim(split_part(a.block_name, ' ', 2)) =  trim(split_part(c.building_name , ' ', 2))
left join reference.hk_midland_estate_extend d on c.estate_id = d.estate_id 
left join reference.hk_midland_backfill_w_bedroom e on c.building_id = e.building_id and b."floorCode" = e.floor and b."flatCode" = e.flat 
where a.estate_name = 'Yee Yuen'; -- 'Noble Park'; --56



select metadata.fn_create_change_request(
    'hk-add-missing-address-records-for-project-Yee Yuen-2023-01-05', 'huying','huying' -- 'pipeline', 'pipeline'
); --2321
call metadata.sp_add_change_table(2321::int, 'hk', replace('address', '-', '_'));


insert into branch_hk.address_cr_2321
(
	address_type_code,full_address_text,address_display_text,address_building,address_number,address_street_text,street_name,street_name_root,development,development_phase,
	city_subarea,city_subarea_id,city_subarea_code,city_area,city_area_id,city_area_code,city,city_id,city_code,metro_area,metro_area_code,country,country_code,country_3code,geographic_zone,continent,
	latitude, longitude, location_marker, status_code,language_code,data_source_count,hash_key,dw_address_id,data_source,cr_record_action
)
with hangseng_base as (
select distinct 
	split_part(a.district_name, '/', 1) as district_name, a.estate_name , replace(a.block_name, '/Tower', '') as block_name, c.address_number , c.address_street , d.location_lat , d.location_lon , d.completion_date , d.hos
from premap_hk.hangseng_block_to_dwid a 
--left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
left join premap_hk.midland_building_to_dwid c on lower(a.estate_name) = lower(c.estate_name) and trim(split_part(a.block_name, ' ', 2)) =  trim(split_part(c.building_name , ' ', 2))
left join reference.hk_midland_estate_extend d on c.estate_id = d.estate_id 
--left join reference.hk_midland_backfill_w_bedroom e on c.building_id = e.building_id and b."floorCode" = e.floor and b."flatCode" = e.flat 
where a.estate_name = 'Yee Yuen' --'Noble Park'
)
select 
	'point-address' as address_type_code,
	lower(trim(trim(trim(coalesce(address_number, '') || ' ' || coalesce(address_street, '') || ',' || coalesce(block_name, '') || ',' ||--coalesce(phase_name, '') || 
		',' || coalesce(estate_name, '') || ',' || coalesce(a.district_name, '') || ',' || coalesce(c.city_area, '')
		|| ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ',')), ',')) as full_address_text,
	replace(replace(initcap(replace(trim(replace(replace(replace(replace(trim(trim(coalesce(estate_name, '') || ',' || --coalesce(phase_name, '') || 
        ',' || coalesce(block_name, '') || ',' || coalesce(address_number, '') || ' ' || coalesce(address_street, '') || ',' || coalesce(a.district_name, '') || ',' ||
        coalesce(c.city, ''), ',')), ',,,,', ','), ',,,', ','), ',,', ','), ', ,', ','), ','), ',', ', ')), '  ', ' '), '''S', '''s') as address_display_text,
    lower(block_name) as address_building,
	address_number as address_number,
	nullif(lower(trim(coalesce(address_number,'') || ' ' || coalesce(address_street,''))), ' ') as address_street_text,
	lower(address_street) as street_name,
	lower(address_street) as street_name_root,
	lower(estate_name) as development,
	null as development_phase,
	lower(a.district_name) as city_subarea,
	c.city_subarea_id,
	lower(c.city_subarea_code) as city_subarea_code,
	lower(c.city_area) as city_area,
	c.city_area_id,
	lower(c.city_area_code) as city_area_code,
	lower(c.city) as city,
    c.city_id,
    lower(c.city_code) as city_code,
    'hong kong (sar)' as metro_area,
	'hk' as metro_area_code,
	'china' as country,
	'cn' as country_code,
	'chn' as country_3code,
	'south-east asia' as geographic_zone,
	'asia' as continent,
	a.location_lat as latitude,
	a.location_lon as longitude,
	st_astext(st_makepoint(a.location_lon, a.location_lat)) as location_marker,
 	'active' as status_code,
 	'eng' as language_code,
 	'1'::int as data_source_count,
 	md5(trim(trim(lower(replace(replace(replace(replace(regexp_replace('cn/hk/' || coalesce(c.city_code, '') || coalesce(c.city_area_code, '') || coalesce(c.city_subarea_code, '') || '/' ||
 		coalesce(estate_name, '') || '/' || --coalesce(phase_name, '') || 
 		'/' || coalesce(block_name, '') || '/' || coalesce(trim(coalesce(address_number, '') || ' ' || coalesce(address_street, '')),'')
 	    , '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '/'))) as hash_key,
 	null as dw_address_id,
 	'hk-hangseng' as data_source,
 	'insert' as cr_record_action
from hangseng_base a 
left join reference.ref_hk_address_hierarchy_subarea c on lower(a.district_name) = lower(c.corrected_city_subarea);


insert into branch_hk.address_cr_2321
(
	address_type_code,full_address_text,address_display_text,address_street_text,street_name,street_name_root,development,
	city_subarea,city_subarea_id,city_subarea_code,city_area,city_area_id,city_area_code,city,city_id,city_code,metro_area,metro_area_code,country,country_code,country_3code,geographic_zone,continent,
	latitude, longitude, location_marker, status_code,language_code,data_source_count,hash_key,dw_address_id,data_source,cr_record_action
)
with hangseng_base as (
select distinct 
	split_part(a.district_name, '/', 1) as district_name, a.estate_name , replace(a.block_name, '/Tower', '') as block_name, c.address_number , c.address_street , d.location_lat , d.location_lon , d.completion_date , d.hos
from premap_hk.hangseng_block_to_dwid a 
--left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
left join premap_hk.midland_building_to_dwid c on lower(a.estate_name) = lower(c.estate_name) and trim(split_part(a.block_name, ' ', 2)) =  trim(split_part(c.building_name , ' ', 2))
left join reference.hk_midland_estate_extend d on c.estate_id = d.estate_id 
--left join reference.hk_midland_backfill_w_bedroom e on c.building_id = e.building_id and b."floorCode" = e.floor and b."flatCode" = e.flat 
where a.estate_name = 'Yee Yuen' --'Noble Park'
)
select distinct 
	'project-address' as address_type_code,
	lower(trim(trim(coalesce(address_street, '') || ',,,' || coalesce(estate_name, '') || ',' || coalesce(a.district_name, '') || ',' ||
		coalesce(c.city_area, '') || ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ','))) as full_address_text,
	replace(initcap(replace(trim(trim(replace(replace(coalesce(estate_name, '') || ',' ||coalesce(address_street, '') || ',' || coalesce(a.district_name, '')
        || ',' || coalesce(c.city, ''), ',,,', ','), ',,', ','), ',')), ',', ', ')), '''S', '''s') as address_display_text,
	lower(address_street) as address_street_text,
	lower(address_street) as street_name,
	lower(address_street) as street_name_root,
	lower(estate_name) as development,
	lower(a.district_name) as city_subarea,
	c.city_subarea_id,
	lower(c.city_subarea_code) as city_subarea_code,
	lower(c.city_area) as city_area,
	c.city_area_id,
	lower(c.city_area_code) as city_area_code,
	lower(c.city) as city,
    c.city_id,
    lower(c.city_code) as city_code,
    'hong kong (sar)' as metro_area,
	'hk' as metro_area_code,
	'china' as country,
	'cn' as country_code,
	'chn' as country_3code,
	'south-east asia' as geographic_zone,
	'asia' as continent,
	a.location_lat as latitude,
	a.location_lon as longitude,
	st_astext(st_makepoint(a.location_lon, a.location_lat)) as location_marker,
 	'active' as status_code,
 	'eng' as language_code,
 	'1'::int as data_source_count,
 	md5(trim(trim(lower(replace(replace(replace(replace(regexp_replace('cn/hk/' || coalesce(c.city_code, '') || coalesce(c.city_area_code, '') || coalesce(c.city_subarea_code, '') || '/' ||
 		coalesce(estate_name, '') || '///' || coalesce(address_street, ''), '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '/'))) as hash_key,
 	null as dw_address_id,
 	'hk-hangseng' as data_source,
 	'insert' as cr_record_action
from hangseng_base a 
left join reference.ref_hk_address_hierarchy_subarea c on lower(a.district_name) = lower(c.corrected_city_subarea);

-- manually fix some gaps in change request table


update branch_hk.address_cr_2321
set slug = trim(trim(lower(replace(replace(replace(replace(regexp_replace('poi/hk/address/' || coalesce(address_building, '') || '-' ||
	coalesce(trim(coalesce(address_number, '') || ' ' || coalesce(street_name, '')), '') || '-' || id, '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '-'))
where address_type_code = 'point-address' and slug isnull
;


update branch_hk.address_cr_2321
set slug = trim(trim(lower(replace(replace(replace(replace(regexp_replace('poi/hk/project/' || coalesce(development, '') || '-' ||
        id, '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '-'))
where address_type_code = 'project-address' and slug isnull
;

update branch_hk.address_cr_2321
set address_dwid = md5(country_code || '__' || 'address' || '__' || id)
where address_dwid isnull
;

call metadata.sp_submit_change_request(2321, 'huying');
call metadata.sp_approve_change_request(2321, 'huying');
call metadata.sp_merge_change_request(2321);



select metadata.fn_create_change_request(
    'hk-add-missing-project-records-for-project-Yee Yuen-2023-01-05', 'huying','huying' -- 'pipeline', 'pipeline'
); --2322
call metadata.sp_add_change_table(2322::int, 'hk', replace('project', '-', '_'));



insert into branch_hk.project_cr_2322
(
	address_dwid,project_type_code,project_display_name,project_name_text,completion_year, tenure_code, unit_count,residential_unit_count,commercial_unit_count,
	address_display_text,location_display_text,location_marker, country_code,is_active,data_source,cr_record_action
)
with hangseng_base as (
select distinct 
	a.district_name , a.estate_name , a.block_name , b."floorCode" , b."flatCode" , c.address_number , c.address_street , d.location_lat , d.location_lon , d.completion_date , d.hos , e.area , e.net_area , e.bedroom
from premap_hk.hangseng_block_to_dwid a 
left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
left join premap_hk.midland_building_to_dwid c on lower(a.estate_name) = lower(c.estate_name) and trim(split_part(a.block_name, ' ', 2)) =  trim(split_part(c.building_name , ' ', 2))
left join reference.hk_midland_estate_extend d on c.estate_id = d.estate_id 
left join reference.hk_midland_backfill_w_bedroom e on c.building_id = e.building_id and b."floorCode" = e.floor and b."flatCode" = e.flat 
where a.estate_name = 'Yee Yuen' --'Noble Park'
)
, unit_count_base as (
	select estate_name , count(*) as unit_count
	from hangseng_base
	group by 1
)
select distinct 
	ad.address_dwid ,
	'condo' as project_type_code,
	initcap(a.estate_name) as project_display_name,
    lower(a.estate_name) as project_name_text,
    EXTRACT('year' FROM a.completion_date::date)::int as completion_year,
    'leasehold' as tenure_code,
    b.unit_count,
    b.unit_count as residential_unit_count,
    null::int as commercial_unit_count,
    ad.address_display_text,
    trim(initcap(reverse(split_part(reverse(ad.full_address_text), ',', 4)||' ,'||
		    split_part(reverse(ad.full_address_text), ',', 3)||' ,'||
		    split_part(reverse(ad.full_address_text), ',', 2)))) as location_display_text,
	ad.location_marker,
	'cn' as country_code,
    true::boolean as is_active,
    'hk-hangseng' as data_source,
    'insert' as cr_record_action
from hangseng_base a 
left join masterdata_hk.address ad on lower(a.estate_name) = lower(ad.development) and ad.address_type_code = 'project-address'
left join unit_count_base b on lower(a.estate_name) = lower(b.estate_name)
where ad.data_source = 'hk-hangseng';

-- manually fix some gaps in change request table

update branch_hk.project_cr_2322
set project_name = api.get_normalized_name(project_name_text)
where project_name isnull
;

update branch_hk.project_cr_2322
set project_dwid = md5(country_code || '__' || 'project' || '__' || id)
where project_dwid isnull
;

update branch_hk.project_cr_2322
set slug = api.clean_slug('project/hk/' || coalesce(project_type_code, '') || '/'
            || coalesce(project_name, '') || '-' || id)
where slug isnull
;

call metadata.sp_submit_change_request(2322, 'huying');
call metadata.sp_approve_change_request(2322, 'huying');
call metadata.sp_merge_change_request(2322);




select metadata.fn_create_change_request(
    'hk-add-missing-building-records-for-project-Yee Yuen-2023-01-05', 'huying','huying' -- 'pipeline', 'pipeline'
); --2323
call metadata.sp_add_change_table(2323::int, 'hk', replace('building', '-', '_'));


insert into branch_hk.building_cr_2323
(
	address_dwid,project_dwid,building_type_code, building_display_name,building_name_text,construction_end_year, unit_count,residential_unit_count,commercial_unit_count,address_display_text,location_marker, country_code,is_active,data_source,cr_record_action
)
with hangseng_base as (
select distinct 
	split_part(a.district_name, '/', 1) as district_name, a.estate_name , replace(a.block_name, '/Tower', '') as block_name, b."floorCode" , b."flatCode" ,
	c.address_number , c.address_street , d.location_lat , d.location_lon , d.completion_date , d.hos , e.area , e.net_area , e.bedroom
from premap_hk.hangseng_block_to_dwid a 
left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
left join premap_hk.midland_building_to_dwid c on lower(a.estate_name) = lower(c.estate_name) and trim(split_part(a.block_name, ' ', 2)) =  trim(split_part(c.building_name , ' ', 2))
left join reference.hk_midland_estate_extend d on c.estate_id = d.estate_id 
left join reference.hk_midland_backfill_w_bedroom e on c.building_id = e.building_id and b."floorCode" = e.floor and b."flatCode" = e.flat 
where a.estate_name = 'Yee Yuen' --'Noble Park'
)
, unit_count_base as (
	select estate_name , block_name , count(*) as unit_count
	from hangseng_base
	group by 1, 2
)
select distinct 
	ad.address_dwid , 
	pj.project_dwid , 
	'multi-story-building' as building_type_code,
	initcap(a.block_name) as building_display_name,
	lower(a.block_name) as building_name_text,
	pj.completion_year as construction_end_year,
	b.unit_count,
	b.unit_count as residential_unit_count,
	null::int as commercial_unit_count,
	lower(ad.address_display_text) as address_display_text,
	ad.location_marker ,
	'cn' as country_code,
	true::boolean as is_active,
	'hk-hangseng' as data_source,
	'insert' as cr_record_action
from hangseng_base a 
left join masterdata_hk.address ad on lower(a.estate_name) = lower(ad.development) and lower(a.block_name) = lower(ad.address_building) and ad.address_type_code = 'point-address'
left join unit_count_base b on lower(a.block_name) = lower(b.block_name)
left join masterdata_hk.project pj on lower(a.estate_name) = lower(pj.project_name_text)
where ad.data_source = 'hk-hangseng' and pj.data_source = 'hk-hangseng'; 

-- manually fix some gaps in change request table

update branch_hk.building_cr_2323
set building_name = api.get_normalized_name(building_name_text)
where building_name isnull
;

update branch_hk.building_cr_2323
set building_dwid = md5(country_code || '__' || 'building' || '__' || id)
where building_dwid isnull
;

update branch_hk.building_cr_2323 a
set slug = api.clean_slug(b.slug || '/' || coalesce(a.building_name, '') || '-' || a.id)
from masterdata_hk.project b
where a.project_dwid = b.project_dwid
and a.slug isnull
;

call metadata.sp_submit_change_request(2323, 'huying');
call metadata.sp_approve_change_request(2323, 'huying');
call metadata.sp_merge_change_request(2323);



select metadata.fn_create_change_request(
    'hk-add-missing-property-records-for-project-Yee Yuen-2023-01-05', 'huying','huying' -- 'pipeline', 'pipeline'
); --2324
call metadata.sp_add_change_table(2325::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_2325
(
	address_dwid,building_dwid,project_dwid,property_type_code,address_unit,address_floor_text,address_floor_num,address_stack,address_stack_num,
	bedroom_count, net_floor_area_sqm, gross_floor_area_sqm, country_code,is_active,data_source,cr_record_action
)
with hangseng_base as (
select distinct 
	split_part(a.district_name, '/', 1) as district_name, a.estate_name , replace(a.block_name, '/Tower', '') as block_name, b."floorCode" as floor, b."flatCode" as unit,
	c.address_number , c.address_street , d.location_lat , d.location_lon , d.completion_date , d.hos , e.area , e.net_area , e.bedroom
from premap_hk.hangseng_block_to_dwid a 
left join raw_hongkong.hk_hangseng_floor_flat b on a.block_code = b."blockCode" 
left join premap_hk.midland_building_to_dwid c on lower(a.estate_name) = lower(c.estate_name) and trim(split_part(a.block_name, ' ', 2)) =  trim(split_part(c.building_name , ' ', 2))
left join reference.hk_midland_estate_extend d on c.estate_id = d.estate_id 
left join reference.hk_midland_backfill_w_bedroom e on c.building_id = e.building_id and b."floorCode" = e.floor and b."flatCode" = e.flat 
where a.estate_name = 'Yee Yuen' --'Noble Park'
)
, stack_number as (
select estate_name, block_name, unit , row_number() over(partition by estate_name, block_name order by unit) as seq
from hangseng_base
group by 1,2,3
)
select distinct 
	ad.address_dwid,
	bd.building_dwid,
	pj.project_dwid,
	'condo' as property_type_code,
	upper(a.floor || '-' || a.unit) as address_unit,
	upper(a.floor) as address_floor_text,
	case when a.floor in ('G', 'P', 'PODIUM') then 0::int else a.floor::int end as address_floor_num,
	upper(a.unit) as address_stack,
	b.seq::numeric as address_stack_num,
	a.bedroom::int as bedroom_count,
	nullif(a.net_area::int, 0) as net_floor_area_sqm,
	nullif(a.area::int, 0) as gross_floor_area_sqm,
	'cn' as country_code,
	true::boolean as is_active,
	'hk-hangseng' as data_source,
	'insert' as cr_record_action
from hangseng_base a 
left join masterdata_hk.address ad on lower(a.estate_name) = lower(ad.development) and lower(a.block_name) = lower(ad.address_building) and ad.address_type_code = 'point-address'
left join masterdata_hk.project pj on lower(a.estate_name) = lower(pj.project_name_text)
left join masterdata_hk.building bd on pj.project_dwid = bd.project_dwid and lower(a.block_name) = lower(bd.building_display_name)
left join stack_number b on a.estate_name = b.estate_name and a.block_name = b.block_name and a.unit = b.unit and seq notnull
where ad.data_source = 'hk-hangseng' and pj.data_source = 'hk-hangseng' and bd.data_source = 'hk-hangseng'; 


update branch_hk.property_cr_2325
set property_dwid = md5(country_code || '__' || 'property' || '__' || id)
where property_dwid isnull
;

with base as (
    select
        a.id,
        case when a.project_dwid notnull then api.clean_slug(b.slug || '/' || coalesce(c.building_name, '') || '/'
            || coalesce(a.address_floor_text, '') || '/' || coalesce(a.address_stack, '') || '-' || a.id)
            else api.clean_slug('property/hk/' || coalesce(a.property_type_code, '') || '/' || coalesce(c.building_name, '') || '/'
            || coalesce(a.address_unit, '') || '-' || a.id)
            end as slug
    from branch_hk.property_cr_2325 a
        left join masterdata_hk.project b
            on a.project_dwid = b.project_dwid
        left join masterdata_hk.building c
            on a.building_dwid = c.building_dwid
    where a.slug isnull
)
update branch_hk.property_cr_2325 a
set slug = b.slug
from base b
where a.id = b.id
;

call metadata.sp_submit_change_request(2325, 'huying');
call metadata.sp_approve_change_request(2325, 'huying');
call metadata.sp_merge_change_request(2325);


-- update project_dwid and building_dwid in premap_hk.hangseng_block_to_dwid
















