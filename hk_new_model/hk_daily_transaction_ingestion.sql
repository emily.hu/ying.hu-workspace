select a.*
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
where a.mem_no isnull and b.id notnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 0

select a.*
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
where a.mem_no notnull and b.id isnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 50898

select a.*
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
where a.mem_no notnull and b.id notnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 77003


select distinct 
	a.district_code , 
	a.estate_name , 
	trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')) as phase_name,
	a.building_name , 
	case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end as address_number,
	a.address_street1 as address_street,
	c.address_dwid ,c.building_dwid , c.project_dwid , c.project_phase_dwid 
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
left join premap_hk.centanet_building_to_dwid c on b.building_code = c.building_id 
where a.mem_no notnull and b.id notnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 11758



CREATE TABLE premap_hk.daily_txn_building_to_dwid (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	district_code text NULL,
	estate_name text NULL,
	phase_name text NULL,
	building_name text NULL,
	address_number text NULL,
	address_street text NULL,
	building_dwid text NULL,
	address_dwid text NULL,
	project_dwid text NULL,
	lot_group_dwid text NULL,
	project_phase_dwid text NULL
);

-- drop table premap_hk.daily_txn_building_to_dwid;

ALTER TABLE premap_hk.daily_txn_building_to_dwid ADD CONSTRAINT daily_txn_building_to_dwid_un_key UNIQUE (district_code,estate_name,phase_name,building_name,address_number,address_street);


insert into premap_hk.daily_txn_building_to_dwid
(
district_code,estate_name,phase_name,building_name,address_number,address_street,address_dwid,building_dwid,project_dwid,project_phase_dwid
)
select distinct 
	a.district_code , 
	a.estate_name , 
	trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')) as phase_name,
	coalesce(a.block, a.building_name) as building_name, 
	case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end as address_number,
	a.address_street1 as address_street,
	c.address_dwid ,c.building_dwid , c.project_dwid , c.project_phase_dwid--, c.id  
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
left join premap_hk.centanet_building_to_dwid c on b.building_code = c.building_id 
where a.mem_no notnull and b.id notnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0
on conflict do nothing; -- 12280


select 
	district_code,estate_name,phase_name,building_name,address_number,address_street,
	count(*)
from premap_hk.daily_txn_building_to_dwid
group by 1,2,3,4,5,6 having count(*) > 1; --89

'''
select 
	d.*, e.address_number ,
	row_number() over (partition by district_code,estate_name,phase_name,building_name,d.address_number,d.address_street
		order by d.address_number = e.address_number ) as seq
from premap_hk.daily_txn_building_to_dwid d
left join premap_hk.centanet_building_to_dwid e on d.id = e.id
'''

with dups as (
select 
	district_code,estate_name,phase_name,building_name,address_number,address_street,
	count(*)
from premap_hk.daily_txn_building_to_dwid
group by 1,2,3,4,5,6 having count(*) > 1 --89
)
, dedup_id as (
select a.id -- a.*
from premap_hk.daily_txn_building_to_dwid a
left join dups b 
on f_prep_dw_id(a.district_code) = f_prep_dw_id(b.district_code)
and f_prep_dw_id(a.estate_name) = f_prep_dw_id(b.estate_name)
and f_prep_dw_id(a.phase_name) = f_prep_dw_id(b.phase_name)
and f_prep_dw_id(a.building_name) = f_prep_dw_id(b.building_name)
and f_prep_dw_id(a.address_number) = f_prep_dw_id(b.address_number)
and f_prep_dw_id(a.address_street) = f_prep_dw_id(b.address_street)
where b.district_code notnull -- 282
)
delete from premap_hk.daily_txn_building_to_dwid where id in (select id from dedup_id); 



update premap_hk.daily_txn_building_to_dwid
set phase_name = NULL
where phase_name = ''; -- 9907


select distinct 
	a.district_code , 
	a.estate_name , 
	nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '') as phase_name,
	coalesce(a.block, a.building_name) as building_name, 
	case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end as address_number,
	a.address_street1 as address_street,
	a.floor , a.unit ,
	c.property_dwid , c.address_dwid ,c.building_dwid , c.project_dwid , c.project_phase_dwid 
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
left join premap_hk.centanet_unit_to_dwid c on b.cuntcode = c.unit_id  
where a.mem_no notnull and b.id notnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 76768



CREATE TABLE premap_hk.daily_txn_unit_to_dwid (
	id int4 NOT NULL DEFAULT nextval('metadata.id_seq'::regclass),
	district_code text NULL,
	estate_name text NULL,
	phase_name text NULL,
	building_name text NULL,
	address_number text NULL,
	address_street text NULL,
	floor text NULL,
	unit text NULL,
	property_dwid text NULL,
	building_dwid text NULL,
	address_dwid text NULL,
	project_dwid text NULL,
	lot_group_dwid text NULL,
	project_phase_dwid text NULL
);


ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_un_key 
UNIQUE (district_code,estate_name,phase_name,building_name,address_number,address_street,floor,unit);


insert into premap_hk.daily_txn_unit_to_dwid
(
district_code,estate_name,phase_name,building_name,address_number,address_street,floor,unit,property_dwid,address_dwid,building_dwid,project_dwid,project_phase_dwid
)
select distinct 
	a.district_code , 
	a.estate_name , 
	nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '') as phase_name,
	coalesce(a.block, a.building_name) as building_name, 
	case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end as address_number,
	a.address_street1 as address_street,
	a.floor , a.unit ,
	c.property_dwid , c.address_dwid ,c.building_dwid , c.project_dwid , c.project_phase_dwid 
from "source".hk_daily_transaction a
left join "source".hk_centanet_sale_transaction b on a.mem_no::text = b.id::text
left join premap_hk.centanet_unit_to_dwid c on b.cuntcode = c.unit_id
where a.mem_no notnull and b.id notnull --and c.id isnull
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 76768


select 
    count(*) as total_records,
    --count(status_code) as status_count,
    count(project_dwid) project_count,
    (count(project_dwid)*100/count(*)) as projects_mapped,
    count(building_dwid),
    (count(building_dwid)*100/count(*)) as buildings_mapped,
    count(address_dwid),
    (count(address_dwid)*100/count(*)) as addresses_mapped,
    count(project_phase_dwid),
    (count(project_phase_dwid)*100/count(*)) as phases_mapped
from premap_hk.daily_txn_building_to_dwid
;
-- 11998	(project)6201	51	(building)10337	 86	(address)10391	86	(phase)986	8



select 
    count(*) as total_records,
    --count(status_code) as status_count,
    count(property_dwid) ,
    (count(property_dwid)*100/count(*)) as properties_mapped,
    count(project_dwid) ,
    (count(project_dwid)*100/count(*)) as projects_mapped,
    count(building_dwid),
    (count(building_dwid)*100/count(*)) as buildings_mapped,
    count(address_dwid),
    (count(address_dwid)*100/count(*)) as addresses_mapped,
    count(project_phase_dwid),
    (count(project_phase_dwid)*100/count(*)) as phases_mapped
from premap_hk.daily_txn_unit_to_dwid
;
-- 76768  (property)50908   66	(project)44525	57	(building)55042	 71	(address)56056	73	(phase)6098	 7



CREATE TABLE map_hk.daily_txn_sale_txn__map (
	data_uuid uuid NOT NULL,
	activity_dwid text NULL,
	property_dwid text NULL,
	building_dwid text NULL,
	address_dwid text NULL,
	project_dwid text NULL,
	land_parcel_dwid text NULL,
	lot_group_dwid text NULL,
	status_code varchar NULL,
	project_phase_dwid text null
);


insert into map_hk.daily_txn_sale_txn__map 
(data_uuid,property_dwid,building_dwid,address_dwid,project_dwid,project_phase_dwid)
select distinct a.data_uuid::uuid, b.property_dwid , b.building_dwid , b.address_dwid , b.project_dwid , b.project_phase_dwid 
from source.hk_daily_transaction a
left join premap_hk.daily_txn_unit_to_dwid b 
on f_prep_dw_id(a.district_code) = f_prep_dw_id(b.district_code)
and f_prep_dw_id(a.estate_name) = f_prep_dw_id(b.estate_name)
and f_prep_dw_id(nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '')) = f_prep_dw_id(b.phase_name)
and f_prep_dw_id(coalesce(a.block, a.building_name)) = f_prep_dw_id(b.building_name)
and f_prep_dw_id(case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end) = f_prep_dw_id(b.address_number)
and f_prep_dw_id(a.address_street1) = f_prep_dw_id(b.address_street)
and f_prep_dw_id(a.floor) = f_prep_dw_id(b.floor)
and f_prep_dw_id(a.unit) = f_prep_dw_id(b.unit)
where a.data_uuid notnull; -- 410626


-- fill in activity_dwid
'''
select distinct a.data_uuid, d.activity_dwid 
from map_hk.daily_txn_sale_txn__map a
left join "source".hk_daily_transaction b on a.data_uuid = b.data_uuid::uuid 
left join "source".hk_centanet_sale_transaction c on b.mem_no::text = c.id::text
left join map_hk.centanet_sale_txn__map d on c.data_uuid::uuid = d.data_uuid 
where d.activity_dwid notnull
'''

with base as (
select distinct a.data_uuid, d.activity_dwid 
from map_hk.daily_txn_sale_txn__map a
left join "source".hk_daily_transaction b on a.data_uuid = b.data_uuid::uuid 
left join "source".hk_midland_realty_sale_transaction c on right(c.id, -10) = b.mem_no::text 
left join map_hk.midland_sale_txn__map d on c.data_uuid = d.data_uuid  
where c.id ilike 'NO%' and d.activity_dwid notnull
)
update map_hk.daily_txn_sale_txn__map a
set activity_dwid = b.activity_dwid
from base b where a.data_uuid = b.data_uuid 
; -- 98018


select sum(case when activity_dwid notnull then 1 else 0 end)*1.0/count(*)
from map_hk.daily_txn_sale_txn__map; -- 0.23870383268473014373






drop table staging_hk.temp_new_sale_records_daily_txn;

CREATE TABLE staging_hk.temp_new_sale_records_daily_txn
(
    activity_dwid text NULL,
    address_dwid text NULL,
    project_dwid text NULL,
    project_phase_dwid text NULL,
    building_dwid text NULL,
    property_dwid text NULL,
    source_dwid text NULL,
    agent_dwid text NULL,

    data_uuid text NULL,
    data_source text NULL,
    data_source_id text NULL, -- midland.id, centanet.id, daily_txn.mem_no
    data_source_estate_id text NULL, -- midland.estate_id
    data_source_phase_id text NULL, -- midland.phase_id
    data_source_building_id text NULL, -- midland.building_id, centanet.building_code
    data_source_unit_id text NULL, -- midland.unit_id, centanet.cuntcode
    original_data_source text NULL, -- midland.original_source, centanet.data_source

    source_transaction_date date NULL, -- midland.tx_date, cent.ins_date, daily_txn.instrument_date, data_gov.transaction_date
	occupation_date date NULL, -- midland.building_first_op_date, cent.op_date, daily_txn.occupation_date
    instrument_date date NULL, -- cent.ins_date, daily_txn.instrument_date
    update_date date NULL, -- midland
    reg_date date NULL, -- cent
    input_date date NULL, -- daily_txn
    delivery_date date NULL, -- daily_txn
    property_completion_year int null, -- midland.building_first_op_date, cent.building_age
    tenure text NULL,
    tenure_code text NULL,
    building_age int NULL,

    sale_type text NULL, -- midland.tx_type
    sale_subtype text NULL, -- midland.mkt_type
    source_property_type varchar NULL,
    property_subtype varchar NULL,
    source_property_model text NULL,

--     postal_code text NULL,
    region_name text NULL,
    district_name text NULL,
    subregion_name text NULL,
    source_project_name text NULL,
    source_phase_name text NULL,
    source_building_name text NULL,
    address_number text NULL,
    street_name text NULL,
    lot_no text NULL,
    address_unit text NULL,
    address_floor_text text NULL,
    floor_level_id text NULL,
    address_stack text NULL,
    floor_num_min int4 NULL,
    floor_num_max int4 NULL,
    latitude numeric NULL,
    longitude numeric NULL,

    bedroom_count int4 NULL,
    source_bedroom_count int4 NULL,
    bathroom_count int4 NULL,
    sitting_room int4 NULL,

    source_net_floor_area_sqm numeric NULL,
    source_gross_floor_area_sqm numeric NULL,
    source_land_area_sqm numeric NULL,
    source_area_sqft numeric NULL,
--     source_gross_floor_area_sqm_min numeric NULL,
--     source_gross_floor_area_sqm_max numeric NULL,
    gross_floor_area_sqm numeric NULL,
    net_floor_area_sqm numeric NULL,
    land_area_sqm numeric NULL,

    country_code text NULL,
--     source_transaction_value numeric NULL,
    purchase_amount numeric NULL, -- midland.price, cent.transaction_price, daily_txn.consideration
    net_psf numeric NULL,
    gross_psf numeric NULL,
    last_transaction_date date NULL, -- midland.last_tx_date
    last_purchase_amount numeric NULL, -- midland.last_price
    gain numeric NULL, -- midland

    premium_paid bool NULL,
    has_vr bool NULL,
    url text NULL, -- midland.url_desc, cent.url
    discount_rate text NULL,
    agency_self_negotiation text NULL,

    developers text NULL,

    creationtime timestamp NULL,
    lastmodifiedtime timestamp NULL,
    lastseentime timestamp NULL,
    active bool NULL,
    status_code text NULL
)
;



insert into staging_hk.temp_new_sale_records_daily_txn
(
    source_dwid,
    data_uuid,
    data_source,
    data_source_id,
    source_transaction_date,
    occupation_date,
    instrument_date,
    input_date,
    delivery_date,
    tenure_code,
    district_name,
    source_project_name,
    source_phase_name,
    source_building_name,
    address_number,
    street_name,
    lot_no,
    address_floor_text,
    address_stack,
    source_gross_floor_area_sqm,
    source_net_floor_area_sqm,
    country_code,
    purchase_amount,
    net_psf,
    gross_psf,
    creationtime,
    lastmodifiedtime,
    lastseentime,
    active
)
select
    replace(r.data_uuid::text, '-', '') as source_dwid,
    r.data_uuid,
    'hk-gov-daily-transaction'::text as data_source,
    r.mem_no as data_source_id,
    r.instrument_date as source_transaction_date,
    r.occupation_date as occupation_date,
    r.instrument_date,
	r.input_date,
	r.delivery_date,
	'leasehold'::text as tenure_code,
	r.district_code as district_name,
	r.estate_name as source_project_name,
	nullif(trim(coalesce(r.phase_description, '') || ' ' || coalesce(r.phase_num, '')), '') as source_phase_name,
	coalesce(r.block, r.building_name) as source_building_name,
	case when r.address_num_to1 notnull then r.address_num_from1 || '-' || r.address_num_to1 
		else r.address_num_from1 end as address_number,
	r.address_street1 as street_name,
	r.lot_no1 as lot_no,
	r.floor as address_floor_text,
    r.unit as address_stack,
    nullif(f_sqft2sqm(r.gross_floor_area), 0) as source_gross_floor_area_sqm,
    nullif(f_sqft2sqm(r.salelable_floor_area), 0) as source_net_floor_area_sqm,
    'cn'::text as country_code,
    r.consideration * 1000000 as purchase_amount,
    r.net_psf,
    r.gross_psf,
    r.creationtime,
    r.lastmodifiedtime,
    r.lastseentime,
    r.active
from "source".hk_daily_transaction r
--where r.usage = 'RES' and r.instrument_nature = 'ASP' and r.consideration != 0 limit 100;
where not exists
    (
        select 1 from map_hk.daily_txn_sale_txn__map rm
        where rm.data_uuid::uuid = r.data_uuid::uuid
    )
and r.usage = 'RES' and r.instrument_nature = 'ASP' and r.consideration != 0;


-- integrate missing building and unit entity records

insert into premap_hk.daily_txn_building_to_dwid
(
district_code,estate_name,phase_name,building_name,address_number,address_street
)
select distinct 
	a.district_code , 
	a.estate_name , 
	nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '') as phase_name,
	coalesce(a.block, a.building_name) as building_name, 
	case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end as address_number,
	a.address_street1 as address_street
from "source".hk_daily_transaction a
	left join premap_hk.daily_txn_building_to_dwid tb
        on f_prep_dw_id(a.district_code) = f_prep_dw_id(tb.district_code)
        and f_prep_dw_id(a.estate_name) = f_prep_dw_id(tb.estate_name)
        and f_prep_dw_id(nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '')) = f_prep_dw_id(tb.phase_name)
        and f_prep_dw_id(coalesce(a.block, a.building_name)) = f_prep_dw_id(tb.building_name)
        and f_prep_dw_id(case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end) = f_prep_dw_id(tb.address_number)
        and f_prep_dw_id(a.address_street1) = f_prep_dw_id(tb.address_street)
where tb.id isnull 
and a.mem_no notnull 
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 6929


insert into premap_hk.daily_txn_unit_to_dwid
(
district_code,estate_name,phase_name,building_name,address_number,address_street,floor,unit
)
select distinct 
	a.district_code , 
	a.estate_name , 
	nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '') as phase_name,
	coalesce(a.block, a.building_name) as building_name, 
	case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end as address_number,
	a.address_street1 as address_street,
	a.floor , a.unit 
from "source".hk_daily_transaction a
	left join premap_hk.daily_txn_unit_to_dwid tb
        on f_prep_dw_id(a.district_code) = f_prep_dw_id(tb.district_code)
        and f_prep_dw_id(a.estate_name) = f_prep_dw_id(tb.estate_name)
        and f_prep_dw_id(nullif(trim(coalesce(a.phase_description, '') || ' ' || coalesce(a.phase_num, '')), '')) = f_prep_dw_id(tb.phase_name)
        and f_prep_dw_id(coalesce(a.block, a.building_name)) = f_prep_dw_id(tb.building_name)
        and f_prep_dw_id(case when a.address_num_to1 notnull then a.address_num_from1 || '-' || a.address_num_to1 
	else a.address_num_from1 end) = f_prep_dw_id(tb.address_number)
        and f_prep_dw_id(a.address_street1) = f_prep_dw_id(tb.address_street)
        and f_prep_dw_id(a.floor) = f_prep_dw_id(tb.floor)
        and f_prep_dw_id(a.unit) = f_prep_dw_id(tb.unit)
where tb.id isnull 
and a.mem_no notnull 
and a.usage = 'RES' and a.instrument_nature = 'ASP' and a.consideration != 0; -- 6929


with update_base as (
select 
	district_code , estate_name , phase_name , building_name , address_number , address_street , 
	count(distinct coalesce(address_dwid, '')) as id_cnt
from premap_hk.daily_txn_unit_to_dwid
group by 1,2,3,4,5,6 having count(distinct coalesce(address_dwid, '')) > 1
),
id_base as (
select distinct 
	district_code , estate_name , phase_name , building_name , address_number , address_street , 
	building_dwid ,address_dwid , project_dwid , project_phase_dwid 
from premap_hk.daily_txn_unit_to_dwid
where address_dwid notnull
)
, update_records as (
select b.id, ib.building_dwid ,ib.address_dwid , ib.project_dwid , ib.project_phase_dwid 
from premap_hk.daily_txn_unit_to_dwid b
left join update_base tb 
on f_prep_dw_id(b.district_code) = f_prep_dw_id(tb.district_code)
and f_prep_dw_id(b.estate_name) = f_prep_dw_id(tb.estate_name)
and f_prep_dw_id(b.phase_name) = f_prep_dw_id(tb.phase_name)
and f_prep_dw_id(b.building_name) = f_prep_dw_id(tb.building_name)
and f_prep_dw_id(b.address_number) = f_prep_dw_id(tb.address_number)
and f_prep_dw_id(b.address_street) = f_prep_dw_id(tb.address_street)
left join id_base ib 
on f_prep_dw_id(ib.district_code) = f_prep_dw_id(tb.district_code)
and f_prep_dw_id(ib.estate_name) = f_prep_dw_id(tb.estate_name)
and f_prep_dw_id(ib.phase_name) = f_prep_dw_id(tb.phase_name)
and f_prep_dw_id(ib.building_name) = f_prep_dw_id(tb.building_name)
and f_prep_dw_id(ib.address_number) = f_prep_dw_id(tb.address_number)
and f_prep_dw_id(ib.address_street) = f_prep_dw_id(tb.address_street)
where tb.id_cnt notnull and ib.address_dwid notnull and b.id notnull
 -- 84893
)
update premap_hk.daily_txn_unit_to_dwid ub
set building_dwid = ur.building_dwid, address_dwid = ur.address_dwid, project_dwid = ur.project_dwid, project_phase_dwid = ur.project_phase_dwid
from update_records ur where ub.id = ur.id; -- 81999



with base as (
	select distinct 
	   b.source_dwid,
	   tb.property_dwid,
	   tb.project_dwid,
	   tb.project_phase_dwid,
	   tb.building_dwid,
	   tb.address_dwid
	from staging_hk.temp_new_sale_records_daily_txn b
		left join premap_hk.daily_txn_unit_to_dwid tb
            on f_prep_dw_id(b.district_name) = f_prep_dw_id(tb.district_code)
            and f_prep_dw_id(b.source_project_name) = f_prep_dw_id(tb.estate_name)
            and f_prep_dw_id(b.source_phase_name) = f_prep_dw_id(tb.phase_name)
            and f_prep_dw_id(b.source_building_name) = f_prep_dw_id(tb.building_name)
            and f_prep_dw_id(b.address_number) = f_prep_dw_id(tb.address_number)
            and f_prep_dw_id(b.street_name) = f_prep_dw_id(tb.address_street)
            and f_prep_dw_id(b.address_floor_text) = f_prep_dw_id(tb.floor)
            and f_prep_dw_id(b.address_stack) = f_prep_dw_id(tb.unit)
        where (tb.property_dwid notnull or tb.address_dwid notnull) and b.data_source_id notnull -- 64
)
update staging_hk.temp_new_sale_records_daily_txn a
set address_dwid = b.address_dwid,
    building_dwid = b.building_dwid,
    project_dwid = b.project_dwid,
    project_phase_dwid = b.project_phase_dwid,
    property_dwid = b.property_dwid
from base b where a.source_dwid = b.source_dwid
and exists (select 1 from base b where a.source_dwid = b.source_dwid)
;














DROP TABLE IF EXISTS staging_hk.temp_new_sale_records_daily_txn;

CREATE TABLE staging_hk.temp_new_sale_records_daily_txn
(
    activity_dwid text NULL,
    address_dwid text NULL,
    project_dwid text NULL,
    project_phase_dwid text NULL,
    building_dwid text NULL,
    property_dwid text NULL,
    source_dwid text NULL,
    agent_dwid text NULL,

    data_uuid text NULL,
    data_source text NULL,
    data_source_id text NULL, -- midland.id, centanet.id, daily_txn.mem_no
    data_source_estate_id text NULL, -- midland.estate_id
    data_source_phase_id text NULL, -- midland.phase_id
    data_source_building_id text NULL, -- midland.building_id, centanet.building_code
    data_source_unit_id text NULL, -- midland.unit_id, centanet.cuntcode
    original_data_source text NULL, -- midland.original_source, centanet.data_source

    source_transaction_date date NULL, -- midland.tx_date, cent.ins_date, daily_txn.instrument_date, data_gov.transaction_date
	occupation_date date NULL, -- midland.building_first_op_date, cent.op_date, daily_txn.occupation_date
    instrument_date date NULL, -- cent.ins_date, daily_txn.instrument_date
    update_date date NULL, -- midland
    reg_date date NULL, -- cent
    input_date date NULL, -- daily_txn
    delivery_date date NULL, -- daily_txn
    property_completion_year int null, -- midland.building_first_op_date, cent.building_age
    tenure text NULL,
    tenure_code text NULL,
    building_age int NULL,

    sale_type text NULL, -- midland.tx_type
    sale_subtype text NULL, -- midland.mkt_type
    source_property_type varchar NULL,
    property_subtype varchar NULL,
    source_property_model text NULL,

--     postal_code text NULL,
    region_name text NULL,
    district_name text NULL,
    subregion_name text NULL,
    source_project_name text NULL,
    source_phase_name text NULL,
    source_building_name text NULL,
    address_number text NULL,
    street_name text NULL,
    lot_no text NULL,
    address_unit text NULL,
    address_floor_text text NULL,
    floor_level_id text NULL,
    address_stack text NULL,
    floor_num_min int4 NULL,
    floor_num_max int4 NULL,
    latitude numeric NULL,
    longitude numeric NULL,

    bedroom_count int4 NULL,
    source_bedroom_count int4 NULL,
    bathroom_count int4 NULL,
    sitting_room int4 NULL,

    source_net_floor_area_sqm numeric NULL,
    source_gross_floor_area_sqm numeric NULL,
    source_land_area_sqm numeric NULL,
    source_area_sqft numeric NULL,
--     source_gross_floor_area_sqm_min numeric NULL,
--     source_gross_floor_area_sqm_max numeric NULL,
    gross_floor_area_sqm numeric NULL,
    net_floor_area_sqm numeric NULL,
    land_area_sqm numeric NULL,

    country_code text NULL,
--     source_transaction_value numeric NULL,
    purchase_amount numeric NULL, -- midland.price, cent.transaction_price, daily_txn.consideration
    net_psf numeric NULL,
    gross_psf numeric NULL,
    last_transaction_date date NULL, -- midland.last_tx_date
    last_purchase_amount numeric NULL, -- midland.last_price
    gain numeric NULL, -- midland

    premium_paid bool NULL,
    has_vr bool NULL,
    url text NULL, -- midland.url_desc, cent.url
    discount_rate text NULL,
    agency_self_negotiation text NULL,

    developers text NULL,

    creationtime timestamp NULL,
    lastmodifiedtime timestamp NULL,
    lastseentime timestamp NULL,
    active bool NULL,
    status_code text NULL
)
;


insert into staging_hk.temp_new_sale_records_daily_txn
(
    source_dwid,
    data_uuid,
    data_source,
    data_source_id,
    source_transaction_date,
    occupation_date,
    instrument_date,
    input_date,
    delivery_date,
    tenure_code,
    district_name,
    source_project_name,
    source_phase_name,
    source_building_name,
    address_number,
    street_name,
    lot_no,
    address_floor_text,
    address_stack,
    source_gross_floor_area_sqm,
    source_net_floor_area_sqm,
    country_code,
    purchase_amount,
    net_psf,
    gross_psf,
    creationtime,
    lastmodifiedtime,
    lastseentime,
    active
)
select
    replace(r.data_uuid::text, '-', '') as source_dwid,
    r.data_uuid,
    'hk-gov-daily-transaction'::text as data_source,
    r.mem_no as data_source_id,
    r.instrument_date as source_transaction_date,
    r.occupation_date as occupation_date,
    r.instrument_date,
	r.input_date,
	r.delivery_date,
	'leasehold'::text as tenure_code,
	r.district_code as district_name,
	r.estate_name as source_project_name,
	nullif(trim(coalesce(r.phase_description, '') || ' ' || coalesce(r.phase_num, '')), '') as source_phase_name,
	coalesce(r.block, r.building_name) as source_building_name,
	case when r.address_num_to1 notnull then r.address_num_from1 || '-' || r.address_num_to1
		else r.address_num_from1 end as address_number,
	r.address_street1 as street_name,
	r.lot_no1 as lot_no,
	r.floor as address_floor_text,
    r.unit as address_stack,
    nullif(f_sqft2sqm(r.gross_floor_area), 0) as source_gross_floor_area_sqm,
    nullif(f_sqft2sqm(r.salelable_floor_area), 0) as source_net_floor_area_sqm,
    'cn'::text as country_code,
    r.consideration * 1000000 as purchase_amount,
    r.net_psf,
    r.gross_psf,
    r.creationtime,
    r.lastmodifiedtime,
    r.lastseentime,
    r.active
from "source".hk_daily_transaction r
where r.usage = 'RES' and r.instrument_nature = 'ASP' and r.consideration != 0
; -- 129191


create table staging_hk.temp_new_sale_records_daily_txn_dwids as
--with base as (
	select distinct
	   b.source_dwid,
	   tb.property_dwid,
	   tb.project_dwid,
	   tb.project_phase_dwid,
	   tb.building_dwid,
	   tb.address_dwid
	from staging_hk.temp_new_sale_records_daily_txn b
		left join premap_hk.daily_txn_unit_to_dwid tb
            on f_prep_dw_id(b.district_name) = f_prep_dw_id(tb.district_code)
            and f_prep_dw_id(b.source_project_name) = f_prep_dw_id(tb.estate_name)
            and f_prep_dw_id(b.source_phase_name) = f_prep_dw_id(tb.phase_name)
            and f_prep_dw_id(b.source_building_name) = f_prep_dw_id(tb.building_name)
            and f_prep_dw_id(b.address_number) = f_prep_dw_id(tb.address_number)
            and f_prep_dw_id(b.street_name) = f_prep_dw_id(tb.address_street)
            and f_prep_dw_id(b.address_floor_text) = f_prep_dw_id(tb.floor)
            and f_prep_dw_id(b.address_stack) = f_prep_dw_id(tb.unit)
    where (tb.property_dwid notnull or tb.address_dwid notnull) and b.data_source_id notnull;
)

update staging_hk.temp_new_sale_records_daily_txn a
set address_dwid = b.address_dwid,
    building_dwid = b.building_dwid,
    project_dwid = b.project_dwid,
    project_phase_dwid = b.project_phase_dwid,
    property_dwid = b.property_dwid
from staging_hk.temp_new_sale_records_daily_txn_dwids b --base b 
where a.source_dwid = b.source_dwid
and exists (
select 1 from staging_hk.temp_new_sale_records_daily_txn_dwids b --base b 
where a.source_dwid = b.source_dwid)
; -- 88834


--create table staging_hk.temp_new_sale_records_daily_txn_new_property_dwid as
with base as (
    select
        p.property_dwid as new_property_dwid,
        st.source_dwid
    from staging_hk.temp_new_sale_records_daily_txn st
        left join masterdata_hk.property p
            on f_prep_dw_id(st.project_dwid) = f_prep_dw_id(p.project_dwid)
--             and f_prep_dw_id(st.project_phase_dwid) = f_prep_dw_id(p.project_phase_dwid)
            and f_prep_dw_id(st.building_dwid) = f_prep_dw_id(p.building_dwid)
            and f_prep_dw_id(st.address_dwid) = f_prep_dw_id(p.address_dwid)
            and f_prep_dw_id(st.address_floor_text) = f_prep_dw_id(p.address_floor_text)
            and f_prep_dw_id(st.address_stack) = f_prep_dw_id(p.address_stack)
    where st.property_dwid isnull and p.property_dwid notnull
    group by 1, 2;
   
)

update staging_hk.temp_new_sale_records_daily_txn a
set property_dwid = b.new_property_dwid
from staging_hk.temp_new_sale_records_daily_txn_new_property_dwid b --base b 
where a.source_dwid = b.source_dwid
and exists (
select 1 from staging_hk.temp_new_sale_records_daily_txn_new_property_dwid b --base b 
where a.source_dwid = b.source_dwid)
; -- 32348




with base as (
    select
        s.source_dwid,
        case when p.property_type_code notnull then p.property_type_code
            when s.source_property_type notnull then s.source_property_type
            when p.property_type_code isnull and s.source_property_type isnull and s.source_bedroom_count notnull and ph.city notnull then 'hos'::text
			when p.property_type_code isnull and s.source_property_type isnull and s.source_bedroom_count notnull and ph.city isnull then 'condo'::text
			when p.property_type_code isnull and s.source_property_type isnull and s.source_bedroom_count isnull then 'comm'::text
			end as source_property_type
    from staging_hk.temp_new_sale_records_daily_txn s
		left join feature_hk.de__building__public_housing ph
	        on f_prep_dw_id(s.project_dwid) = f_prep_dw_id(ph.project_dwid)
	            and f_prep_dw_id(s.building_dwid) = f_prep_dw_id(ph.building_dwid)
	            and s.building_dwid || s.project_dwid notnull
	    left join masterdata_hk.property p
	        on s.property_dwid = p.property_dwid
    group by 1, 2
)
update staging_hk.temp_new_sale_records_daily_txn a
set source_property_type = b.source_property_type
from base b where a.source_dwid = b.source_dwid
and exists (select 1 from base b where a.source_dwid = b.source_dwid)
; -- 129191



update staging_hk.temp_new_sale_records_daily_txn st
set bedroom_count = coalesce(p.bedroom_count, st.source_bedroom_count),
    gross_floor_area_sqm = coalesce(p.gross_floor_area_sqm, st.source_gross_floor_area_sqm::numeric),
    net_floor_area_sqm = coalesce(p.net_floor_area_sqm, st.source_net_floor_area_sqm::numeric),
    address_unit = coalesce(p.address_unit, upper(st.address_floor_text || '-' || st.address_stack))
from masterdata_hk.property p
where st.property_dwid = p.property_dwid
; -- 83953


update staging_hk.temp_new_sale_records_daily_txn sr
set status_code = 'property-not-found'
where sr.property_dwid isnull
and sr.status_code isnull
and (sr.project_dwid notnull or sr.building_dwid notnull)
and sr.address_dwid notnull; -- 4750



select distinct st.data_uuid, d.activity_dwid 
from staging_hk.temp_new_sale_records_daily_txn st
left join "source".hk_midland_realty_sale_transaction c on right(c.id, -10) = st.data_source_id::text
left join map_hk.midland_sale_txn__map d on c.data_uuid::uuid = d.data_uuid::uuid
where d.activity_dwid notnull; -- 89035


select st.data_uuid, b.activity_dwid 
from staging_hk.temp_new_sale_records_daily_txn st
left join map_hk.daily_txn_sale_txn__map b on st.data_uuid::uuid = b.data_uuid::uuid 
where b.activity_dwid notnull; -- 88416



with base as (
	select distinct st.data_uuid, d.activity_dwid 
	from staging_hk.temp_new_sale_records_daily_txn st
	left join "source".hk_midland_realty_sale_transaction c on right(c.id, -10) = st.data_source_id::text
	left join map_hk.midland_sale_txn__map d on c.data_uuid::uuid = d.data_uuid::uuid
	where d.activity_dwid notnull and st.activity_dwid isnull
)
update staging_hk.temp_new_sale_records_daily_txn a
set activity_dwid = b.activity_dwid,
    status_code = 'transaction-verified'
from base b where a.data_uuid::uuid = b.data_uuid::uuid
; -- 89035



select
    st.source_dwid,
    b.activity_dwid,
    row_number() over (partition by st.source_dwid order by abs(st.delivery_date-b.settlement_date)) as seq
from staging_hk.temp_new_sale_records_daily_txn st
    left join masterdata_hk.sale_transaction b
        on st.property_dwid = b.property_dwid
        and st.purchase_amount <= b.purchase_amount + 10
        and st.purchase_amount >= b.purchase_amount - 10
        and st.source_transaction_date >= b.contract_date - interval '1 month'
        and st.source_transaction_date <= b.contract_date + interval '1 month'
where st.activity_dwid isnull and b.activity_dwid notnull
and st.data_source = 'hk-gov-daily-transaction'
;

select abs('2021-12-28'::date-'2021-12-29'::date);


with base as (
	select
	    st.source_dwid,
	    b.activity_dwid,
	    row_number() over (partition by st.source_dwid order by abs(st.delivery_date-b.settlement_date)) as seq
	from staging_hk.temp_new_sale_records_daily_txn st
	    left join masterdata_hk.sale_transaction b
	        on st.property_dwid = b.property_dwid
	        and st.purchase_amount <= b.purchase_amount + 10
	        and st.purchase_amount >= b.purchase_amount - 10
	        and st.source_transaction_date >= b.contract_date - interval '1 month'
	        and st.source_transaction_date <= b.contract_date + interval '1 month'
	where st.activity_dwid isnull and b.activity_dwid notnull
	and st.data_source = 'hk-gov-daily-transaction'
)
update staging_hk.temp_new_sale_records_daily_txn a
set activity_dwid = b.activity_dwid,
    status_code = 'transaction-verified'
from base b
where a.source_dwid = b.source_dwid and b.seq = 1
;





with base as (
	select
	    st.source_dwid,
	    b.activity_dwid,
	    row_number() over (partition by st.source_dwid order by abs(st.delivery_date-b.settlement_date)) as seq
	from staging_hk.temp_new_sale_records_daily_txn st
	    left join masterdata_hk.sale_transaction b
	        on st.property_dwid = b.property_dwid
	        and st.purchase_amount <= b.purchase_amount + 1000
	        and st.purchase_amount >= b.purchase_amount - 1000
	        and st.source_transaction_date >= b.contract_date - interval '1 month'
	        and st.source_transaction_date <= b.contract_date + interval '1 month'
	    left join "source".hk_midland_realty_sale_transaction c 
	    	on b.data_uuid::uuid = c.data_uuid::uuid
	    	and st.net_floor_area_sqm <= c.net_area_sqm + 1
	    	and st.net_floor_area_sqm >= c.net_area_sqm - 1
	where st.activity_dwid isnull and b.activity_dwid notnull
	and st.data_source = 'hk-gov-daily-transaction' and b.data_source = 'hk-midland-transaction-sale'
	and c.id notnull -- 1203
)
update staging_hk.temp_new_sale_records_daily_txn a
set activity_dwid = b.activity_dwid,
    status_code = 'transaction-verified'
from base b
where a.source_dwid = b.source_dwid and b.seq = 1
; -- 1197



update staging_hk.temp_new_sale_records_daily_txn a
set property_dwid = b.property_dwid,
    project_dwid = b.project_dwid,
    building_dwid = b.building_dwid,
    address_dwid = b.address_dwid,
    project_phase_dwid = b.project_phase_dwid
from masterdata_hk.sale_transaction b
where a.activity_dwid = b.activity_dwid
and a.activity_dwid notnull
;



-- QA:
select 
    count(*) as total_records,
    count(activity_dwid) ,
    (count(activity_dwid)*100/count(*)) as activities_mapped,
    count(property_dwid) ,
    (count(property_dwid)*100/count(*)) as properties_mapped,
    count(project_dwid) ,
    (count(project_dwid)*100/count(*)) as projects_mapped,
    count(building_dwid),
    (count(building_dwid)*100/count(*)) as buildings_mapped,
    count(address_dwid),
    (count(address_dwid)*100/count(*)) as addresses_mapped,
    count(project_phase_dwid),
    (count(project_phase_dwid)*100/count(*)) as phases_mapped
from staging_hk.temp_new_sale_records_daily_txn
;-- 129191	105617	81	104747	81	87591	67	106182	82	107293	83	17441	13
-- 129191	106814	82	104734	81	87625	67	106219	82	107293	83	17441	13






insert into staging_hk.temp_masterdata_sale_transaction
(
    property_dwid,
    address_dwid,
    building_dwid,
    project_phase_dwid,
    project_dwid,
    units_sold,
    sale_type,
    property_type_code,
    tenure_code,
    bedroom_count,
    bathroom_count,
    property_completion_year,
    address_unit,
    gross_floor_area_sqm,
    net_floor_area_sqm,
    contract_date,
    settlement_date,
    purchase_amount,
    country_code,
    --source_dwid,
    data_source,
    --data_source_id,
    data_uuid
)
select
    m.property_dwid,
    m.address_dwid,
    m.building_dwid,
    m.project_phase_dwid,
    m.project_dwid,
    1::int as units_sold,
    m.sale_type,
    m.source_property_type as property_type_code,
    m.tenure_code,
    m.bedroom_count,
    m.bathroom_count,
	coalesce(m.property_completion_year, pj.completion_year) as property_completion_year,
	coalesce(m.address_unit, upper(m.address_floor_text || '-' || m.address_stack)) as address_unit,
    m.gross_floor_area_sqm,
    m.net_floor_area_sqm,
	m.source_transaction_date as contract_date,
	m.update_date as settlement_date,
    m.purchase_amount,
    m.country_code,
    --m.source_dwid,
    m.data_source,
    --m.data_source_id,
    m.data_uuid::uuid
from staging_hk.temp_new_sale_records_daily_txn m
    left join masterdata_hk.project pj on pj.project_dwid = m.project_dwid
-- one time ingest
where m.activity_dwid isnull
    and m.status_code isnull
    and m.data_source = 'hk-gov-daily-transaction'
on conflict do nothing; -- 20893
  




update staging_hk.temp_masterdata_sale_transaction
set id = nextval('metadata.activity_id_seq'::regclass)
where id isnull
;

update staging_hk.temp_masterdata_sale_transaction
set activity_dwid = api.get_dwid(country_code, 'sale_transaction', id)
where activity_dwid isnull and property_dwid notnull
;

--- set the activity dwid of the primary match
update staging_hk.temp_new_sale_records_daily_txn m
set activity_dwid = st.activity_dwid,
    status_code = 'transaction-source'
from staging_hk.temp_masterdata_sale_transaction st
where st.data_uuid::uuid = m.data_uuid::uuid
and m.activity_dwid isnull
; -- 20893




insert into masterdata_hk.sale_transaction_data_source
(
    activity_dwid,
    data_source,
    data_source_id,
    data_source_datetime,
    source_dwid
)
select
    activity_dwid,
    data_source,
    data_source_id,
    source_transaction_date,
    source_dwid
from staging_hk.temp_new_sale_records_daily_txn
where activity_dwid notnull
on conflict do nothing
; -- 109713



insert into masterdata_hk.sale_transaction
select * from staging_hk.temp_masterdata_sale_transaction
where activity_dwid notnull
on conflict do nothing; -- 2899


-- update data source naming for old records
update masterdata_hk.sale_transaction
set data_source = 'hk-gov-daily-transaction'
where data_source = 'hk-daily-transaction';


select sale_type, count(*)
from masterdata_hk.sale_transaction
where data_source = 'hk-gov-daily-transaction'
group by 1;


select *
from masterdata_hk.sale_transaction
where data_source = 'hk-gov-daily-transaction'
and sale_type isnull;

with base as (
select data_uuid, count(*)
from map_hk.daily_txn_sale_txn__map
group by 1 having count(*) > 1 -- 49
)
delete from map_hk.daily_txn_sale_txn__map where data_uuid in (select distinct data_uuid from base); --102



-- todo: update table constraint

ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_pk PRIMARY KEY (data_uuid);
ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_un UNIQUE (data_uuid);

ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_fk FOREIGN KEY (property_dwid) REFERENCES masterdata_hk.property(property_dwid);
ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_fk_1 FOREIGN KEY (project_dwid) REFERENCES masterdata_hk.project(project_dwid);
ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_fk_2 FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid);
ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_fk_3 FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);
ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_fk_4 FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);
ALTER TABLE map_hk.daily_txn_sale_txn__map ADD CONSTRAINT daily_txn_sale_txn__map_fk_5 FOREIGN KEY (activity_dwid) REFERENCES masterdata_hk.sale_transaction(activity_dwid);


CREATE INDEX daily_txn_sale_txn__map_activity_dwid_idx ON map_hk.daily_txn_sale_txn__map USING btree (activity_dwid);
CREATE INDEX daily_txn_sale_txn__map_address_dwid_idx ON map_hk.daily_txn_sale_txn__map USING btree (address_dwid);
CREATE INDEX daily_txn_sale_txn__map_building_dwid_idx ON map_hk.daily_txn_sale_txn__map USING btree (building_dwid);
CREATE INDEX daily_txn_sale_txn__map_project_dwid_idx ON map_hk.daily_txn_sale_txn__map USING btree (project_dwid);
CREATE INDEX daily_txn_sale_txn__map_property_dwid_idx ON map_hk.daily_txn_sale_txn__map USING btree (property_dwid);

GRANT SELECT ON TABLE map_hk.daily_txn_sale_txn__map TO data_science;





insert into map_hk.daily_txn_sale_txn__map
(
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    project_phase_dwid
)
select
    data_uuid::uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    project_phase_dwid
from staging_hk.temp_new_sale_records_daily_txn m
where m.data_source = 'hk-gov-daily-transaction' -- normal process
and not exists -- one time
    (
        select 1 from map_hk.daily_txn_sale_txn__map rm
        where rm.data_uuid::uuid = m.data_uuid::uuid
    ) -- 1324
on conflict do nothing
;




ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_pkey_id PRIMARY KEY (id);


ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_fk_property_dwid FOREIGN KEY (property_dwid) REFERENCES masterdata_hk.property(property_dwid);
ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_fk_project_dwid FOREIGN KEY (project_dwid) REFERENCES masterdata_hk.project(project_dwid);
ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_fk_building_dwid FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid);
ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_fk_address_dwid FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);
ALTER TABLE premap_hk.daily_txn_unit_to_dwid ADD CONSTRAINT daily_txn_unit_to_dwid_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);

CREATE INDEX daily_txn_unit_to_dwid_address_dwid_idx ON premap_hk.daily_txn_unit_to_dwid USING btree (address_dwid);
CREATE INDEX daily_txn_unit_to_dwid_building_dwid_idx ON premap_hk.daily_txn_unit_to_dwid USING btree (building_dwid);
CREATE INDEX daily_txn_unit_to_dwid_project_dwid_idx ON premap_hk.daily_txn_unit_to_dwid USING btree (project_dwid);
CREATE INDEX daily_txn_unit_to_dwid_property_dwid_idx ON premap_hk.daily_txn_unit_to_dwid USING btree (property_dwid);



ALTER TABLE premap_hk.daily_txn_building_to_dwid ADD CONSTRAINT daily_txn_building_to_dwid_pkey_id PRIMARY KEY (id);

ALTER TABLE premap_hk.daily_txn_building_to_dwid ADD CONSTRAINT daily_txn_building_to_dwid_fk_project_dwid FOREIGN KEY (project_dwid) REFERENCES masterdata_hk.project(project_dwid);
ALTER TABLE premap_hk.daily_txn_building_to_dwid ADD CONSTRAINT daily_txn_building_to_dwid_fk_building_dwid FOREIGN KEY (building_dwid) REFERENCES masterdata_hk.building(building_dwid);
ALTER TABLE premap_hk.daily_txn_building_to_dwid ADD CONSTRAINT daily_txn_building_to_dwid_fk_address_dwid FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);
ALTER TABLE premap_hk.daily_txn_building_to_dwid ADD CONSTRAINT daily_txn_building_to_dwid_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);

CREATE INDEX daily_txn_building_to_dwid_address_dwid_idx ON premap_hk.daily_txn_building_to_dwid USING btree (address_dwid);
CREATE INDEX daily_txn_building_to_dwid_building_dwid_idx ON premap_hk.daily_txn_building_to_dwid USING btree (building_dwid);
CREATE INDEX daily_txn_building_to_dwid_project_dwid_idx ON premap_hk.daily_txn_building_to_dwid USING btree (project_dwid);





insert into premap_hk.daily_txn_unit_to_dwid
(
	district_code,
	estate_name,
	phase_name,
	building_name,
	address_number,
	address_street,
	floor,
	unit,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid,
    project_phase_dwid
)


with change_request_base as
(
    select distinct	
		b.district_code , 
		b.estate_name , 
		trim(coalesce(b.phase_description, '') || ' ' || coalesce(b.phase_num, '')) as phase_name,
		b.building_name , 
		case when b.address_num_to1 notnull then b.address_num_from1 || '-' || b.address_num_to1 
		else b.address_num_from1 end as address_number,
		b.address_street1 as address_street,
		b.floor,
		b.unit,
        a.property_dwid,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        null as lot_group_dwid,
        a.project_phase_dwid
    from staging_hk.temp_new_sale_records_daily_txn a -- temp_new_sale_records a
        left join "source".hk_daily_transaction b
            on a.data_uuid::uuid = b.data_uuid::uuid
        left join premap_hk.daily_txn_unit_to_dwid tb
            on f_prep_dw_id(b.district_code) = f_prep_dw_id(tb.district_code)
            and f_prep_dw_id(b.estate_name) = f_prep_dw_id(tb.estate_name)
            and f_prep_dw_id(trim(coalesce(b.phase_description, '') || ' ' || coalesce(b.phase_num, ''))) = f_prep_dw_id(tb.phase_name)
            and f_prep_dw_id(b.building_name) = f_prep_dw_id(tb.building_name)
            and f_prep_dw_id(case when b.address_num_to1 notnull then b.address_num_from1 || '-' || b.address_num_to1 
		else b.address_num_from1 end) = f_prep_dw_id(tb.address_number)
            and f_prep_dw_id(b.address_street1) = f_prep_dw_id(tb.address_street)
            and f_prep_dw_id(b.floor) = f_prep_dw_id(tb.floor)
            and f_prep_dw_id(b.unit) = f_prep_dw_id(tb.unit)
    where b.data_uuid notnull and tb.id isnull and a.data_source = 'hk-gov-daily-transaction'
)
, change_request as (
select
	district_code,
	estate_name,
	phase_name,
	building_name,
	address_number,
	address_street,
	floor,
	unit,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid,
    project_phase_dwid,
    row_number() over (partition by district_code, estate_name, phase_name, building_name, address_number, address_street, floor, unit
                       order by project_dwid) as seq
from change_request_base
)
select
	district_code,
	estate_name,
	phase_name,
	building_name,
	address_number,
	address_street,
	floor,
	unit,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid,
    project_phase_dwid
from change_request
where seq = 1
;


insert into premap_hk.daily_txn_unit_to_dwid
(
	district_code,
	estate_name,
	phase_name,
	building_name,
	address_number,
	address_street,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid,
    project_phase_dwid
)



with change_request_base as
(
    select distinct	
		b.district_code , 
		b.estate_name , 
		trim(coalesce(b.phase_description, '') || ' ' || coalesce(b.phase_num, '')) as phase_name,
		b.building_name , 
		case when b.address_num_to1 notnull then b.address_num_from1 || '-' || b.address_num_to1 
		else b.address_num_from1 end as address_number,
		b.address_street1 as address_street,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        null as lot_group_dwid,
        a.project_phase_dwid
    from staging_hk.temp_new_sale_records_daily_txn a -- temp_new_sale_records a
        left join "source".hk_daily_transaction b
            on a.data_uuid::uuid = b.data_uuid::uuid
        left join premap_hk.daily_txn_building_to_dwid tb
            on f_prep_dw_id(b.district_code) = f_prep_dw_id(tb.district_code)
            and f_prep_dw_id(b.estate_name) = f_prep_dw_id(tb.estate_name)
            and f_prep_dw_id(trim(coalesce(b.phase_description, '') || ' ' || coalesce(b.phase_num, ''))) = f_prep_dw_id(tb.phase_name)
            and f_prep_dw_id(b.building_name) = f_prep_dw_id(tb.building_name)
            and f_prep_dw_id(case when b.address_num_to1 notnull then b.address_num_from1 || '-' || b.address_num_to1 
		else b.address_num_from1 end) = f_prep_dw_id(tb.address_number)
            and f_prep_dw_id(b.address_street1) = f_prep_dw_id(tb.address_street)
    where b.data_uuid notnull and tb.id isnull and a.data_source = 'hk-gov-daily-transaction'
)
, change_request as (
select
	district_code,
	estate_name,
	phase_name,
	building_name,
	address_number,
	address_street,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid,
    project_phase_dwid,
    row_number() over (partition by district_code, estate_name, phase_name, building_name, address_number, address_street
                       order by project_dwid) as seq
from change_request_base
)
select
	district_code,
	estate_name,
	phase_name,
	building_name,
	address_number,
	address_street,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid,
    project_phase_dwid
from change_request
where seq = 1
;


select 
from masterdata_hk,temp_new_sale_listings tnsl 
where geographic_area isnull;
where




update staging_hk.temp_new_sale_records_daily_txn st
set bedroom_count = coalesce(p.bedroom_count, st.source_bedroom_count),
    gross_floor_area_sqm = coalesce(p.gross_floor_area_sqm, st.source_gross_floor_area_sqm::numeric),
    net_floor_area_sqm = coalesce(p.net_floor_area_sqm, st.source_net_floor_area_sqm::numeric),
    address_unit = coalesce(p.address_unit, upper(st.address_floor_text || '-' || st.address_stack))
from masterdata_hk.property p
where st.property_dwid = p.property_dwid
;



with base as (
select distinct st.activity_dwid 
from staging_hk.temp_new_sale_records_daily_txn st
left join masterdata_hk.property p on st.property_dwid = p.property_dwid
where p.property_dwid isnull -- 4980
)
update staging_hk.temp_new_sale_records_daily_txn
set bedroom_count = source_bedroom_count,
    gross_floor_area_sqm = source_gross_floor_area_sqm::numeric,
    net_floor_area_sqm = source_net_floor_area_sqm::numeric,
    address_unit = upper(address_floor_text || '-' || address_stack)
where activity_dwid in (select activity_dwid from base);


select property_type_code, coalesce(gross_floor_area_sqm, net_floor_area_sqm) notnull, count(*)
from masterdata_hk.sale_transaction st 
where data_source = 'hk-gov-daily-transaction'
group by 1,2 order by 1,2
;

'''
comm	false	9
comm	true	56
condo	false	93
condo	true	8150
hos	false	75
hos	true	288
NULL	false	48
'''

with base as (
select 
	a.activity_dwid , st.bedroom_count , st.gross_floor_area_sqm , st.net_floor_area_sqm 
from masterdata_hk.sale_transaction a 
left join staging_hk.temp_new_sale_records_daily_txn st on a.activity_dwid = st.activity_dwid 
where a.data_source = 'hk-gov-daily-transaction'
)
update masterdata_hk.sale_transaction a
set bedroom_count = b.bedroom_count,
	gross_floor_area_sqm = b.gross_floor_area_sqm,
	net_floor_area_sqm = b.net_floor_area_sqm
from base b where a.activity_dwid = b.activity_dwid
;


--> revert the update 

select property_type_code, coalesce(gross_floor_area_sqm, net_floor_area_sqm) notnull, count(*)
from masterdata_hk.sale_transaction_0627backup st 
where data_source = 'hk-gov-daily-transaction'
group by 1,2 order by 1,2
;



with base as (
select 
	st.activity_dwid , st.bedroom_count , st.gross_floor_area_sqm , st.net_floor_area_sqm 
from masterdata_hk.sale_transaction_0627backup st 
where data_source = 'hk-gov-daily-transaction'
)
update masterdata_hk.sale_transaction a
set bedroom_count = b.bedroom_count,
	gross_floor_area_sqm = b.gross_floor_area_sqm,
	net_floor_area_sqm = b.net_floor_area_sqm
from base b where a.activity_dwid = b.activity_dwid
;



























