-- promote phase to project, project to development

'''
So basically the concept of phase is more like the "project" in SG. Although the multiple phases can under a same estate name, when I check with Grace, she mentioned that when people purchase the house, they tend to look at specific phase since each of them have their own launching time, selling price, building age and corresponding market condition. 


The time range of launching from first phase to the latest one can be more than 10 years. (eg Lohas Park) So stay at a general project level for certain features is too vague and blur for more in-depth analysis. Eg. the completion year and project age, previously were struggling to keep it in building level or project level, both of which are not so appropriate. 

So for AVM side, explicit phase level will be helpful for the valuation for a more accurate features and were also reaching out to most similar phase to find comparable trans for valuation. 
For product side, we plan to put similar phases and corresponding features inside the property report for a clearer indication when agent use it (generated from Real Agent APP). Also for the real analytics were going to launch, certain analysis need to go down to phase level. Also a phase base table will help to smoother the process of transferring some analytical output from AVM team to product team to display.
For Demand Curve side, we plan to initiate demand curve modelling for HK soon. This model is greatly market-associated, means it need to have exact input like launching time, current market stock, supply etc. And the demand curve can only be generated down to certain phase and building level. 

This improvement was decided when I had meeting with Patrick and Grace. He mentioned that its necessary for further improvement we may need to try and do. Graces also acknowledge on this.
'''
create a copy of current masterdata_hk schema, and refactor based on the copy schema

1.project -> development
1)address: no big change - development_phase column will be our target new project concept; 'project-address' address_type_code rename to 'development-address'
2)project: project table rename to 'development', project_dwid rename to 'development_dwid'; address_dwid use 'development-address' type (old 'project-address' type) dwid; 'project_xx' columns renmame to 'development_xx'; completion_year rename to development_completion_year? or built_year?;
	unit_count, residential_unit_count and commercial_unit_count should be consistent with new project (old phase) table total count in each development; slug change prefix to be 'development/...';
3)(need to add a new project table containing phase level info: see 2.)
4)building: project_dwid rename to 'development_dwid'; add new project_dwid (new dwid for phase); slug change to use new project concept rather than development concept;
5)property: project_dwid rename to 'development_dwid'; add new project_dwid (new dwid for phase); slug change to use new project concept rather than development concept;
6)sale/rent txn/listing: project_dwid rename to 'development_dwid'; add new project_dwid (new dwid for phase); 
7)unit_group: slug change to use new project concept rather than development concept;


2.phase -> project: (question: if a development does not have multi phases, do we need to create a project using same name with its development name? or just create project for those developments have phases?)
1)address: add new 'project-address' type address for each phase in development

2)new project table: column structure no big change - 
id, new project_dwid (new dwid for phase), development_dwid (old project_dwid), address_dwid use new 'project-address' type dwid, developer_dwid, lot_group_dwid, 

project_type_code,project_name,project_display_name,project_name_text,completion_year, ownership_type_code,tenure_code,unit_count,residential_unit_count,commercial_unit_count,(these info are for each phase)

address_display_text,location_display_text,location_marker,status_code,slug,country_code,is_active,data_source,data_source_id


3.update all related tables:
1)premap table: project_dwid rename to 'development_dwid'; add new project_dwid (new dwid for phase); 
2)map table: project_dwid rename to 'development_dwid'; add new project_dwid (new dwid for phase); 
3)feature table: project_dwid rename to 'development_dwid'; add new project_dwid (new dwid for phase); 
4)translation table: address - add phase level new records; phase - rename to project and use new project_dwid; project - rename to development and use development_dwid; 


4.pay attention: 
Before we actually update masterdata_hk schema with new development and project concept!
need to refactor HK ingestion query;
need to refactor HK UI query; 




-- create project_phase core table
0)create a copy of current masterdata_hk schema, and refactor based on the copy schema [done]

1)address: add new 'project-phase-address' type address for each phase in projects;[in progress]
2.1)create project_phase table, use similar table structure with project table; [to do]
2.2)create project reocrds for those single buildings without project - every building should have its project;[move to another task]
3)building / property: add column project_phase_dwid, slug change to use phase concept; [to do]
4)sale/rent txn/listing: add column project_phase_dwid; [to do]
5)unit_group: slug change to use phase concept; [to do]
6)related tables: add column project_phase_dwid 
 - premap, map, feature, translation [to do]
7)need to refactor HK ingestion query and HK UI query before we actually update masterdata_hk schema. [to do]



SELECT f_clone_table('masterdata_hk', 'address', 'staging_hk', 'masterdata_hk_address_wo_phase_address', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'building', 'staging_hk', 'masterdata_hk_building_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'project', 'staging_hk', 'masterdata_hk_project_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'property', 'staging_hk', 'masterdata_hk_property_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'sale_transaction', 'staging_hk', 'masterdata_hk_sale_transaction_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'sale_listing', 'staging_hk', 'masterdata_hk_sale_listing_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'rent_transaction', 'staging_hk', 'masterdata_hk_rent_transaction_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'rent_listing', 'staging_hk', 'masterdata_hk_rent_listing_wo_phase', TRUE, TRUE);
SELECT f_clone_table('masterdata_hk', 'unit_group', 'staging_hk', 'masterdata_hk_unit_group_wo_phase', TRUE, TRUE);


select * from staging_hk.masterdata_hk_building_wo_phase mhbwp where project_dwid isnull;

select * from feature_hk.de__building__phase_info dbpi; --10148

select count(distinct project_dwid), count(distinct phase_dwid), count(distinct building_dwid)
from feature_hk.de__building__phase_info dbpi; -- 127 project	414 phase  10148 building

select * from staging_hk.masterdata_hk_project_wo_phase mhpwp limit 100;


select phase_dwid , project_dwid , development_phase , phase_num 
from feature_hk.de__building__phase_info dbpi 
group by 1,2,3,4


CREATE SEQUENCE metadata.project_phase_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 9223372036854775807
	START 1
	CACHE 1
	NO CYCLE;

drop table staging_hk.masterdata_hk_project_phase;

CREATE TABLE staging_hk.masterdata_hk_project_phase (
	id int8 NOT NULL DEFAULT nextval('metadata.project_phase_id_seq'::regclass),
	project_dwid text NULL,
	project_phase_dwid text NULL,
	address_dwid text NULL,
	developer_dwid text NULL,
	lot_group_dwid text NULL,
	project_phase_type_code text NULL,
	project_phase_name text NULL,
	project_phase_number text NULL,
	project_phase_display_name text NULL,
	project_phase_name_text text NULL,
	completion_year int2 NULL,
	ownership_type_code text NULL,
	tenure_code text NULL,
	unit_count int4 NULL,
	residential_unit_count int4 NULL,
	commercial_unit_count int4 NULL,
	address_display_text text NULL,
	location_display_text text NULL,
	location_marker public.geometry NULL,
	status_code text NULL,
	slug text NULL,
	country_code varchar(2) NULL,
	is_active bool NULL,
	data_source varchar NULL,
	data_source_id varchar NULL
);


insert into staging_hk.masterdata_hk_project_phase 
(project_phase_dwid, project_dwid, project_phase_name_text , project_phase_number)
select phase_dwid , project_dwid , development_phase , phase_num 
from feature_hk.de__building__phase_info dbpi 
group by 1,2,3,4; -- 414


with base as (
select distinct project_dwid , developer_dwid 
from staging_hk.masterdata_hk_project_wo_phase where developer_dwid notnull
)
update staging_hk.masterdata_hk_project_phase a
set developer_dwid = b.developer_dwid
from base b where a.project_dwid = b.project_dwid; --19


update staging_hk.masterdata_hk_project_phase
set project_phase_name = api.get_normalized_name(project_phase_name_text), project_phase_display_name = initcap(project_phase_name_text) 
where project_phase_name_text notnull
;

update staging_hk.masterdata_hk_project_phase
set country_code = 'cn', is_active = true;

-- completion year by phase

with base as (
select phase_dwid , b.construction_end , b.completion_year ,row_number() over (partition by phase_dwid order by b.completion_year desc) as seq
from feature_hk.de__building__phase_info a
left join feature_hk.de__building__completion_year_info b on a.building_dwid = b.building_dwid  
where b.completion_year notnull
group by 1,2,3 order by 1
-- use the latest completion year among all buildings within each phase
)
update staging_hk.masterdata_hk_project_phase a
set completion_year = b.completion_year::int
from base b where a.project_phase_dwid = b.phase_dwid and b.seq = 1


-- use the centeral point of all buildings within a phase as the phase location_marker
with base1 as (
select phase_dwid , a.building_dwid , d.location_marker 
from feature_hk.de__building__phase_info a
left join feature_hk.de__building__completion_year_info b on a.building_dwid = b.building_dwid  
left join staging_hk.masterdata_hk_building_wo_phase c on a.building_dwid = c.building_dwid 
left join staging_hk.masterdata_hk_address_wo_phase_address d on c.address_dwid = d.address_dwid  
where b.completion_year notnull
group by 1,2,3 order by 1
)
, base2 as (
select phase_dwid, ST_Centroid(st_union(location_marker)) as phase_location_marker
from base1 
group by 1
)
update staging_hk.masterdata_hk_project_phase a
set location_marker = b.phase_location_marker
from base2 b where a.project_phase_dwid = b.phase_dwid
; -- 332

-- tenure_code follow its project
update staging_hk.masterdata_hk_project_phase a
set tenure_code = b.tenure_code 
from staging_hk.masterdata_hk_project_wo_phase b where a.project_dwid = b.project_dwid 
;


-- unit_count
with base as (
select phase_dwid , count(distinct b.property_dwid) as unit_count
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_property_wo_phase b on a.building_dwid = b.building_dwid  
group by 1
)
update staging_hk.masterdata_hk_project_phase a
set unit_count = b.unit_count 
from base b where a.project_phase_dwid = b.phase_dwid
;

select distinct property_type_code from staging_hk.masterdata_hk_property_wo_phase mhpwp; -- 

with base as (
select phase_dwid , count(distinct b.property_dwid) as residential_unit_count
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_property_wo_phase b on a.building_dwid = b.building_dwid  
where b.property_type_code in ('condo', 'hos', 'hse')
group by 1
)
update staging_hk.masterdata_hk_project_phase a
set residential_unit_count = b.residential_unit_count 
from base b where a.project_phase_dwid = b.phase_dwid
;

with base as (
select phase_dwid , count(distinct b.property_dwid) as commercial_unit_count
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_property_wo_phase b on a.building_dwid = b.building_dwid  
where b.property_type_code in ('comm', 'offc', 'club')
group by 1
)
update staging_hk.masterdata_hk_project_phase a
set commercial_unit_count = b.commercial_unit_count 
from base b where a.project_phase_dwid = b.phase_dwid
;

update staging_hk.masterdata_hk_project_phase a
set commercial_unit_count = 0
where commercial_unit_count isnull;

update staging_hk.masterdata_hk_project_phase a
set residential_unit_count = 0
where residential_unit_count isnull;

-- project_phase_type_code follow its project
update staging_hk.masterdata_hk_project_phase a
set project_phase_type_code = b.project_type_code  
from staging_hk.masterdata_hk_project_wo_phase b where a.project_dwid = b.project_dwid 
;


-- address_dwid

select phase_dwid , c.address_dwid , c.full_address_text 
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_building_wo_phase b on a.building_dwid = b.building_dwid  
left join staging_hk.masterdata_hk_address_wo_phase_address c on b.address_dwid = c.address_dwid 
group by 1,2,3 order by 1;


select a.phase_dwid ,
	lower(trim(trim(coalesce(c.street_name , '') || ',,' || coalesce(c.development_phase , '') || ',' || coalesce(c.development, '') || ',' || coalesce(c.city_subarea , '') || ',' || coalesce(c.city_area, '')
		|| ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ','))) as full_address_text,
    replace(replace(initcap(replace(trim(replace(replace(replace(replace(trim(trim(coalesce(c.development , '') || ',' || coalesce(c.development_phase , '') || ',' ||
        coalesce(c.street_name, '') || ',' || coalesce(c.city_subarea , '') || ',' ||
        coalesce(c.city, ''), ',')), ',,,,', ','), ',,,', ','), ',,', ','), ', ,', ','), ','), ',', ', ')), '  ', ' '), '''S', '''s') as address_display_text
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_building_wo_phase b on a.building_dwid = b.building_dwid  
left join staging_hk.masterdata_hk_address_wo_phase_address c on b.address_dwid = c.address_dwid 
group by 1,2,3;



--insert into staging_hk.masterdata_hk_address_phase_address
update masterdata_hk.address 
set is_active = false
where address_type_code = 'project-phase-address';
-- DONE:
select count(*) from masterdata_hk.address where address_type_code = 'project-phase-address' and is_active = false;
-- delete from masterdata_hk.address where address_type_code = 'project-phase-address' and is_active = False;
-- delete from masterdata_hk.address where id >= 691226050 and id <= 691227367;
-- 691217017 ~ 691227367


insert into masterdata_hk.address 
(
address_type_code,full_address_text,address_display_text,address_street_text,street_name,street_name_root,development,development_phase,
city,city_id,city_code,city_area,city_area_id,city_area_code,city_subarea,city_subarea_id,city_subarea_code,metro_area,metro_area_code,
country,country_code,country_3code,geographic_zone,continent,status_code,language_code,data_source_count,hash_key,data_source,is_active
)
with phase_address_base as (
select development , development_phase , street_name , 
        c.city,
        c.city_id,
        c.city_code,
        c.city_area,
        c.city_area_id,
        c.city_area_code,
        c.city_subarea,
        c.city_subarea_id,
        c.city_subarea_code
from staging_hk.masterdata_hk_address_wo_phase_address c
where development_phase notnull
)
select distinct 
	'project-phase-address' as address_type_code,
	lower(trim(trim(coalesce(c.street_name , '') || ',,' || coalesce(c.development_phase , '') || ',' || coalesce(c.development, '') || ',' || coalesce(c.city_subarea , '') || ',' || coalesce(c.city_area, '')
		|| ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ','))) as full_address_text,
    replace(replace(initcap(replace(trim(replace(replace(replace(replace(trim(trim(coalesce(c.development , '') || ',' || coalesce(c.development_phase , '') || ',' ||
        coalesce(c.street_name, '') || ',' || coalesce(c.city_subarea , '') || ',' ||
        coalesce(c.city, ''), ',')), ',,,,', ','), ',,,', ','), ',,', ','), ', ,', ','), ','), ',', ', ')), '  ', ' '), '''S', '''s') as address_display_text,
	lower(street_name) as address_street_text,
	lower(street_name) as street_name,
	lower(street_name) as street_name_root,
	development,
	development_phase,
    c.city,
    c.city_id,
    c.city_code,
    c.city_area,
    c.city_area_id,
    c.city_area_code,
    c.city_subarea ,
    c.city_subarea_id,
    c.city_subarea_code,
	'hong kong (sar)' as metro_area,
	'hk' as metro_area_code,
	'china' as country,
	'cn' as country_code,
	'chn' as country_3code,
	'south-east asia' as geographic_zone,
	'asia' as continent,
 	'active' as status_code,
 	'eng' as language_code,
 	'1'::int as data_source_count,
 	md5(trim(trim(lower(replace(replace(replace(replace(regexp_replace('cn/hk/' || coalesce(city_code, '') || coalesce(city_area_code, '') || coalesce(city_subarea_code, '')
 		|| '/' || coalesce(development, '') || '///' || coalesce(street_name, ''), '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '/'))) as hash_key,
 	'rea-manual' as data_source,
 	true as is_active 
from phase_address_base c
;


update masterdata_hk.address  -- staging_hk.masterdata_hk_address_phase_address
set slug = trim(trim(lower(replace(replace(replace(replace(regexp_replace('poi/hk/phase/' || coalesce(development, '') || '-' || 
				coalesce(development_phase , '') || '-' || id, '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '-'))
where address_type_code = 'project-phase-address' and slug isnull
;


update masterdata_hk.address 
set address_dwid = md5(country_code || '__' || 'address' || '__' || id)
where address_type_code = 'project-phase-address' and  address_dwid isnull
;



select full_address_text , count(*)
from masterdata_hk.address 
where address_type_code = 'project-phase-address'
group by 1




with base as (
select a.phase_dwid ,
	lower(trim(trim(coalesce(c.street_name , '') || ',,' || coalesce(c.development_phase , '') || ',' || coalesce(c.development, '') || ',' || coalesce(c.city_subarea , '') || ',' || coalesce(c.city_area, '')
		|| ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ','))) as full_address_text,
	b.address_display_text 
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_building_wo_phase b on a.building_dwid = b.building_dwid  
left join staging_hk.masterdata_hk_address_wo_phase_address c on b.address_dwid = c.address_dwid 
group by 1,2,3
)
select a.phase_dwid , b.address_dwid, b.full_address_text
from base a
left join masterdata_hk.address b on a.full_address_text = b.full_address_text
where b.address_type_code = 'project-phase-address' and b.is_active = true
;



with base as (
select a.phase_dwid ,
	lower(trim(trim(coalesce(c.street_name , '') || ',,' || coalesce(c.development_phase , '') || ',' || coalesce(c.development, '') || ',' || coalesce(c.city_subarea , '') || ',' || coalesce(c.city_area, '')
		|| ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ','))) as full_address_text
from feature_hk.de__building__phase_info a
left join staging_hk.masterdata_hk_building_wo_phase b on a.building_dwid = b.building_dwid  
left join staging_hk.masterdata_hk_address_wo_phase_address c on b.address_dwid = c.address_dwid 
group by 1,2
)
, base2 as (
select a.phase_dwid , b.address_dwid, row_number() over (partition by a.phase_dwid ) as seq
from base a
left join masterdata_hk.address b on a.full_address_text = b.full_address_text
where b.address_type_code = 'project-phase-address' and b.is_active = true
)
update staging_hk.masterdata_hk_project_phase a
set address_dwid = b.address_dwid 
from base2 b where a.project_phase_dwid = b.phase_dwid and b.seq = 1;

-- address_display_text,location_display_text

update staging_hk.masterdata_hk_project_phase a
set address_display_text = b.address_display_text
from masterdata_hk.address b where a.address_dwid = b.address_dwid 


update staging_hk.masterdata_hk_project_phase a
set location_display_text = trim(initcap(reverse(split_part(reverse(b.full_address_text), ',', 4)||' ,'||
		    split_part(reverse(b.full_address_text), ',', 3)||' ,'||
		    split_part(reverse(b.full_address_text), ',', 2))))
from masterdata_hk.address b where a.address_dwid = b.address_dwid 



-- slug

update staging_hk.masterdata_hk_project_phase a
set slug = api.clean_slug('project-phase/hk/' || coalesce(b.project_name, '') || '-' || b.id || '/'
            || coalesce(a.project_phase_name , '') || '-' || a.id)
from staging_hk.masterdata_hk_project_wo_phase b where a.project_dwid = b.project_dwid 
and a.slug isnull;



SELECT f_clone_table('staging_hk', 'masterdata_hk_project_phase', 'masterdata_hk', 'project_phase', TRUE, TRUE);


select project_phase_dwid , count(*)
from staging_hk.masterdata_hk_project_phase
group by 1 having count(*) > 1;

ALTER TABLE masterdata_hk.project_phase ADD CONSTRAINT project_phase_pk PRIMARY KEY (id);
ALTER TABLE masterdata_hk.project_phase ADD CONSTRAINT project_phase_un UNIQUE (project_phase_dwid);
ALTER TABLE masterdata_hk.project_phase ADD CONSTRAINT project_phase_slug_un UNIQUE (slug);
ALTER TABLE masterdata_hk.project_phase ADD CONSTRAINT project_phase_fk FOREIGN KEY (project_dwid) REFERENCES masterdata_hk.project(project_dwid);
ALTER TABLE masterdata_hk.project_phase ADD CONSTRAINT project_phase_to_address_fk FOREIGN KEY (address_dwid) REFERENCES masterdata_hk.address(address_dwid);


-- add project_phase_dwid into building and property table:

ALTER TABLE masterdata_hk.building ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update masterdata_hk.building a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 10148

ALTER TABLE masterdata_hk.building ADD CONSTRAINT building_to_project_phase_fk FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);


ALTER TABLE masterdata_hk.property ADD project_phase_dwid text NULL;


select count(*)
from masterdata_hk.property
where building_dwid in (select distinct building_dwid from feature_hk.de__building__phase_info); -- 206165


with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update masterdata_hk.property a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 206165


ALTER TABLE masterdata_hk.property ADD CONSTRAINT property_to_project_phase_fk FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);



-- add project_phase_dwid into sale/rent txn/listing table:

ALTER TABLE masterdata_hk.sale_transaction ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update masterdata_hk.sale_transaction a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 250702

select sum(case when project_phase_dwid notnull then 1 else 0 end)*1.0/count(*), count(*)  from masterdata_hk.sale_transaction; -- 0.14875172958848254273   1685372

ALTER TABLE masterdata_hk.sale_transaction ADD CONSTRAINT sale_transaction_to_project_phase_fk FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);



ALTER TABLE masterdata_hk.rent_transaction ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update masterdata_hk.rent_transaction a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 10624

select sum(case when project_phase_dwid notnull then 1 else 0 end)*1.0/count(*), count(*)  from masterdata_hk.rent_transaction; -- 0.17851861809382981584	59512

ALTER TABLE masterdata_hk.rent_transaction ADD CONSTRAINT rent_transaction_to_project_phase_fk FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);



ALTER TABLE masterdata_hk.sale_listing ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update masterdata_hk.sale_listing a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 20326

select sum(case when project_phase_dwid notnull then 1 else 0 end)*1.0/count(*), count(*)  from masterdata_hk.sale_listing; -- 0.16317192216299531180	124568

ALTER TABLE masterdata_hk.sale_listing ADD CONSTRAINT sale_listing_to_project_phase_fk FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);



ALTER TABLE masterdata_hk.rent_listing ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update masterdata_hk.rent_listing a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 15072

select sum(case when project_phase_dwid notnull then 1 else 0 end)*1.0/count(*), count(*)  from masterdata_hk.rent_listing; -- 0.16152609580966670239	93310

ALTER TABLE masterdata_hk.rent_listing ADD CONSTRAINT rent_listing_to_project_phase_fk FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);


-- refactor translation:

select a.id , b.phase_dwid , 
from translation_hk.phase_translation a 
left join feature_hk.de__building__phase_info b on a.building_dwid = b.building_dwid and lower(a.phase_name) = lower(b.development_phase) 


ALTER TABLE translation_hk.phase_translation ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update translation_hk.phase_translation a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 10122



-- refactor map:


ALTER TABLE map_hk.midland_sale_txn__map ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update map_hk.midland_sale_txn__map a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 47534

select sum(case when project_phase_dwid notnull then 1 else 0 end)*1.0/count(*), count(*) from map_hk.midland_sale_txn__map; -- 0.16285125014560479091	291886

ALTER TABLE map_hk.midland_sale_txn__map ADD CONSTRAINT midland_sale_txn__map_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);


ALTER TABLE map_hk.midland_rent_txn__map ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update map_hk.midland_rent_txn__map a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 10624


ALTER TABLE map_hk.midland_rent_txn__map ADD CONSTRAINT midland_rent_txn__map_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);




ALTER TABLE map_hk.midland_sale_listing__map ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update map_hk.midland_sale_listing__map a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 19002


ALTER TABLE map_hk.midland_sale_listing__map ADD CONSTRAINT midland_sale_listing__map_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);



ALTER TABLE map_hk.midland_rent_listing__map  ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update map_hk.midland_rent_listing__map a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 15072


ALTER TABLE map_hk.midland_rent_listing__map ADD CONSTRAINT midland_rent_listing__map_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);



-- refactor premap:


ALTER TABLE premap_hk.hk_post_to_dwid ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update premap_hk.hk_post_to_dwid a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 191982


select sum(case when project_phase_dwid notnull then 1 else 0 end)*1.0/count(*), count(*) from premap_hk.hk_post_to_dwid; -- 0.05952369141017404871	3225304


ALTER TABLE premap_hk.midland_building_to_dwid ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update premap_hk.midland_building_to_dwid a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 2452


ALTER TABLE premap_hk.midland_building_to_dwid ADD CONSTRAINT midland_building_to_dwid_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);


ALTER TABLE premap_hk.midland_unit_to_dwid  ADD project_phase_dwid text NULL;

with base as (
select distinct building_dwid , phase_dwid as project_phase_dwid
from feature_hk.de__building__phase_info
)
update premap_hk.midland_unit_to_dwid a
set project_phase_dwid = b.project_phase_dwid
from base b where a.building_dwid = b.building_dwid
; -- 63711


ALTER TABLE premap_hk.midland_unit_to_dwid ADD CONSTRAINT midland_unit_to_dwid_fk_project_phase_dwid FOREIGN KEY (project_phase_dwid) REFERENCES masterdata_hk.project_phase(project_phase_dwid);






---------------------------------------------------------------------------------
-- 1. manually add new phase records for target project
-- how to find phase info: https://www.midland.com.hk/en/list/estate

update masterdata_hk.project_phase 
set project_phase_name = api.get_normalized_name(project_phase_display_name),
	project_phase_name_text = lower(project_phase_display_name)
where project_phase_dwid isnull;


update masterdata_hk.project_phase a
set slug = api.clean_slug('project-phase/hk/' || coalesce(b.project_name, '') || '-' || b.id || '/'
            || coalesce(a.project_phase_name , '') || '-' || a.id)
from masterdata_hk.project b where a.project_dwid = b.project_dwid 
and a.project_phase_dwid isnull;


-- 2. update 'point-address' -- add phase info (esp. column full_address_text,address_display_text,development_phase)
update masterdata_hk.address
set slug = trim(trim(lower(replace(replace(replace(replace(regexp_replace('poi/hk/address/' || coalesce(address_building, '') || '-' ||
	coalesce(trim(coalesce(address_number, '') || ' ' || coalesce(street_name, '')), '') || '-' || id, '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '-'))
where address_type_code = 'point-address' and slug isnull;

update masterdata_hk.address
set address_dwid = md5(country_code || '__' || 'address' || '__' || id)
where address_dwid isnull;


-- 3.add phase address

insert into masterdata_hk.address 
(
address_type_code,full_address_text,address_display_text,address_street_text,street_name,street_name_root,development,development_phase,
city,city_id,city_code,city_area,city_area_id,city_area_code,city_subarea,city_subarea_id,city_subarea_code,metro_area,metro_area_code,
country,country_code,country_3code,geographic_zone,continent,status_code,language_code,data_source_count,hash_key,data_source,is_active
)
with phase_address_base as (
select distinct development , development_phase , street_name , 
        c.city,
        c.city_id,
        c.city_code,
        c.city_area,
        c.city_area_id,
        c.city_area_code,
        c.city_subarea,
        c.city_subarea_id,
        c.city_subarea_code
from masterdata_hk.address c
where development_phase notnull
and development ilike '%Dragons Range%'
)
select distinct 
	'project-phase-address' as address_type_code,
	lower(trim(trim(coalesce(c.street_name , '') || ',,' || coalesce(c.development_phase , '') || ',' || coalesce(c.development, '') || ',' || coalesce(c.city_subarea , '') || ',' || coalesce(c.city_area, '')
		|| ',' || coalesce(c.city, '') || ',' || 'hong kong (sar)', ','))) as full_address_text,
    replace(replace(initcap(replace(trim(replace(replace(replace(replace(trim(trim(coalesce(c.development , '') || ',' || coalesce(c.development_phase , '') || ',' ||
        coalesce(c.street_name, '') || ',' || coalesce(c.city_subarea , '') || ',' ||
        coalesce(c.city, ''), ',')), ',,,,', ','), ',,,', ','), ',,', ','), ', ,', ','), ','), ',', ', ')), '  ', ' '), '''S', '''s') as address_display_text,
	lower(street_name) as address_street_text,
	lower(street_name) as street_name,
	lower(street_name) as street_name_root,
	development,
	development_phase,
    c.city,
    c.city_id,
    c.city_code,
    c.city_area,
    c.city_area_id,
    c.city_area_code,
    c.city_subarea ,
    c.city_subarea_id,
    c.city_subarea_code,
	'hong kong (sar)' as metro_area,
	'hk' as metro_area_code,
	'china' as country,
	'cn' as country_code,
	'chn' as country_3code,
	'south-east asia' as geographic_zone,
	'asia' as continent,
 	'active' as status_code,
 	'eng' as language_code,
 	'1'::int as data_source_count,
 	md5(trim(trim(lower(replace(replace(replace(replace(regexp_replace('cn/hk/' || coalesce(city_code, '') || coalesce(city_area_code, '') || coalesce(city_subarea_code, '')
 		|| '/' || coalesce(development, '') || '///' || coalesce(street_name, ''), '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '/'))) as hash_key,
 	'rea-manual' as data_source,
 	true as is_active 
from phase_address_base c
;

update masterdata_hk.address  -- staging_hk.masterdata_hk_address_phase_address
set slug = trim(trim(lower(replace(replace(replace(replace(regexp_replace('poi/hk/phase/' || coalesce(development, '') || '-' || 
				coalesce(development_phase , '') || '-' || id, '[ ]+', '-'), ' ', '-'), '''', '-'), '---', '-'), '--', '-')), '-'))
where address_type_code = 'project-phase-address' and slug isnull
;


update masterdata_hk.address 
set address_dwid = md5(country_code || '__' || 'address' || '__' || id)
where address_type_code = 'project-phase-address' and  address_dwid isnull
;


-- 4.populate address_dwid, project_phase_dwid in project_phase
with base as (
select a.id , b.address_dwid 
from masterdata_hk.project_phase a
left join masterdata_hk.address b on a.project_phase_name_text = b.development_phase 
where b.address_type_code = 'project-phase-address'
and b.development ilike '%Dragons Range%'
)
update masterdata_hk.project_phase c
set address_dwid = d.address_dwid
from base d where c.id = d.id


update masterdata_hk.project_phase a
set address_display_text = b.address_display_text
from masterdata_hk.address b where a.address_dwid = b.address_dwid 
and a.project_phase_dwid isnull;


update masterdata_hk.project_phase a
set location_display_text = trim(initcap(reverse(split_part(reverse(b.full_address_text), ',', 4)||' ,'||
		    split_part(reverse(b.full_address_text), ',', 3)||' ,'||
		    split_part(reverse(b.full_address_text), ',', 2))))
from masterdata_hk.address b where a.address_dwid = b.address_dwid 
and a.project_phase_dwid isnull;


update masterdata_hk.project_phase
set project_phase_dwid = md5(country_code || '__' || 'project_phase' || '__' || id)
where project_phase_dwid isnull
;


-- 5.update building with phase info
update masterdata_hk.building a
set slug = api.clean_slug(b.slug || '/' || coalesce(a.building_name, '') || '-' || a.id)
from masterdata_hk.project b
where a.project_dwid = b.project_dwid
and a.building_dwid isnull and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3';

update masterdata_hk.building a
set address_display_text = lower(b.address_display_text)
from masterdata_hk.address b
where a.address_dwid = b.address_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3';

update masterdata_hk.building
set building_dwid = md5(country_code || '__' || 'building' || '__' || id)
where building_dwid isnull
;

-- 6.update premap_hk.midland_building_to_dwid
-- column building_dwid,address_dwid,project_phase_dwid


-- 7.update premap_hk.midland_unit_to_dwid
update premap_hk.midland_unit_to_dwid a
set building_dwid = b.building_dwid ,
	address_dwid = b.address_dwid ,
	project_dwid = b.project_dwid ,
	project_phase_dwid = b.project_phase_dwid 
from premap_hk.midland_building_to_dwid b
where a.building_id = b.building_id 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3';


create table staging_hk.temp_hk_midland_unit_0609 as 
select a.unit_id, p.property_dwid , a.phase_name 
from masterdata_hk.property p
left join masterdata_hk.building b on p.building_dwid = b.building_dwid 
left join reference.hk_midland_backfill a 
on lower(a.building_name) = lower(b.building_name_text)
and p.address_floor_text = a.floor 
and p.address_stack = a.flat  
and f_sqft2sqm(a.net_area::float) - 0.1 < p.net_floor_area_sqm 
and p.net_floor_area_sqm < f_sqft2sqm(a.net_area::float) + 0.1
where a.estate_id = 'E000015284' and p.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'


update premap_hk.midland_unit_to_dwid a
set property_dwid = b.property_dwid
from staging_hk.temp_hk_midland_unit_0609 b 
where a.unit_id = b.unit_id;

-- 8.remap building_dwid and project_phase_dwid in property table 

with base as (
select distinct p.property_dwid , b2.building_dwid , b2.address_dwid , b2.project_phase_dwid 
from masterdata_hk.property p
left join staging_hk.temp_hk_midland_unit_0609 a
on p.property_dwid = a.property_dwid 
left join masterdata_hk.building b1
on p.building_dwid = b1.building_dwid 
left join masterdata_hk.project_phase c 
on p.project_dwid = c.project_dwid 
and a.phase_name = c.project_phase_display_name 
left join masterdata_hk.building b2
on c.project_phase_dwid = b2.project_phase_dwid 
and b1.building_display_name = b2.building_display_name 
where p.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'-- 976
)
update masterdata_hk.property a
set address_dwid = b.address_dwid, building_dwid = b.building_dwid, project_phase_dwid = b.project_phase_dwid
from base b where a.property_dwid = b.property_dwid
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'

-- 9.update unit_count in building
with unit_count_update_base as (
    select
        building_dwid,
        count(distinct property_dwid) as unit_count
    from masterdata_hk.property a
    where a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
    and building_dwid notnull
    group by 1
)
update masterdata_hk.building a
set unit_count = b.unit_count, residential_unit_count = b.unit_count
from unit_count_update_base b
where a.building_dwid = b.building_dwid
;


-- 10.update txn / listing table, map table
update masterdata_hk.sale_transaction a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;


update masterdata_hk.sale_transaction a
set address_local_text = b.full_address_text
from masterdata_hk.address b
where a.address_dwid = b.address_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;

update masterdata_hk.rent_transaction a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;

update masterdata_hk.sale_listing a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;

update masterdata_hk.rent_listing a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;


update map_hk.midland_sale_txn__map a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;

update map_hk.midland_rent_txn__map a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;

update map_hk.midland_sale_listing__map a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;

update map_hk.midland_rent_listing__map a
set address_dwid = b.address_dwid, 
	building_dwid = b.building_dwid, 
	project_phase_dwid = b.project_phase_dwid
from masterdata_hk.property b
where a.property_dwid = b.property_dwid 
and a.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
and b.project_dwid = '89af1de5215c48850d2f62f0ac6accf3'
;


-- summarize the step and let researcher / intern do the other 14 projects

-- TODO: staging_hk.temp_hk_need_phase_project_0609
-- example: project_dwid = '89af1de5215c48850d2f62f0ac6accf3' -- 'Dragons Range'





-- QA: 
'''
update masterdata_hk.sale_transaction
set property_type_code = 'condo' , tenure_code = 'leasehold'
where project_dwid = '97e1665ab6ade222fbdc414e79e60e41'; -- 1181
update masterdata_hk.rent_listing --sale_listing --rent_transaction 
set property_type_code = 'condo'
where project_dwid = '97e1665ab6ade222fbdc414e79e60e41';
'''

with base as (
SELECT DISTINCT a.unit_id, p.property_dwid, a.phase_name
FROM masterdata_hk.property p
LEFT JOIN masterdata_hk.building b ON p.building_dwid = b.building_dwid and p.project_dwid = '97e1665ab6ade222fbdc414e79e60e41'
left join premap_hk.midland_building_to_dwid mb on mb.building_dwid = b.building_dwid
LEFT JOIN reference.hk_midland_backfill a
ON lower(a.building_name) = lower(mb.building_name)
AND p.address_floor_text = a.floor
AND p.address_stack = a.flat
AND (f_sqft2sqm(a.net_area::float) - 0.01 < p.net_floor_area_sqm OR f_sqft2sqm(a.net_area::float) - 0.01 < p.gross_floor_area_sqm)
AND (f_sqft2sqm(a.net_area::float) + 0.01 > p.net_floor_area_sqm OR f_sqft2sqm(a.net_area::float) + 0.01 > p.gross_floor_area_sqm)
AND a.estate_id = 'E000017049'
--WHERE p.project_dwid = '97e1665ab6ade222fbdc414e79e60e41';
)
select *
from base where property_dwid isnull;



select 
	p.address_dwid = st.address_dwid as addressid_consistency,
	p.building_dwid = st.building_dwid as buildingid_consistency,
	--p.property_dwid = st.property_dwid as propertyid_consistency, 
	p.project_phase_dwid = st.project_phase_dwid as projectphaseid_consistency, 
	count(*)
from masterdata_hk.sale_transaction st
left join masterdata_hk.property p on p.property_dwid = st.property_dwid 
where st.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and p.project_dwid = '97e1665ab6ade222fbdc414e79e60e41'
group by 1,2,3;
-- true	 true	true	1148
-->
'''
false	false	true	38
true	true	true	710
'''

select 
	p.address_dwid = st.address_dwid as addressid_consistency,
	p.building_dwid = st.building_dwid as buildingid_consistency,
	--p.property_dwid = st.property_dwid as propertyid_consistency, 
	p.project_phase_dwid = st.project_phase_dwid as projectphaseid_consistency, 
	count(*)
from masterdata_hk.sale_listing st
left join masterdata_hk.property p on p.property_dwid = st.property_dwid 
where st.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and p.project_dwid = '97e1665ab6ade222fbdc414e79e60e41'
group by 1,2,3;
-- true	 true	true	105

select 
	p.address_dwid = st.address_dwid as addressid_consistency,
	p.building_dwid = st.building_dwid as buildingid_consistency,
	--p.property_dwid = st.property_dwid as propertyid_consistency, 
	p.project_phase_dwid = st.project_phase_dwid as projectphaseid_consistency, 
	count(*)
from masterdata_hk.rent_listing st
left join masterdata_hk.property p on p.property_dwid = st.property_dwid 
where st.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and p.project_dwid = '97e1665ab6ade222fbdc414e79e60e41'
group by 1,2,3;
-- true	 true	true	22


select a.*
from masterdata_hk.sale_transaction a
left join map_hk.midland_sale_txn__map b on a.data_uuid = b.data_uuid 
where md5(
		f_prep_dw_id(a.activity_dwid)||'__'||
		f_prep_dw_id(a.property_dwid)||'__'||
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)||'__'||
		f_prep_dw_id(a.project_phase_dwid)
		) 
	!= md5(
		f_prep_dw_id(b.activity_dwid)||'__'||
		f_prep_dw_id(b.property_dwid)||'__'||
		f_prep_dw_id(b.address_dwid)||'__'||
		f_prep_dw_id(b.building_dwid)||'__'||
		f_prep_dw_id(b.project_dwid)||'__'||
		f_prep_dw_id(b.project_phase_dwid)
		) 
and a.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and b.project_dwid = '97e1665ab6ade222fbdc414e79e60e41'
; -- 53 --0


select a.*, c.*
from masterdata_hk.sale_transaction a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id
where md5(
		f_prep_dw_id(a.property_dwid)||'__'||
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)||'__'||
		f_prep_dw_id(a.project_phase_dwid)
		) 
	!= md5(
		f_prep_dw_id(c.property_dwid)||'__'||
		f_prep_dw_id(c.address_dwid)||'__'||
		f_prep_dw_id(c.building_dwid)||'__'||
		f_prep_dw_id(c.project_dwid)||'__'||
		f_prep_dw_id(c.project_phase_dwid)
		) 
and a.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and b.estate_id = 'E000017049' and c.estate_id = 'E000017049'
; -- 490 -- 0


select a.*, c.*
from masterdata_hk.sale_transaction a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_building_to_dwid c on b.building_id = c.building_id
where md5(
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)||'__'||
		f_prep_dw_id(a.project_phase_dwid)
		) 
	!= md5(
		f_prep_dw_id(c.address_dwid)||'__'||
		f_prep_dw_id(c.building_dwid)||'__'||
		f_prep_dw_id(c.project_dwid)||'__'||
		f_prep_dw_id(c.project_phase_dwid)
		) 
and a.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and b.estate_id = 'E000017049' and c.estate_id = 'E000017049'
; -- 480 --0


select a.*, c.*
from map_hk.midland_sale_txn__map a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id 
where md5(
		f_prep_dw_id(a.property_dwid)||'__'||
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)||'__'||
		f_prep_dw_id(a.project_phase_dwid)
		) 
	!= md5(
		f_prep_dw_id(c.property_dwid)||'__'||
		f_prep_dw_id(c.address_dwid)||'__'||
		f_prep_dw_id(c.building_dwid)||'__'||
		f_prep_dw_id(c.project_dwid)||'__'||
		f_prep_dw_id(c.project_phase_dwid)
		) 
and a.project_dwid = '97e1665ab6ade222fbdc414e79e60e41' and b.estate_id = 'E000017049' and c.estate_id = 'E000017049'
; -- 544 -- 



with idbase as (
	select building_id , building_dwid, address_dwid, project_dwid, project_phase_dwid ,
		ROW_NUMBER() over (PARTITION BY building_id order by building_dwid, address_dwid, project_dwid, project_phase_dwid) AS seq
	from premap_hk.midland_unit_to_dwid
	where estate_id = 'E000017049'
	group by 1,2,3,4,5
)
, base as (
	select building_id , building_dwid, address_dwid, project_dwid, project_phase_dwid
	from idbase where seq = 1
)
select a.*, b.*
from premap_hk.midland_building_to_dwid a
left join base b on a.building_id = b.building_id
where md5(
		f_prep_dw_id(a.address_dwid)||'__'||
		f_prep_dw_id(a.building_dwid)||'__'||
		f_prep_dw_id(a.project_dwid)||'__'||
		f_prep_dw_id(a.project_phase_dwid)
		) 
	!= md5(
		f_prep_dw_id(b.address_dwid)||'__'||
		f_prep_dw_id(b.building_dwid)||'__'||
		f_prep_dw_id(b.project_dwid)||'__'||
		f_prep_dw_id(b.project_phase_dwid)
		) 
and a.estate_id = 'E000017049'
; -- 0 [consistent]



select 
    count(*) as total_records,
    --count(status_code) as status_count,
    count(property_dwid) ,
    (count(property_dwid)*100/count(*)) as properties_mapped,
    count(project_dwid) ,
    (count(project_dwid)*100/count(*)) as projects_mapped,
    count(building_dwid),
    (count(building_dwid)*100/count(*)) as buildings_mapped,
    count(address_dwid),
    (count(address_dwid)*100/count(*)) as addresses_mapped,
    count(project_phase_dwid),
    (count(project_phase_dwid)*100/count(*)) as phases_mapped
from premap_hk.midland_unit_to_dwid mutd 
;-- 387663	(property)358167	92	(project)310148	 80	(building)372513	96	(address)377301	 97	(phase)64520	16

