with lastmodifiedtime_base as (
select b.lastmodifiedtime , count(*)
from map_hk.midland_sale_txn__map a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
group by 1 order by 1 desc limit 1 -- 2022-07-27 06:00:36.355
)
update metadata.md_data_source a
set current_map_datetime = b.lastmodifiedtime
from lastmodifiedtime_base b
where a.data_source_code = 'hk-midland-transaction-sale';

select b.lastmodifiedtime, a.*
from map_hk.midland_rent_txn__map a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
order by 1 desc

select b.creationtime, a.* --b.lastmodifiedtime, a.*
from masterdata_hk.rent_transaction a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
order by 1 desc

select b.lastmodifiedtime, count(*)
from map_hk.midland_rent_txn__map a
left join "source".hk_midland_realty_rental_transaction b on a.data_uuid = b.data_uuid 
group by 1 order by 1 desc;

select b.lastmodifiedtime, count(*)
from "source".hk_midland_realty_rental_transaction b --on a.data_uuid = b.data_uuid 
group by 1 order by 1 desc


update metadata.md_data_source a
set current_map_datetime = '2022-08-11 06:00:39.007'::date
where a.data_source_code = 'hk-midland-transaction-rent';


select b.creationtime , count(*)
from map_hk.midland_sale_txn__map a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
group by 1 order by 1 desc limit 1 -- 2022-06-09 06:00:42.270

select b.creationtime, a.* --b.lastmodifiedtime, a.*
from masterdata_hk.sale_transaction a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where data_source = 'hk-midland-transaction-sale' --b.creationtime notnull
order by 1 desc; --2022-06-09 06:00:42.270


update metadata.md_data_source a
set current_map_datetime = '2022-06-09 06:00:42.270'::date
where a.data_source_code = 'hk-midland-transaction-sale';


select b.creationtime, a.* 
from masterdata_hk.sale_listing a
left join "source".hk_midland_realty_sale_listing b on a.data_uuid = b.data_uuid 
where data_source = 'hk-midland-listing-sale'
order by 1 desc; -- 2022-08-14 06:05:04.365

update metadata.md_data_source a
set current_map_datetime = '2022-08-14 06:05:04.365'::date
where a.data_source_code = 'hk-midland-listing-sale';

select b.creationtime, a.* 
from masterdata_hk.rent_listing a
left join "source".hk_midland_realty_rental_listing b on a.data_uuid = b.data_uuid 
where data_source = 'hk-midland-listing-rent'
order by 1 desc; -- 2022-08-11 06:06:36.570

update metadata.md_data_source a
set current_map_datetime = '2022-08-11 06:06:36.570'::date
where a.data_source_code = 'hk-midland-listing-rent';


select b.creationtime, a.* --b.lastmodifiedtime, a.*
from masterdata_hk.sale_transaction a
left join "source".hk_daily_transaction b on a.data_uuid = b.data_uuid::uuid 
where data_source = 'hk-daily-transaction' 
order by 1 desc; --2022-06-09 06:03:54.629


update metadata.md_data_source a
set current_map_datetime = '2022-06-09 06:03:54.629'::date
where a.data_source_code = 'hk-daily-transaction';



------------------------------------------


with last_map_record as (
    select
        *
    from metadata.md_data_source mds
    where data_source_code = 'hk-midland-transaction-sale'
)
, new_record as (
	select
	    distinct case when lastmodifiedtime > last_map_record.current_map_datetime
	    				and not exists (select 1 from map_hk.midland_sale_txn__map mst where mst.data_uuid = h.data_uuid)
	    			then lastmodifiedtime > last_map_record.current_map_datetime
	    		else false
	    		end
	from "source".hk_midland_realty_sale_transaction h,
	    last_map_record
	where lastmodifiedtime > last_map_record.current_map_datetime

	union
	select
		false
)
select
	*
from new_record
order by 1 desc
limit 1
; -- true



with last_map_record as (
    select
        *
    from metadata.md_data_source mds
    where data_source_code = 'sg-gov-hdb-transaction-sale'
)
, new_record as (
	select
	    distinct case when lastmodifiedtime > last_map_record.current_map_datetime
	    				and not exists (select 1 from map_sg.ura_sale__map usm where usm.data_uuid = s.data_uuid)
	    			then lastmodifiedtime > last_map_record.current_map_datetime
	    		else false
	    		end
	from "source".sg_gov_resale s,
	    last_map_record
	where lastmodifiedtime > last_map_record.current_map_datetime
	union select false
)
select
	*
from new_record
order by 1 desc
limit 1
; -- false


select 
	case when count(a.*) > 0 then true else false end as have_new_records
from "source".sg_gov_resale a
cross join metadata.md_data_source mds
where mds.data_source_code = 'sg-gov-hdb-transaction-sale'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_sg.ura_sale__map usm where usm.data_uuid = a.data_uuid)
; -- false

select 
	case when count(a.*) > 0 then true else false end as have_new_records
from "source".hk_midland_realty_sale_transaction a
cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-transaction-sale'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_sale_txn__map mp where mp.data_uuid = a.data_uuid)
; -- true


create table staging_hk.tmp_new_midland_sale_transaction_records as 
select
    a.*
from "source".hk_midland_realty_sale_transaction a
cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-transaction-sale'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_sale_txn__map mp where mp.data_uuid = a.data_uuid)
; -- 9519





-- done:

create table staging_hk.tmp_new_midland_sale_transaction_records_20220823_1 as
select
   b.*,
   t.property_dwid,
   t.project_dwid,
   t.building_dwid,
   t.address_dwid,
   null as status_code,
   t.id as map_id
from staging_hk.tmp_new_midland_sale_transaction_records b
left join premap_hk.midland_unit_to_dwid t 
	on (b.unit_id = t.unit_id and t.unit_id notnull and b.unit_id notnull) -- 4,277 out of 9519
where t.id notnull 
;

create table staging_hk.tmp_new_midland_sale_transaction_records_20220823_2 as
select
   b.*,
   t.property_dwid,
   t.project_dwid,
   t.building_dwid,
   t.address_dwid,
   null as status_code,
   t.id as map_id
from staging_hk.tmp_new_midland_sale_transaction_records b
left join premap_hk.midland_unit_to_dwid t 
	on (f_prep_dw_id(t.region_name) = f_prep_dw_id(b.region_name)
		and f_prep_dw_id(t.district_name) = f_prep_dw_id(b.district_name)
		and f_prep_dw_id(t.subregion_name) = f_prep_dw_id(b.subregion_name)
		and f_prep_dw_id(t.estate_name) = f_prep_dw_id(b.estate_name)
		and f_prep_dw_id(t.phase_name) = f_prep_dw_id(b.phase_name)
		and f_prep_dw_id(t.building_name) = f_prep_dw_id(b.building_name)
		and f_prep_dw_id(t.floor) = f_prep_dw_id(b.floor)
		and f_prep_dw_id(t.stack) = f_prep_dw_id(b.flat)
		and (t.unit_id notnull or b.unit_id notnull)) -- 3304 out of 9519
where t.id notnull 
;


select distinct data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_1; -- 4277
select distinct data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_2; -- 3299

select data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_1
union
select data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_2; -- 4,270


create table staging_hk.tmp_new_midland_sale_transaction_records_20220823_3 as
select
   b.*,
   t.project_dwid,
   t.building_dwid,
   t.address_dwid,
   null as status_code,
   t.id as map_id
from staging_hk.tmp_new_midland_sale_transaction_records b
	left join premap_hk.midland_building_to_dwid t 
		on (b.building_id = t.building_id and t.building_id notnull and b.building_id notnull) -- 7564 out of 9519
where t.id notnull 
;


create table staging_hk.tmp_new_midland_sale_transaction_records_20220823_4 as
select
   b.*,
   t.project_dwid,
   t.building_dwid,
   t.address_dwid,
   null as status_code,
   t.id as map_id
from staging_hk.tmp_new_midland_sale_transaction_records b
	left join premap_hk.midland_building_to_dwid t 
		on (f_prep_dw_id(t.region_name) = f_prep_dw_id(b.region_name)
		and f_prep_dw_id(t.district_name) = f_prep_dw_id(b.district_name)
		and f_prep_dw_id(t.subregion_name) = f_prep_dw_id(b.subregion_name)
		and f_prep_dw_id(t.estate_name) = f_prep_dw_id(b.estate_name)
		and f_prep_dw_id(t.phase_name) = f_prep_dw_id(b.phase_name)
		and f_prep_dw_id(t.building_name) = f_prep_dw_id(b.building_name)
		and (t.building_id notnull or b.building_id notnull))-- 7132 out of 9519
where t.id notnull 
;

select distinct data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_3; -- 7,563
select distinct data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_4; -- 7,108

select data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_3
union
select data_uuid from staging_hk.tmp_new_midland_sale_transaction_records_20220823_4; -- 7,566



create table staging_hk.tmp_new_midland_sale_transaction_records_20220823 as
select
   b.*,
   tu.property_dwid,
   tb.project_dwid,
   tb.building_dwid,
   tb.address_dwid,
   null as status_code,
   tb.id as map_id
from staging_hk.tmp_new_midland_sale_transaction_records b
	left join premap_hk.midland_building_to_dwid tb 
		on (b.building_id = tb.building_id and tb.building_id notnull and b.building_id notnull) -- 7,581 out of 9,537
	left join premap_hk.midland_unit_to_dwid tu 
	on (b.unit_id = tu.unit_id and tu.unit_id notnull and b.unit_id notnull) -- 3,427 out of 9,537
;

-- done:
select p.property_dwid as new_property_dwid, st.*
from staging_hk.tmp_new_midland_sale_transaction_records_20220823 st
left join masterdata_hk.property p 
on f_prep_dw_id(st.project_dwid) = f_prep_dw_id(p.project_dwid)
and f_prep_dw_id(st.building_dwid) = f_prep_dw_id(p.building_dwid)
and f_prep_dw_id(st.address_dwid) = f_prep_dw_id(p.address_dwid)
and f_prep_dw_id(st.floor) = f_prep_dw_id(p.address_floor_text)
and f_prep_dw_id(st.flat) = f_prep_dw_id(p.address_stack)
where st.property_dwid isnull and st.map_id notnull
; -- 1781 out of 4,167


with base as (
select p.property_dwid as new_property_dwid, st.data_uuid 
from staging_hk.tmp_new_midland_sale_transaction_records_20220823 st
left join masterdata_hk.property p 
on f_prep_dw_id(st.project_dwid) = f_prep_dw_id(p.project_dwid)
and f_prep_dw_id(st.building_dwid) = f_prep_dw_id(p.building_dwid)
and f_prep_dw_id(st.address_dwid) = f_prep_dw_id(p.address_dwid)
and f_prep_dw_id(st.floor) = f_prep_dw_id(p.address_floor_text)
and f_prep_dw_id(st.flat) = f_prep_dw_id(p.address_stack)
where st.property_dwid isnull and st.map_id notnull and p.property_dwid notnull -- 1781
)
update staging_hk.tmp_new_midland_sale_transaction_records_20220823 a
set property_dwid = b.new_property_dwid
from base b where a.data_uuid = b.data_uuid
and exists (select 1 from base b where a.data_uuid = b.data_uuid); --1768

--------------
-- preparation:

select a.*, p.project_dwid 
from staging_hk.tmp_hk_public_housing a 
left join masterdata_hk.project p on lower(a.estate_name) = lower(p.project_name_text)


update staging_hk.tmp_hk_public_housing a
set project_dwid = p.project_dwid 
from masterdata_hk.project p where lower(a.estate_name) = lower(p.project_name_text)


select a.*, b.building_dwid 
from staging_hk.tmp_hk_public_housing a 
left join masterdata_hk.building b on a.project_dwid = b.project_dwid 
and lower(a.building_name) = lower(b.building_name_text)
where a.project_dwid notnull; -- 1219

update staging_hk.tmp_hk_public_housing a 
set building_dwid = b.building_dwid
from masterdata_hk.building b where a.project_dwid = b.project_dwid and lower(a.building_name) = lower(b.building_name_text)
and a.project_dwid notnull;


select a.*, b.building_dwid , ad.address_dwid 
from staging_hk.tmp_hk_public_housing a 
left join masterdata_hk.building b on lower(a.building_name) = lower(b.building_name_text)
left join masterdata_hk.address ad on b.address_dwid = ad.address_dwid and lower(a.city) = lower(ad.city) and lower(a.city_area) = lower(ad.city_area)
where a.project_dwid isnull and a.building_name notnull; -- 1219


select f_clone_table('staging_hk', 'tmp_hk_public_housing', 'feature_hk', 'de__building__public_housing', TRUE, TRUE);







--------------
select metadata.fn_create_change_request(
    'hk-new-hk_midland_sale_transaction-records-2022-08-24',
    'pipeline',
    'pipeline'
);

call metadata.sp_add_change_table(
    currval('metadata.cr_id_seq')::int,
    'hk',
    replace('sale_transaction', '-', '_')
);


create table staging_hk.hk_midland_sale_transaction_id_table as
select
    id
from metadata.change_request cr
where title = 'hk-new-hk_midland_sale_transaction-records-2022-08-24'
    and author = 'pipeline'
    and requester = 'pipeline'
order by id desc
limit 1;


select
    id
from staging_hk.hk_midland_sale_transaction_id_table; -- 561



insert into branch_hk.sale_transaction_cr_561
(
	property_dwid,
	address_dwid,building_dwid,project_dwid,
	activity_name,units_sold,sale_type,property_completion_year,property_type_code,
	address_unit,address_local_text,tenure_code,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,
	contract_date,settlement_date,purchase_amount,country_code,data_uuid,data_source,cr_record_action
)
-- need to use raw data to prevent infomation loss, if we have some info not consistent with current core table data, then we could use them to update or check core table
select
	s.property_dwid,
	s.address_dwid,s.building_dwid,s.project_dwid,
	null as activity_name, -- but rent_txn has notnull activity_name
	1::int as units_sold,
	case when s.mkt_type = '1' then 'new-sale' else 'resale' end as sale_type,
	date_part('year', s.building_first_op_date::date) as property_completion_year, --ci.completion_year as property_completion_year, 
	-- use raw data and update de__building__completion_year_info feature table 
	-- need to update de__building__completion_year_info using midland building_first_op_date first; also need to add update process into ingestion 
	case when p.property_type_code isnull and s.bedroom notnull and ph.city notnull then 'hos'
		when p.property_type_code isnull and s.bedroom notnull and ph.city isnull then 'condo'
		when p.property_type_code isnull and s.bedroom isnull then 'comm'
		else p.property_type_code
		end as property_type_code, -- (x)need to be consistent with property_dwid
	s.floor||'-'||s.flat as address_unit, --case when p.address_unit isnull then s.floor||'-'||s.flat else p.address_unit end as address_unit,
	-- (v)use raw data or match property_dwid first? discuss with willy 
	--> (x)use unit_id to join property_dwid first, coalesce info from property table and raw data, use raw data to match property_dwid for 2nd round for those without property_dwid, need to update property related columns as well like property_type_code etc. 
	--> (v)use unit_id to join property_dwid first, then use floor, stack to join property_dwid for 2nd round, then we can directly join with property table to get property info here
	a.full_address_text as address_local_text,
	'leasehold' as tenure_code,
	--p.bathroom_count,p.bedroom_count,p.gross_floor_area_sqm,p.net_floor_area_sqm,
	p.bathroom_count, -- txn source don't have bathroom info
	coalesce(s.bedroom, p.bedroom_count) as bedroom_count, -- remain its raw data
	coalesce(s.area_sqm,p.gross_floor_area_sqm)::float as gross_floor_area_sqm,
    coalesce(s.net_area_sqm,p.net_floor_area_sqm)::float as net_floor_area_sqm,
	s.tx_date as contract_date,
	s.update_date as settlement_date,
	s.price as purchase_amount,
	'cn' as country_code,
	s.data_uuid,
	'hk-midland-transaction-sale' as data_source,
	'insert' as cr_record_action
from staging_hk.tmp_new_midland_sale_transaction_records_20220823 s
--left join feature_hk.de__building__completion_year_info ci on s.building_dwid = ci.building_dwid 
left join feature_hk.de__building__public_housing ph on f_prep_dw_id(s.project_dwid) = f_prep_dw_id(ph.project_dwid) 
and f_prep_dw_id(s.building_dwid) = f_prep_dw_id(ph.building_dwid) and s.building_dwid||s.project_dwid notnull
--coalesce(s.project_dwid,'')||coalesce(s.building_dwid,'') = coalesce(ph.project_dwid,'')||coalesce(ph.project_dwid,'') 
left join masterdata_hk.property p on s.property_dwid = p.property_dwid 
left join masterdata_hk.address a on s.address_dwid = a.address_dwid 
--left join masterdata_hk.project pj on s.project_dwid = pj.project_dwid 
; -- 9537



update branch_hk.sale_transaction_cr_561
set activity_dwid = api.get_dwid(country_code, 'sale_transaction', id)
;


call metadata.sp_submit_change_request(561, 'pipeline');

call metadata.sp_approve_change_request(561, 'pipeline');

call metadata.sp_merge_change_request(561);



select settlement_date , count(*)
from masterdata_hk.sale_transaction st 
group by 1 order by 1 desc;
-- 2022-06-10 --> 2022-08-22




-- need to update premap, map and property(?) tables as well


-- map table 

select data_uuid , count(*)
from masterdata_hk.sale_transaction st 
group by 1 having count(*) > 1; --19

data_uuid in ('16679e6b-47e5-4228-ade2-b832e135efb1', '1f63d028-85d7-4564-b79f-38cfa1a76ce8', '207905d4-73ff-46d4-8e00-99660ff39c90', 
'23b0a2cc-7a50-430c-b5bc-9839b3490702', '4d904c14-ffd1-4156-b100-eac03ede41c0', '6241a309-bf19-4628-a2c7-32ca9c3efbf1', '69550d14-1d5d-40f7-83ec-e85d776c90be', 
'781ade27-b024-430b-82e7-8326b5408fd1', '8e11ede5-7e62-4103-a4e9-e6efaef562b4', '970ba885-40d8-431a-a690-5cd825e315b5', '9a41125c-7dfa-4e00-b7ca-5d9020480936', 
'a5f99ef0-f0c8-46d6-bbe5-e1b1d3cb5701', 'b60cbc2c-cfcc-4d5a-8fc9-d5098d48175d', 'c531fe6b-808f-4b34-b714-80feb4b7b821', 'd1ba8755-bc23-4aa0-a99d-dbeab01cf7bd', 
'd96bf6a7-78ca-41aa-ad12-1ce1faedfad0', 'da5f3650-3d62-4f75-a42d-1e933bad7753', 'ee3392a2-f502-478d-b6aa-12226236497d')




insert into map_hk.midland_sale_txn__map 
(
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
select distinct
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from branch_hk.sale_transaction_cr_561 a
; --9519



with base as (
    select
        max(t.lastmodifiedtime) as new_map_datetime
    from staging_hk.tmp_new_midland_sale_transaction_records_20220823 t
)
update metadata.md_data_source mds
set current_map_datetime = base.new_map_datetime
from base
where mds.data_source_code = 'hk-midland-transaction-sale'
;


-- feature table: use buildind_dwid, property_completion_year in sale_transaction table to update, need to check whether we have one building with multi completion year problem
---- de__building__completion_year_info

with base as (
select 
	a.building_dwid , b.building_id ,
	a.property_completion_year as old_completion_year, 
	date_part('year', b.building_first_op_date::date) as raw_completion_year,
	a.property_completion_year = date_part('year', b.building_first_op_date::date) as same_year,
	row_number() over (partition by a.building_dwid order by a.property_completion_year) as seq
from masterdata_hk.sale_transaction a 
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
where a.data_source = 'hk-midland-transaction-sale'
group by 1,2,3,4 order by 1 -- 36,056
)
, checklist as (
select building_dwid 
from base
where seq > 1 or same_year = false or same_year isnull
)
select * from base 
where exists (select 1 from checklist where checklist.building_dwid = base.building_dwid)
; -- 21,121




with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull -- 3555
)
select a.*, b.property_completion_year as new_completion_year, a.completion_year = b.property_completion_year::text
from feature_hk.de__building__completion_year_info a
left join base b on a.building_dwid = b.building_dwid
where b.building_dwid notnull; --2702


with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull
)
select b.*
from base b
where not exists (select 1 from feature_hk.de__building__completion_year_info a where a.building_dwid = b.building_dwid)
; -- 853

with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull and building_dwid notnull
)
insert into feature_hk.de__building__completion_year_info
(
	building_dwid,construction_start,construction_end,completion_year
)
select b.building_dwid, null, b.property_completion_year, b.property_completion_year
from base b
where not exists (select 1 from feature_hk.de__building__completion_year_info a where a.building_dwid = b.building_dwid)
; -- 782




with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull
)
, cr_completion_year as (
select a.*, b.property_completion_year as new_completion_year
from feature_hk.de__building__completion_year_info a
left join base b on a.building_dwid = b.building_dwid
where a.completion_year != b.property_completion_year::text --146
or (a.completion_year isnull and b.property_completion_year notnull) -- 146
)
--select bd.building_dwid, bd.construction_end_year , c.new_completion_year
--from masterdata_hk.building bd
--left join cr_completion_year c on bd.building_dwid = c.building_dwid
--where c.building_dwid notnull
update masterdata_hk.building bd
set construction_end_year = c.new_completion_year
from cr_completion_year c where bd.building_dwid = c.building_dwid
and exists (select 1 from cr_completion_year c where bd.building_dwid = c.building_dwid); -- 146



with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull
)
, cr_completion_year as (
select a.*, b.property_completion_year as new_completion_year
from feature_hk.de__building__completion_year_info a
left join base b on a.building_dwid = b.building_dwid
where a.completion_year != b.property_completion_year::text --146
or (a.completion_year isnull and b.property_completion_year notnull) -- 146
)
update masterdata_hk.sale_transaction bd
set property_completion_year = c.new_completion_year
from cr_completion_year c where bd.building_dwid = c.building_dwid
and exists (select 1 from cr_completion_year c where bd.building_dwid = c.building_dwid);


with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull
)
, cr_completion_year as (
select a.*, b.property_completion_year as new_completion_year
from feature_hk.de__building__completion_year_info a
left join base b on a.building_dwid = b.building_dwid
where a.completion_year != b.property_completion_year::text --146
or (a.completion_year isnull and b.property_completion_year notnull) -- 146
)
update masterdata_hk.sale_listing bd
set property_completion_year = c.new_completion_year
from cr_completion_year c where bd.building_dwid = c.building_dwid
and exists (select 1 from cr_completion_year c where bd.building_dwid = c.building_dwid);


with base as (
    select distinct
        building_dwid, property_completion_year
    from branch_hk.sale_transaction_cr_561 t
    where property_completion_year notnull
)
, cr_completion_year as (
select a.*, b.property_completion_year as new_completion_year
from feature_hk.de__building__completion_year_info a
left join base b on a.building_dwid = b.building_dwid
where a.completion_year != b.property_completion_year::text --146
or (a.completion_year isnull and b.property_completion_year notnull) -- 146
)
update feature_hk.de__building__completion_year_info bd
set construction_end = c.new_completion_year, completion_year = c.new_completion_year
from cr_completion_year c where bd.building_dwid = c.building_dwid
and exists (select 1 from cr_completion_year c where bd.building_dwid = c.building_dwid);



-- to do:

-- premap table
---- midland_unit_to_dwid
---- midland_building_to_dwid

select b.unit_id, b.building_id , a.*
from masterdata_hk.sale_transaction a
left join "source".hk_midland_realty_sale_transaction b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id  
where c.unit_id isnull and b.lastmodifiedtime > '2022-06-09'; -- 5261
-- or use unique key columns
branch_hk.sale_transaction_cr_561


select b.unit_id, b.building_id , a.*
from masterdata_hk.sale_transaction a
left join staging_hk.tmp_new_midland_sale_transaction_records_20220823 b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id  
where c.unit_id isnull and b.id notnull -- 5260




create table branch_hk.midland_unit_to_dwid_cr_29 as
select
	distinct
	null as id,b.region_name,b.region_id,b.district_name,b.district_id,b.subregion_name,b.subregion_id,
	b.estate_name,b.estate_id,b.phase_name,b.phase_id,b.building_name,b.building_id,
	d.corrected_street_num as address_number, d.corrected_street_name as address_street, 
	b.floor,b.flat as stack, b.unit_id,
	a.property_dwid,a.building_dwid,a.address_dwid,a.project_dwid,a.lot_group_dwid, 
	'insert' as cr_record_action
from masterdata_hk.sale_transaction a
left join staging_hk.tmp_new_midland_sale_transaction_records_20220823 b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id  
left join reference.hk_midland_hkpost_correction_backfill d on b.building_id = d.building_id 
where c.unit_id isnull and b.id notnull; -- 5090

insert into premap_hk.midland_unit_to_dwid
(
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,phase_name,phase_id,
	building_name,building_id,address_number,address_street,floor,stack,unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid
)
with change_request as (
select
	distinct
	null as id,b.region_name,b.region_id,b.district_name,b.district_id,b.subregion_name,b.subregion_id,
	b.estate_name,b.estate_id,b.phase_name,b.phase_id,b.building_name,b.building_id,
	d.corrected_street_num as address_number, d.corrected_street_name as address_street, 
	b.floor,b.flat as stack, b.unit_id,
	a.property_dwid,a.building_dwid,a.address_dwid,a.project_dwid,a.lot_group_dwid, 
	'insert' as cr_record_action
from masterdata_hk.sale_transaction a
left join staging_hk.tmp_new_midland_sale_transaction_records_20220823 b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_unit_to_dwid c on b.unit_id = c.unit_id  
left join reference.hk_midland_hkpost_correction_backfill d on b.building_id = d.building_id 
where c.unit_id isnull and b.id notnull
)
, base1 as(
select 
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,phase_name,phase_id,
	building_name,building_id,address_number,address_street,floor,stack,unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid,
	row_number() over (partition by region_name, district_name, subregion_name, estate_name, phase_name, building_name, floor, stack order by project_dwid) as seq
from change_request
where cr_record_action = 'insert'
)
select 
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,phase_name,phase_id,
	building_name,building_id,address_number,address_street,floor,stack,unit_id,property_dwid,building_dwid,address_dwid,project_dwid,lot_group_dwid
from base1 where seq = 1; -- 5071





create table midland_building_to_dwid_cr_15 as
select distinct
	null as id,b.region_name,b.region_id,b.district_name,b.district_id,b.subregion_name,b.subregion_id,
	b.estate_name,b.estate_id,b.phase_name,b.phase_id,b.building_name,b.building_id,
	d.corrected_street_num as address_number, d.corrected_street_name as address_street, 
	a.building_dwid,a.address_dwid,a.project_dwid,a.lot_group_dwid,
	'insert' as cr_record_action
from masterdata_hk.sale_transaction a
left join staging_hk.tmp_new_midland_sale_transaction_records_20220823 b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_building_to_dwid c on b.building_id = c.building_id  
left join reference.hk_midland_hkpost_correction_backfill d on b.building_id = d.building_id 
where c.building_id isnull and b.id notnull; -- 201



insert into premap_hk.midland_building_to_dwid
(
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,
	building_dwid,address_dwid,project_dwid,lot_group_dwid
)
with change_request as (
select distinct
	null as id,b.region_name,b.region_id,b.district_name,b.district_id,b.subregion_name,b.subregion_id,
	b.estate_name,b.estate_id,b.phase_name,b.phase_id,b.building_name,b.building_id,
	d.corrected_street_num as address_number, d.corrected_street_name as address_street, 
	a.building_dwid,a.address_dwid,a.project_dwid,a.lot_group_dwid,
	'insert' as cr_record_action
from masterdata_hk.sale_transaction a
left join staging_hk.tmp_new_midland_sale_transaction_records_20220823 b on a.data_uuid = b.data_uuid 
left join premap_hk.midland_building_to_dwid c on b.building_id = c.building_id  
left join reference.hk_midland_hkpost_correction_backfill d on b.building_id = d.building_id 
where c.building_id isnull and b.id notnull
)
select distinct 
	region_name,region_id,district_name,district_id,subregion_name,subregion_id,estate_name,estate_id,
	phase_name,phase_id,building_name,building_id,address_number,address_street,
	building_dwid,address_dwid,project_dwid,lot_group_dwid
from change_request
where cr_record_action = 'insert'; --201



--ALTER TABLE premap_hk.midland_building_to_dwid ADD CONSTRAINT midland_building_to_dwid_region_name_district_name_subregio_key UNIQUE (region_name, district_name, subregion_name, estate_name, phase_name, building_name, building_id)

-- property table?
---- use property_dwid in sale_transaction table to update bedroom_count, gross_floor_area_sqm, net_floor_area_sqm if they are NULL in property table

select p.bedroom_count notnull, st.bedroom_count notnull, count(*)
from masterdata_hk.property p 
left join masterdata_hk.sale_transaction st on p.property_dwid = st.property_dwid 
where st.property_dwid notnull 
group by 1,2;
'''
false	false	86795
false	true	4215 --> 0
true	false	695 --> 0
true	true	1347226
'''


select p.*, st.*
from masterdata_hk.property p 
left join masterdata_hk.sale_transaction st on p.property_dwid = st.property_dwid 
where st.property_dwid notnull and p.bedroom_count notnull and st.bedroom_count isnull



select metadata.fn_create_change_request(
    'hk-update-bedroom-count-in-sale-transaction-2022-08-25', 'huying','huying'
);-- 567

call metadata.sp_add_change_table(567::int, 'hk', replace('sale_transaction', '-', '_'));

drop table  branch_hk.sale_transaction_cr_567;

insert into branch_hk.sale_transaction_cr_567
select
	st.id,st.activity_dwid,st.property_dwid,st.address_dwid,st.building_dwid,st.project_dwid,st.lot_group_dwid,
	current_lot_group_dwid,activity_name,units_sold,sale_type,sale_subtype,property_completion_year,
	st.property_type_code,st.address_unit,
	st.address_local_text,st.tenure_code,st.bathroom_count,
	p.bedroom_count,
	coalesce(st.gross_floor_area_sqm, p.gross_floor_area_sqm) as gross_floor_area_sqm,
	coalesce(st.net_floor_area_sqm, p.net_floor_area_sqm) as net_floor_area_sqm,
	st.land_area_sqm,contract_date,settlement_date,purchase_amount,
	st.country_code,st.data_uuid,st.data_source_uuid,st.data_source, null as address_lot_number, 'update' as cr_record_action
from masterdata_hk.sale_transaction st 
left join masterdata_hk.property p on p.property_dwid = st.property_dwid 
where st.property_dwid notnull and p.bedroom_count notnull and st.bedroom_count isnull; -- 695

call metadata.sp_submit_change_request(567, 'huying');

call metadata.sp_approve_change_request(567, 'huying');

call metadata.sp_merge_change_request(567);


---

select metadata.fn_create_change_request(
    'hk-update-bedroom-count-in-property-2022-08-25', 'pipeline','pipeline'
);-- 568


call metadata.sp_add_change_table(currval('metadata.cr_id_seq')::int, 'hk', replace('property', '-', '_'));


insert into branch_hk.property_cr_568
select distinct 
	p.id,p.property_dwid,p.address_dwid,p.building_dwid,p.unit_group_dwid,p.project_dwid,p.property_type_code,
	p.property_name,p.address_unit,p.address_floor_text,p.address_floor_num,p.address_stack,p.address_stack_num,
	ownership_type_code,st.bedroom_count,p.bathroom_count,p.other_room_count,p.net_floor_area_sqm,p.gross_floor_area_sqm,
	p.slug,p.country_code,p.is_active,property_display_text,p.data_source,p.data_source_id,p.status_code,'update' as cr_record_action
from masterdata_hk.property p
left join masterdata_hk.sale_transaction st on p.property_dwid = st.property_dwid
where st.property_dwid notnull and p.bedroom_count isnull and st.bedroom_count notnull; -- 3808


call metadata.sp_submit_change_request(568, 'huying');

call metadata.sp_approve_change_request(568, 'huying');

call metadata.sp_merge_change_request(568);


with bedroom_count_cr as (
select p.property_dwid, st.bedroom_count
from masterdata_hk.property p
left join masterdata_hk.sale_transaction st on p.property_dwid = st.property_dwid
where st.property_dwid notnull and p.bedroom_count isnull and st.bedroom_count notnull
group by 1, 2 -- 3808
)
update masterdata_hk.property p 
set bedroom_count = st.bedroom_count
from bedroom_count_cr st where p.property_dwid = st.property_dwid
and exists (select 1 from bedroom_count_cr st where p.property_dwid = st.property_dwid)
; -- 3808


with change_request as (
select p.property_dwid, st.gross_floor_area_sqm
from masterdata_hk.property p
left join masterdata_hk.sale_transaction st on p.property_dwid = st.property_dwid
where st.property_dwid notnull and p.gross_floor_area_sqm isnull and st.gross_floor_area_sqm notnull and st.gross_floor_area_sqm != 0
group by 1, 2 -- 313
)
update masterdata_hk.property p 
set gross_floor_area_sqm = st.gross_floor_area_sqm
from change_request st where p.property_dwid = st.property_dwid
and exists (select 1 from change_request st where p.property_dwid = st.property_dwid)
;

with change_request as (
select p.property_dwid, st.net_floor_area_sqm 
from masterdata_hk.property p
left join masterdata_hk.sale_transaction st on p.property_dwid = st.property_dwid
where st.property_dwid notnull and p.net_floor_area_sqm isnull and st.net_floor_area_sqm notnull and st.net_floor_area_sqm != 0
group by 1, 2 -- 760
)
update masterdata_hk.property p 
set net_floor_area_sqm = st.net_floor_area_sqm
from change_request st where p.property_dwid = st.property_dwid
and exists (select 1 from change_request st where p.property_dwid = st.property_dwid)
;




------------------------------------------------------------------

create table staging_hk.hk_midland_rent_transaction_id_table as
select 1 as id;

create table staging_hk.hk_midland_rent_listing_id_table as
select 1 as id;

create table staging_hk.hk_midland_sale_listing_id_table as
select 1 as id;      
      
     

drop table if exists staging_hk.tmp_new_midland_rent_transaction_records;

-- get the latest transactions not exists in current rent_transaction table
create table staging_hk.tmp_new_midland_rent_transaction_records as
select
    a.*
from "source".hk_midland_realty_rental_transaction a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-transaction-rent'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_rent_txn__map mp where mp.data_uuid = a.data_uuid)
; -- 1122


create table staging_hk.tmp_new_midland_rent_transaction_records_20220826 as
select
   b.*,
   tu.property_dwid,
   tb.project_dwid,
   tb.building_dwid,
   tb.address_dwid,
   null as status_code,
   tb.id as map_id
from staging_hk.tmp_new_midland_rent_transaction_records b
	left join premap_hk.midland_building_to_dwid tb
		on b.building_id = tb.building_id
		    and tb.building_id notnull
		    and b.building_id notnull
	left join premap_hk.midland_unit_to_dwid tu
	    on b.unit_id = tu.unit_id
		    and tu.unit_id notnull
		    and b.unit_id notnull
;


-- to do:
with midland_units as (
    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
    from reference.hk_midland_backfill
    where unit_id notnull and floor notnull
)
--, base as (
    select
        p.property_dwid as new_property_dwid,
        st.data_uuid
    from staging_hk.tmp_new_midland_rent_transaction_records_20220826 st
    	left join midland_units mu on st.unit_id = mu.unit_id and mu.rn = 1
        left join masterdata_hk.property p
            on f_prep_dw_id(st.project_dwid) = f_prep_dw_id(p.project_dwid)
            and f_prep_dw_id(st.building_dwid) = f_prep_dw_id(p.building_dwid)
            and f_prep_dw_id(st.address_dwid) = f_prep_dw_id(p.address_dwid)
            and f_prep_dw_id(mu.floor) = f_prep_dw_id(p.address_floor_text)
            and f_prep_dw_id(st.flat) = f_prep_dw_id(p.address_stack)
    where st.property_dwid isnull and st.map_id notnull and p.property_dwid notnull
    group by 1, 2
)
update staging_hk.tmp_new_midland_rent_transaction_records_20220826 a
set property_dwid = b.new_property_dwid
from base b where a.data_uuid = b.data_uuid
and exists (select 1 from base b where a.data_uuid = b.data_uuid)
;



insert into branch_hk.rent_transaction_cr_571
(
	property_dwid,address_dwid,building_dwid,project_dwid,
	activity_name,rent_type,property_type_code,property_subtype,address_unit,bathroom_count,bedroom_count,gross_floor_area_sqm,net_floor_area_sqm,
	rent_start_date,rent_amount_monthly,country_code,data_uuid,data_source,cr_record_action
)


with midland_units as (
    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
    from reference.hk_midland_backfill
    where unit_id notnull and floor notnull
)
,base as (
select
	s.property_dwid,
	s.address_dwid,
    s.building_dwid,
    s.project_dwid,
    'rental'::text as rent_type,
    case when p.property_type_code isnull and s.bedroom notnull and ph.city notnull then 'hos'
		when p.property_type_code isnull and s.bedroom notnull and ph.city isnull then 'condo'
		when p.property_type_code isnull and s.bedroom isnull then 'comm'
		else p.property_type_code
		end as property_type_code,
	s.estate_name, 
	s.building_name,
    mu.floor || '-' || s.flat as address_unit,
    p.bathroom_count,
	coalesce(s.bedroom, p.bedroom_count) as bedroom_count,
	coalesce(s.area_sqm, p.gross_floor_area_sqm)::float as gross_floor_area_sqm,
    coalesce(s.net_area_sqm, p.net_floor_area_sqm)::float as net_floor_area_sqm,
    tx_date::date as rent_start_date,
    price::float as rent_amount_monthly,
   	'cn' as country_code,
	s.data_uuid::uuid as data_uuid,
	'hk-midland-transaction-rent' as data_source,
	'insert' as cr_record_action
from staging_hk.tmp_new_midland_rent_transaction_records_20220826 s
    left join feature_hk.de__building__public_housing ph
        on f_prep_dw_id(s.project_dwid) = f_prep_dw_id(ph.project_dwid)
            and f_prep_dw_id(s.building_dwid) = f_prep_dw_id(ph.building_dwid)
            and s.building_dwid || s.project_dwid notnull
    left join midland_units mu 
    	on s.unit_id = mu.unit_id and mu.rn = 1
    left join masterdata_hk.property p
        on s.property_dwid = p.property_dwid
)
select 
	s.property_dwid,
	s.address_dwid,
    s.building_dwid,
    s.project_dwid,
    lower(s.property_type_code || ' ' || s.bedroom_count || '-Rm at ' || coalesce(s.estate_name, s.building_name)) as activity_name,
    --lower(s.property_type_code || ' ' || s.bedroom_count || '-Rm at ' || coalesce(pj.project_name_text , bd.building_name_text)) as activity_name,
    rent_type,
    s.property_type_code,
    s.bedroom_count || '-Rm' as property_subtype,
    s.address_unit,
    s.bathroom_count,
    s.bedroom_count,
    s.gross_floor_area_sqm,
    s.net_floor_area_sqm,
    rent_start_date,
    rent_amount_monthly,
    country_code,
    data_uuid,
    data_source,
    cr_record_action
from base s
--left join masterdata_hk.project pj on s.project_dwid = pj.project_dwid 
--left join masterdata_hk.building bd on s.building_dwid = bd.building_dwid 
;


update branch_hk.rent_transaction_cr_571
set activity_dwid = api.get_dwid(country_code, 'rent_transaction', id)
;


call metadata.sp_submit_change_request(571, 'pipeline');

call metadata.sp_approve_change_request(571, 'pipeline');

call metadata.sp_merge_change_request(571);


select data_uuid , count(*)
from masterdata_hk.rent_transaction rt 
group by 1 having count(*) > 1;

select data_uuid , count(*)
from branch_hk.rent_transaction_cr_571
group by 1 having count(*) > 1;

with base as (
select distinct 
	s.property_dwid,
	s.address_dwid,
    s.building_dwid,
    s.project_dwid,
    activity_name,
    rent_type,
    s.property_type_code,
    property_subtype,
    s.address_unit,
    s.bathroom_count,
    s.bedroom_count,
    s.gross_floor_area_sqm,
    s.net_floor_area_sqm,
    rent_start_date,
    rent_amount_monthly,
    country_code,
    data_uuid,
    data_source,
    cr_record_action
from branch_hk.rent_transaction_cr_571 s
)
select data_uuid , count(*)
from base
group by 1 having count(*) > 1;

-- dedup data_uuid

-- add unique key for data_uuid if there is no NULL data in this column 
-- ALTER TABLE masterdata_hk.rent_transaction ADD CONSTRAINT rent_transaction_un_data_uuid UNIQUE (data_uuid);
--> some records from sources other than 'midland' may not have data_uuid so this column could be NULLable, add unique key for NULLable column is useless;


insert into map_hk.midland_rent_txn__map
(
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
select
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from branch_hk.rent_transaction_cr_571
;

with base as (
    select
        max(t.lastmodifiedtime) as new_map_datetime
    from staging_hk.tmp_new_midland_rent_transaction_records_20220826 t
)
update metadata.md_data_source mds
set current_map_datetime = base.new_map_datetime
from base
where mds.data_source_code = 'hk-midland-transaction-rent'
;


-- check:
select a.*
from "source".hk_midland_realty_rental_transaction a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-transaction-rent'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_rent_txn__map mp where mp.data_uuid = a.data_uuid)
;



insert into premap_hk.midland_unit_to_dwid
(
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
    phase_name,
    phase_id,
	building_name,
    building_id,
    address_number,
    address_street,
    floor,
    stack,
    unit_id,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
with change_request as (
    select distinct
        null as id,
        b.region_name,
        b.region_id,
        b.district_name,
        b.district_id,
        b.subregion_name,
        b.subregion_id,
        b.estate_name,
        b.estate_id,
        b.phase_name,
        b.phase_id,
        b.building_name,
        b.building_id,
        d.corrected_street_num as address_number,
        d.corrected_street_name as address_street,
        split_part(a.address_unit, '-', 1)  as floor,
        b.flat as stack,
        b.unit_id,
        a.property_dwid,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        a.lot_group_dwid,
        'insert' as cr_record_action
    from branch_hk.rent_transaction_cr_571 a -- new!!
        left join staging_hk.tmp_new_midland_rent_transaction_records_20220826 b
            on a.data_uuid = b.data_uuid
        left join premap_hk.midland_unit_to_dwid c
            on b.unit_id = c.unit_id
        left join reference.hk_midland_hkpost_correction_backfill d
            on b.building_id = d.building_id
    where c.unit_id isnull and b.id notnull
)
, change_request_seq as(
    select
        *,
        row_number() over (partition by region_name, district_name, subregion_name, estate_name,
                            phase_name, building_name, building_id, floor, stack order by project_dwid) as seq
    from change_request
    where cr_record_action = 'insert'
)
select
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
    phase_name,
    phase_id,
	building_name,
    building_id,
    address_number,
    address_street,
    floor,
    stack,
    unit_id,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from change_request_seq
where seq = 1; -- 172



insert into premap_hk.midland_building_to_dwid
(
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
	phase_name,
    phase_id,
    building_name,
    building_id,
    address_number,
    address_street,
	building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
with change_request as (
    select distinct
        b.region_name,
        b.region_id,
        b.district_name,
        b.district_id,
        b.subregion_name,
        b.subregion_id,
        b.estate_name,
        b.estate_id,
        b.phase_name,
        b.phase_id,
        b.building_name,
        b.building_id,
        d.corrected_street_num as address_number,
        d.corrected_street_name as address_street,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        a.lot_group_dwid,
        'insert' as cr_record_action
    from branch_hk.rent_transaction_cr_571 a -- new!!
        left join staging_hk.tmp_new_midland_rent_transaction_records_20220826 b
            on a.data_uuid = b.data_uuid
        left join premap_hk.midland_building_to_dwid c
            on b.building_id = c.building_id
        left join reference.hk_midland_hkpost_correction_backfill d
            on b.building_id = d.building_id
    where c.building_id isnull and b.id notnull
)
select distinct
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
	phase_name,
    phase_id,
    building_name,
    building_id,
    address_number,
    address_street,
	building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from change_request
where cr_record_action = 'insert'
; -- 11



------------------------------------------------------------------
select
    a.*
from "source".hk_midland_realty_sale_transaction a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-transaction-sale'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_sale_txn__map mp where mp.data_uuid = a.data_uuid)
;



select
	case when count(a.*) > 0 then true
	    else false
	    end as have_new_records
from "source".hk_midland_realty_rental_listing a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-listing-rent'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_rent_listing__map mp where mp.data_uuid = a.data_uuid)
;


select metadata.fn_create_change_request(
    'hk-new-hk_midland_rent_listing-records-2022-08-28',
    'pipeline',
    'pipeline'
); -- 586

call metadata.sp_add_change_table(
    currval('metadata.cr_id_seq')::int,
    'hk',
    replace('rent_listing', '-', '_')
);


drop table if exists staging_hk.hk_midland_rent_listing_id_table;

create table staging_hk.hk_midland_rent_listing_id_table as
select
    id
from metadata.change_request cr
where title = 'hk-new-hk_midland_rent_listing-records-2022-08-28'
    and author = 'pipeline'
    and requester = 'pipeline'
order by id desc
limit 1;


create table staging_hk.tmp_new_midland_rent_listing_records as
select
    a.*
from "source".hk_midland_realty_rental_listing a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-listing-rent'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_rent_listing__map mp where mp.data_uuid = a.data_uuid)
;


create table staging_hk.tmp_new_midland_rent_listing_records_20220828 as
select
   b.*,
   tu.property_dwid,
   tb.project_dwid,
   tb.building_dwid,
   tb.address_dwid,
   null as status_code,
   tb.id as map_id
from staging_hk.tmp_new_midland_rent_listing_records b
	left join premap_hk.midland_building_to_dwid tb
		on b.building_id = tb.building_id
		    and tb.building_id notnull
		    and b.building_id notnull
	left join premap_hk.midland_unit_to_dwid tu
	    on b.unit_id = tu.unit_id
		    and tu.unit_id notnull
		    and b.unit_id notnull
;-- 1991


with midland_units as (
    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
    from reference.hk_midland_backfill
    where unit_id notnull and floor notnull
)
, base as (
    select
        p.property_dwid as new_property_dwid,
        st.data_uuid
    from staging_hk.tmp_new_midland_rent_listing_records_20220828 st
    	left join midland_units mu
            on st.unit_id = mu.unit_id and mu.rn = 1
        left join masterdata_hk.property p
            on f_prep_dw_id(st.project_dwid) = f_prep_dw_id(p.project_dwid)
            and f_prep_dw_id(st.building_dwid) = f_prep_dw_id(p.building_dwid)
            and f_prep_dw_id(st.address_dwid) = f_prep_dw_id(p.address_dwid)
            and f_prep_dw_id(mu.floor) = f_prep_dw_id(p.address_floor_text)
            and f_prep_dw_id(st.flat) = f_prep_dw_id(p.address_stack)
    where st.property_dwid isnull and st.map_id notnull and p.property_dwid notnull
    group by 1, 2
)
update staging_hk.tmp_new_midland_rent_listing_records_20220828 a
set property_dwid = b.new_property_dwid
from base b where a.data_uuid = b.data_uuid
and exists (select 1 from base b where a.data_uuid = b.data_uuid)
;



insert into branch_hk.rent_listing_cr_586
(
	property_dwid,
	address_dwid,
    building_dwid,
    project_dwid,
    activity_display_text,
    rent_type,
    property_type_code,
    property_subtype,
    address_unit,
    address_local_text,
    bathroom_count,
    bedroom_count,
    gross_floor_area_sqm,
    net_floor_area_sqm,
	first_listing_date,
	rent_amount_monthly,
    country_code,
    data_uuid,
    data_source,
    cr_record_action
)
with midland_units as (
    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
    from reference.hk_midland_backfill
    where unit_id notnull and floor notnull
)
, base as (
select distinct
	s.property_dwid,
	s.address_dwid,
    s.building_dwid,
    s.project_dwid,
    s.estate_name,
	s.building_name,
	'rent'::text as rent_type,
	case when s.hos is true then 'hos'::text
		when (s.hos is false or s.hos isnull) and s.bedroom notnull then 'condo'::text
		when (s.hos is false or s.hos isnull) and s.bedroom isnull then 'comm'::text
		end as property_type_code,
	mu.floor || '-' || s.flat as address_unit,
	a.full_address_text as address_local_text,
	p.bathroom_count,
	coalesce(s.bedroom, p.bedroom_count) as bedroom_count,
	coalesce(s.area_sqm, p.gross_floor_area_sqm)::float as gross_floor_area_sqm,
    coalesce(s.net_area_sqm, p.net_floor_area_sqm)::float as net_floor_area_sqm,
	s.post_date::date as first_listing_date,
	s.rent::float as rent_amount_monthly,
	'cn'::text as country_code,
	s.data_uuid,
	'hk-midland-listing-rent'::text as data_source,
	'insert'::text as cr_record_action
from staging_hk.tmp_new_midland_rent_listing_records_20220828 s
    left join midland_units mu
            on s.unit_id = mu.unit_id and mu.rn = 1
    left join masterdata_hk.property p
        on s.property_dwid = p.property_dwid
    left join masterdata_hk.address a
        on s.address_dwid = a.address_dwid
)
select distinct
	property_dwid,
	address_dwid,
    building_dwid,
    project_dwid,
    lower(coalesce(bedroom_count || ' bdr ', '') || property_type_code || ' at ' || coalesce(estate_name, building_name)) as activity_display_text,
    rent_type,
    property_type_code,
    bedroom_count || ' bdr' as property_subtype,
    address_unit,
    address_local_text,
    bathroom_count,
    bedroom_count,
    gross_floor_area_sqm,
    net_floor_area_sqm,
	first_listing_date,
	rent_amount_monthly,
    country_code,
    data_uuid,
    data_source,
    cr_record_action
from base
;-- 1988

update branch_hk.rent_listing_cr_586
set activity_dwid = api.get_dwid(country_code, 'rent_listing', id)
;


call metadata.sp_submit_change_request(586, 'pipeline');

call metadata.sp_approve_change_request(586, 'pipeline');

call metadata.sp_merge_change_request(586);



insert into map_hk.midland_rent_listing__map
(
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
select
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from branch_hk.rent_listing_cr_586
; -- 1988


with base as (
    select
        max(t.lastmodifiedtime) as new_map_datetime
    from staging_hk.tmp_new_midland_rent_listing_records_20220828 t
)
update metadata.md_data_source mds
set current_map_datetime = base.new_map_datetime
from base
where mds.data_source_code = 'hk-midland-listing-rent'
;


-- use 'hos' column to update feature table
with base as (
    select
        b.region_name as city, 
        b.district_name as city_area, 
        upper(b.estate_name) as estate_name, 
        upper(b.building_name) as building_name, 
        upper(a.property_type_code) as property_type_code, 
        a.project_dwid , 
        a.building_dwid
    from branch_hk.rent_listing_cr_586 a
    left join staging_hk.tmp_new_midland_rent_listing_records_20220828 b on a.data_uuid = b.data_uuid
    where a.property_type_code = 'hos' and coalesce(a.project_dwid, a.building_dwid) notnull
    group by 1, 2, 3, 4, 5, 6, 7 --62
)
insert into feature_hk.de__building__public_housing
(
	city, city_area, estate_name, building_name, project_type, project_dwid, building_dwid
)
select b.*
from base b
where not exists (select 1 from feature_hk.de__building__public_housing a
where f_prep_dw_id(a.project_dwid) = f_prep_dw_id(b.project_dwid)
and f_prep_dw_id(a.building_dwid) = f_prep_dw_id(b.building_dwid)
)
; -- 3


insert into premap_hk.midland_unit_to_dwid
(
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
    phase_name,
    phase_id,
	building_name,
    building_id,
    address_number,
    address_street,
    floor,
    stack,
    unit_id,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
with change_request as (
    select distinct
        b.region_name,
        b.region_id,
        b.district_name,
        b.district_id,
        b.subregion_name,
        b.subregion_id,
        b.estate_name,
        b.estate_id,
        b.phase_name,
        b.phase_id,
        b.building_name,
        b.building_id,
        d.corrected_street_num as address_number,
        d.corrected_street_name as address_street,
        split_part(a.address_unit, '-', 1) as floor,
        b.flat as stack,
        b.unit_id,
        a.property_dwid,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        a.lot_group_dwid,
        'insert'::text as cr_record_action
    from branch_hk.rent_listing_cr_586 a
        left join staging_hk.tmp_new_midland_rent_listing_records_20220828 b
            on a.data_uuid = b.data_uuid
        left join premap_hk.midland_unit_to_dwid c
            on b.unit_id = c.unit_id
        left join reference.hk_midland_hkpost_correction_backfill d
            on b.building_id = d.building_id
    where c.unit_id isnull and b.serial_no  notnull
)
, change_request_seq as(
    select
        *,
        row_number() over (partition by region_name, district_name, subregion_name, estate_name,
                            phase_name, building_name, building_id, floor, stack order by project_dwid) as seq
    from change_request
    where cr_record_action = 'insert'
)
select
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
    phase_name,
    phase_id,
	building_name,
    building_id,
    address_number,
    address_street,
    floor,
    stack,
    unit_id,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from change_request_seq
where seq = 1
; -- 973



insert into premap_hk.midland_building_to_dwid
(
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
	phase_name,
    phase_id,
    building_name,
    building_id,
    address_number,
    address_street,
	building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
with change_request as (
    select distinct
        b.region_name,
        b.region_id,
        b.district_name,
        b.district_id,
        b.subregion_name,
        b.subregion_id,
        b.estate_name,
        b.estate_id,
        b.phase_name,
        b.phase_id,
        b.building_name,
        b.building_id,
        d.corrected_street_num as address_number,
        d.corrected_street_name as address_street,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        a.lot_group_dwid,
        'insert'::text as cr_record_action
    from branch_hk.rent_listing_cr_586 a
        left join staging_hk.tmp_new_midland_rent_listing_records_20220828 b
            on a.data_uuid = b.data_uuid
        left join premap_hk.midland_building_to_dwid c
            on b.building_id = c.building_id
        left join reference.hk_midland_hkpost_correction_backfill d
            on b.building_id = d.building_id
    where c.building_id isnull and b.serial_no notnull
)
select distinct
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
	phase_name,
    phase_id,
    building_name,
    building_id,
    address_number,
    address_street,
	building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from change_request
where cr_record_action = 'insert'
; -- 48




------------------------------------------------------------------
select
	case when count(a.*) > 0 then true
	    else false
	    end as have_new_records
from "source".hk_midland_realty_sale_listing a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-listing-sale'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_sale_listing__map mp where mp.data_uuid = a.data_uuid)
;



select metadata.fn_create_change_request(
    'hk-new-hk_midland_sale_listing-records-2022-08-28',
    'pipeline',
    'pipeline'
); -- 587

call metadata.sp_add_change_table(
    currval('metadata.cr_id_seq')::int,
    'hk',
    replace('sale_listing', '-', '_')
);


drop table if exists staging_hk.hk_midland_sale_listing_id_table;

create table staging_hk.hk_midland_sale_listing_id_table as
select
    id
from metadata.change_request cr
where title = 'hk-new-hk_midland_sale_listing-records-2022-08-28'
    and author = 'pipeline'
    and requester = 'pipeline'
order by id desc
limit 1;


create table staging_hk.tmp_new_midland_sale_listing_records as
select
    a.*
from "source".hk_midland_realty_sale_listing a
    cross join metadata.md_data_source mds
where mds.data_source_code = 'hk-midland-listing-sale'
and a.lastmodifiedtime > mds.current_map_datetime
and not exists (select 1 from map_hk.midland_sale_listing__map mp where mp.data_uuid = a.data_uuid)
; -- 1669


create table staging_hk.tmp_new_midland_sale_listing_records_20220828 as
select
   b.*,
   tu.property_dwid,
   tb.project_dwid,
   tb.building_dwid,
   tb.address_dwid,
   null as status_code,
   tb.id as map_id
from staging_hk.tmp_new_midland_sale_listing_records b
	left join premap_hk.midland_building_to_dwid tb
		on b.building_id = tb.building_id
		    and tb.building_id notnull
		    and b.building_id notnull
	left join premap_hk.midland_unit_to_dwid tu
	    on b.unit_id = tu.unit_id
		    and tu.unit_id notnull
		    and b.unit_id notnull
;


with midland_units as (
    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
    from reference.hk_midland_backfill
    where unit_id notnull and floor notnull
)
, base as (
    select
        p.property_dwid as new_property_dwid,
        st.data_uuid
    from staging_hk.tmp_new_midland_sale_listing_records_20220828 st
    	left join midland_units mu
            on st.unit_id = mu.unit_id and mu.rn = 1
        left join masterdata_hk.property p
            on f_prep_dw_id(st.project_dwid) = f_prep_dw_id(p.project_dwid)
            and f_prep_dw_id(st.building_dwid) = f_prep_dw_id(p.building_dwid)
            and f_prep_dw_id(st.address_dwid) = f_prep_dw_id(p.address_dwid)
            and f_prep_dw_id(mu.floor) = f_prep_dw_id(p.address_floor_text)
            and f_prep_dw_id(st.flat) = f_prep_dw_id(p.address_stack)
    where st.property_dwid isnull and st.map_id notnull and p.property_dwid notnull
    group by 1, 2
)
update staging_hk.tmp_new_midland_sale_listing_records_20220828 a
set property_dwid = b.new_property_dwid
from base b where a.data_uuid = b.data_uuid
and exists (select 1 from base b where a.data_uuid = b.data_uuid)
;



insert into branch_hk.sale_listing_cr_587
(
	property_dwid,
	address_dwid,
	building_dwid,
	project_dwid,
	activity_display_text,
	sale_type,
	property_completion_year,
	property_type_code,
	address_unit,
	address_local_text,
	tenure_code,
	bathroom_count,
	bedroom_count,
	gross_floor_area_sqm,
	net_floor_area_sqm,
	first_listing_date,
	listing_amount,
	country_code,
	data_uuid,
	data_source,
    cr_record_action
)
with midland_units as (
    select unit_id, floor, row_number() over(partition by unit_id order by last_tx_date) as rn
    from reference.hk_midland_backfill
    where unit_id notnull and floor notnull
)
, base as (
    select
        s.property_dwid,
        s.address_dwid,
        s.building_dwid,
        s.project_dwid,
        'sale'::text as sale_type,
        date_part('year', s.building_first_op_date::date) as property_completion_year,
        case when s.hos is true then 'hos'::text
            when (s.hos is false or s.hos isnull) and s.bedroom notnull then 'condo'::text
            when (s.hos is false or s.hos isnull) and s.bedroom isnull then 'comm'::text
            end as property_type_code,
        s.estate_name,
        s.building_name,
        mu.floor || '-' || s.flat as address_unit,
        a.full_address_text as address_local_text,
        'leasehold'::text as tenure_code,
        p.bathroom_count,
        coalesce(s.bedroom, p.bedroom_count) as bedroom_count,
        coalesce(s.area_sqm, p.gross_floor_area_sqm)::float as gross_floor_area_sqm,
        coalesce(s.net_area_sqm, p.net_floor_area_sqm)::float as net_floor_area_sqm,
        s.post_date::date as first_listing_date,
        case when s.hos is True then coalesce(nullif(s.price::float, 0), s.price_hos::float) 
			else s.price::float end as listing_amount,
        'cn'::text as country_code,
        s.data_uuid::uuid as data_uuid,
        'hk-midland-listing-sale'::text as data_source,
        'insert'::text as cr_record_action
    from staging_hk.tmp_new_midland_sale_listing_records_20220828 s
        left join midland_units mu
                on s.unit_id = mu.unit_id and mu.rn = 1
        left join masterdata_hk.property p
            on s.property_dwid = p.property_dwid
        left join masterdata_hk.address a
            on s.address_dwid = a.address_dwid
)
select distinct
	property_dwid,
	address_dwid,
    building_dwid,
    project_dwid,
    lower(coalesce(bedroom_count || ' bdr ', '') || property_type_code || ' at ' || coalesce(estate_name, building_name)) as activity_display_text,
    sale_type,
    property_completion_year,
	property_type_code,
	address_unit,
	address_local_text,
	tenure_code,
	bathroom_count,
	bedroom_count,
	gross_floor_area_sqm,
	net_floor_area_sqm,
	first_listing_date,
	listing_amount,
	country_code,
	data_uuid,
	data_source,
    cr_record_action
from base
; -- 1669



update branch_hk.sale_listing_cr_587
set activity_dwid = api.get_dwid(country_code, 'sale_listing', id)
;


call metadata.sp_submit_change_request(587, 'pipeline');

call metadata.sp_approve_change_request(587, 'pipeline');

call metadata.sp_merge_change_request(587);



insert into map_hk.midland_sale_listing__map
(
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
select
    data_uuid,
    activity_dwid,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from branch_hk.sale_listing_cr_587
; -- 1669



with base as (
    select
        max(t.lastmodifiedtime) as new_map_datetime
    from staging_hk.tmp_new_midland_sale_listing_records_20220828 t
)
update metadata.md_data_source mds
set current_map_datetime = base.new_map_datetime
from base
where mds.data_source_code = 'hk-midland-listing-sale'
;


with base as (
    select
        building_dwid, property_completion_year
    from branch_hk.sale_listing_cr_587
    where property_completion_year notnull and building_dwid notnull
    group by 1, 2
)
insert into feature_hk.de__building__completion_year_info
(
	building_dwid, construction_start, construction_end, completion_year
)
select
    b.building_dwid,
    null as construction_start,
    b.property_completion_year as construction_end,
    b.property_completion_year as completion_year
from base b
where not exists (select 1 from feature_hk.de__building__completion_year_info a
where a.building_dwid = b.building_dwid)
;-- 106



with base as (
    select
        initcap(b.region_name) as city,
        initcap(b.district_name) as city_area,
        upper(b.estate_name) as estate_name,
        upper(b.building_name) as building_name,
        upper(a.property_type_code) as property_type_code,
        a.project_dwid,
        a.building_dwid
    from branch_hk.sale_listing_cr_587 a
        left join staging_hk.tmp_new_midland_sale_listing_records_20220828 b
            on a.data_uuid = b.data_uuid
    where a.property_type_code = 'hos' and coalesce(a.project_dwid, a.building_dwid) notnull
    group by 1, 2, 3, 4, 5, 6, 7
)
insert into feature_hk.de__building__public_housing
(
	city, city_area, estate_name, building_name, project_type, project_dwid, building_dwid
)
select b.*
from base b
where not exists (
    select 1 from feature_hk.de__building__public_housing a
    where f_prep_dw_id(a.project_dwid) = f_prep_dw_id(b.project_dwid)
    and f_prep_dw_id(a.building_dwid) = f_prep_dw_id(b.building_dwid)
)
; -- 12



insert into premap_hk.midland_unit_to_dwid
(
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
    phase_name,
    phase_id,
	building_name,
    building_id,
    address_number,
    address_street,
    floor,
    stack,
    unit_id,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
with change_request as (
    select distinct
        b.region_name,
        b.region_id,
        b.district_name,
        b.district_id,
        b.subregion_name,
        b.subregion_id,
        b.estate_name,
        b.estate_id,
        b.phase_name,
        b.phase_id,
        b.building_name,
        b.building_id,
        d.corrected_street_num as address_number,
        d.corrected_street_name as address_street,
        split_part(a.address_unit, '-', 1) as floor,
        b.flat as stack,
        b.unit_id,
        a.property_dwid,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        a.lot_group_dwid,
        'insert'::text as cr_record_action
    from branch_hk.sale_listing_cr_587 a
        left join staging_hk.tmp_new_midland_sale_listing_records_20220828 b
            on a.data_uuid = b.data_uuid
        left join premap_hk.midland_unit_to_dwid c
            on b.unit_id = c.unit_id
        left join reference.hk_midland_hkpost_correction_backfill d
            on b.building_id = d.building_id
    where c.unit_id isnull and b.serial_no notnull
)
, change_request_seq as(
    select
        *,
        row_number() over (partition by region_name, district_name, subregion_name, estate_name,
                            phase_name, building_name, building_id, floor, stack order by project_dwid) as seq
    from change_request
    where cr_record_action = 'insert'
)
select
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
    phase_name,
    phase_id,
	building_name,
    building_id,
    address_number,
    address_street,
    floor,
    stack,
    unit_id,
    property_dwid,
    building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from change_request_seq
where seq = 1
; -- 1121




insert into premap_hk.midland_building_to_dwid
(
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
	phase_name,
    phase_id,
    building_name,
    building_id,
    address_number,
    address_street,
	building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
)
with change_request as (
    select distinct
        b.region_name,
        b.region_id,
        b.district_name,
        b.district_id,
        b.subregion_name,
        b.subregion_id,
        b.estate_name,
        b.estate_id,
        b.phase_name,
        b.phase_id,
        b.building_name,
        b.building_id,
        d.corrected_street_num as address_number,
        d.corrected_street_name as address_street,
        a.building_dwid,
        a.address_dwid,
        a.project_dwid,
        a.lot_group_dwid,
        'insert' as cr_record_action
    from branch_hk.sale_listing_cr_587 a
        left join staging_hk.tmp_new_midland_sale_listing_records_20220828 b
            on a.data_uuid = b.data_uuid
        left join premap_hk.midland_building_to_dwid c
            on b.building_id = c.building_id
        left join reference.hk_midland_hkpost_correction_backfill d
            on b.building_id = d.building_id
    where c.building_id isnull and b.serial_no notnull
)
select distinct
	region_name,
    region_id,
    district_name,
    district_id,
    subregion_name,
    subregion_id,
    estate_name,
    estate_id,
	phase_name,
    phase_id,
    building_name,
    building_id,
    address_number,
    address_street,
	building_dwid,
    address_dwid,
    project_dwid,
    lot_group_dwid
from change_request
where cr_record_action = 'insert'
; -- 42







