-- masterdata_sg.poi: https://gitlab.com/rea-workspace/willy/documentation/-/blob/master/singapore/sql/masterdata_sg/address_poi.sql

drop table playground.hk_mtr_stations;

CREATE TABLE playground.hk_mtr_stations (
	line_code varchar NULL,
	direction varchar NULL,
	station_code varchar NULL,
	station_id varchar NULL,
	mtr_station_name_cn varchar NULL,
	mtr_station_name_eng varchar NULL,
	mtr_sequence integer NULL
);

-- next week:
-- think about the table structure and how to fill in lat/long
-- ask for some new tasks besides HK data, maybe I can help ML or extraction side or i3mproving infrastructure?
		

------------------------------------------------------------------------------------

select
	'HK' as country_code,
	coalesce(uid.dwlocationpoiid, md5(poi_type || '_' || poi || '_' || poi_address)) as location_poi_dwid,
    coalesce(uid.dwprojectid, p.project_dwid) as project_dwid,
    null as amenities_dwid,
    'project'::varchar as location_type,
			case when lower(coalesce(uid.poitype, poi_type)) = 'hospital' then 'medical'
					when lower(coalesce(uid.poitype, poi_type)) = 'supermarket' then 'market'
					when lower(coalesce(uid.poitype, poi_type)) = 'bus' then 'bus stop'
					when lower(coalesce(uid.poitype, poi_type)) = 'traffic' then 'mtr station'
					else lower(poi_type)
			end as poi_type,
			lower(coalesce(uid.poitype, poi_type)) as poi_subtype,
    coalesce(uid.locationpoidisplaytext, poi) as location_poi_display_text,
    coalesce(uid.locationpoidisplaytext, poi) as location_poi_display_text_cn,
    coalesce(poi_ch, uid.locationpoidisplaytext, poi) as location_poi_display_text_hk,
    coalesce(uid.locationpoidisplaytext, poi) as location_poi_display_text_my,
    coalesce(uid.distancetopoim, distance_to_poi_m) as distance_to_poi_m,
    coalesce(uid.timetopoimin, time_to_poi_min) as time_to_poi_min,
	uid.latitude,
	uid.longitude,
	row_number() over(partition by coalesce(uid.dwlocationpoiid, md5(poi_type || '_' || poi || '_' || poi_address))) as rn
from reference.hk_new_launch_poi poi
	left join masterdata_hk.project p
		on lower(poi.project_name) = lower(p.project_name_text)
	left join map_hk.old_new_dwid ond
		on ond.dwid = p.project_dwid
			and ond.core_table = 'project'
	full outer join "source".user_input_dmlocationpoi_uniqueness uid
		on ond.old_dw_id = uid.dwprojectid
			and md5(poi_type || '_' || poi) = uid.dwlocationpoiid
			and uid.flag in ('create', 'update')
where uid.country = 'hk' or uid.country isnull
;

--create table masterdata_sg.poi as

drop table masterdata_hk.poi;

create table masterdata_hk.poi as 


with nl_base as (
select original_project_name, project_dwid, address_dwid
from premap_hk.new_launch_schematic_to_dwid
group by 1,2,3
)
, base as (
select 
	'HK' as country_code,
	ad.city,
	ad.city_area,
	ad.city_subarea,
	uid.longitude as poi_long,
	uid.latitude as poi_lat,
	case when lower(coalesce(uid.poitype, poi_type)) = 'hospital' then 'medical'
		when lower(coalesce(uid.poitype, poi_type)) = 'supermarket' then 'market'
		when lower(coalesce(uid.poitype, poi_type)) = 'bus' then 'bus stop'
		when lower(coalesce(uid.poitype, poi_type)) = 'traffic' then 'mtr station'
		else lower(poi_type)
		end as poi_type,
	lower(coalesce(uid.poitype, poi_type)) as poi_subtype,
	poi.poi as poi_name,
	poi.poi_address,
	null as poi_postal_code,
	true::bool poi_is_active,
	coalesce(uid.dwlocationpoiid, md5(poi_type || '_' || poi || '_' || coalesce(poi_address,''))) as location_poi_dwid,
	null as school_type,
	null as school_code,
	null as school_cop,
	null as good_school_indicator
	--, nl.project_dwid as project_dwid
	, row_number() over(partition by coalesce(uid.dwlocationpoiid, md5(poi_type || '_' || poi || '_' || coalesce(poi_address,'')))) as rn
from reference.hk_new_launch_poi poi
left join nl_base nl on lower(poi.project_name) = lower(nl.original_project_name)
left join masterdata_hk.address ad on nl.address_dwid = ad.address_dwid 
left join map_hk.old_new_dwid ond on ond.dwid = nl.project_dwid and ond.core_table = 'project'
full outer join "source".user_input_dmlocationpoi_uniqueness uid
	on ond.old_dw_id = uid.dwprojectid and md5(poi_type || '_' || poi) = uid.dwlocationpoiid and uid.flag in ('create', 'update')
where uid.country = 'hk' or uid.country isnull
group by 1,2,3,4,5,6,7,8,9,10,13
)
select 
	country_code,city,city_area,city_subarea,poi_long,poi_lat,poi_type,poi_subtype,poi_name,poi_address,poi_postal_code,poi_is_active,location_poi_dwid,school_type,school_code,school_cop,good_school_indicator
from base where rn = 1
; -- 2823


update masterdata_hk.poi 
set data_source = 'rea-manual'
where data_source isnull;

insert into masterdata_hk.poi 
with base as (
(select mtr_station_name_eng as poi_name
from playground.hk_mtr_stations
where direction ilike '%dt%' --97
group by 1
)
except 
(select trim(split_part(poi_name, '(',1)) as poi_name
from masterdata_hk.poi 
where poi_type IN ('mtr station') and poi_name ilike '%Subway%' --26
group by 1
)
)--72
select 
	'HK' as country_code,
	null as city,null as city_area,null as city_subarea,null as poi_long,null as poi_lat,
	'mtr station' as poi_type,
	'traffic' as poi_subtype,
	base.poi_name||' (Subway Station)' as poi_name,
	null as poi_address,
	null as poi_postal_code,
	true::bool as poi_is_active,
	md5('mtr station' || '_' || base.poi_name||' (Subway Station)' || '_' || '') as location_poi_dwid,
	null as school_type,null as school_code,null as school_cop,null as good_school_indicator,
	'rea-manual' as data_source 
from base; --72

update masterdata_hk.poi 
set city = lower(city), city_area = lower(city_area), poi_name = lower(poi_name)
where poi_type IN ('mtr station');


update masterdata_hk.poi 
set city = lower(city), city_area = lower(city_area), city_subarea = lower(city_subarea), poi_name = lower(poi_name), poi_address = lower(poi_address)
;



-- address_poi
create table feature_hk.de__address__poi as
with nl_base as (
select original_project_name, project_dwid, address_dwid
from premap_hk.new_launch_schematic_to_dwid
group by 1,2,3
)
select 
	nl.project_dwid,
	null as address_dwid,
	coalesce(uid.dwlocationpoiid, md5(poi_type || '_' || poi || '_' || coalesce(poi_address,''))) as location_poi_dwid,
	'HK' as country_code,
	ad.city,
	ad.city_area,
	ad.city_subarea,
	uid.longitude as poi_long,
	uid.latitude as poi_lat,
    coalesce(uid.distancetopoim, poi.distance_to_poi_m) as distance_to_poi_m,
    coalesce(uid.timetopoimin, poi.time_to_poi_min) as time_to_poi_min,
	case when lower(coalesce(uid.poitype, poi_type)) = 'hospital' then 'medical'
		when lower(coalesce(uid.poitype, poi_type)) = 'supermarket' then 'market'
		when lower(coalesce(uid.poitype, poi_type)) = 'bus' then 'bus stop'
		when lower(coalesce(uid.poitype, poi_type)) = 'traffic' then 'mtr station'
		else lower(poi_type)
		end as poi_type,
	lower(coalesce(uid.poitype, poi_type)) as poi_subtype,
	poi.poi as poi_name,
	poi.poi_address,
	null as poi_postal_code,
	true::bool poi_is_active
	--, row_number() over(partition by coalesce(uid.dwlocationpoiid, md5(poi_type || '_' || poi || '_' || coalesce(poi_address,''))), nl.project_dwid) as rn
from reference.hk_new_launch_poi poi
left join nl_base nl on lower(poi.project_name) = lower(nl.original_project_name)
left join masterdata_hk.address ad on nl.address_dwid = ad.address_dwid 
left join map_hk.old_new_dwid ond on ond.dwid = nl.project_dwid and ond.core_table = 'project'
full outer join "source".user_input_dmlocationpoi_uniqueness uid
	on ond.old_dw_id = uid.dwprojectid and md5(poi_type || '_' || poi) = uid.dwlocationpoiid and uid.flag in ('create', 'update')
where uid.country = 'hk' or uid.country isnull
group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,17
; 

--left join masterdata_sg.address a
--	on st_intersects(st_buffer(st_makepoint(b.poi_long, b.poi_lat), 0.01), st_makepoint(a.longitude, a.latitude))
--		and a.address_type_attribute = 'primary-point';

		
	replace(cuj.distance_to_poi_m, 'm', '')::int as distance_to_poi_m,
	round(coalesce(cuj.time_to_poi_min::int, cuj.distance_to_poi_m / 84)) as time_to_poi_min,

